<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'/v1','middleware'=>'check.lang'],function () {
    //Route::post('members/social', 'MemberController@social');
    Route::post('signin', 'MemberController@signin');
    Route::patch('change-password', 'MemberController@changePassword');
    Route::post('signup', 'MemberController@signup');

    Route::post('members/social', 'MemberController@social');
    Route::post('members/facebook', 'MemberController@facebook');

    //Get lists
    Route::get('currencies', 'CurrencyController@getCurrencies')->name('currencies.index');
    Route::get('countries', 'CountryController@getCountries')->name('countries.index');
    Route::get('cities', 'CityController@getCities')->name('cities.index');
    Route::get('listing-manager-types', 'ListingManagerTypeController@getListingManagerTypes')->name('listing-manager-types.index');
    Route::get('listing-types', 'ListingTypeController@getListingTypes')->name('listing-types.index');
    Route::get('room-types', 'RoomTypeController@getRoomTypes')->name('room-types.index');
    //Members
    Route::get('members/single-listings', 'MemberController@getMemberSingleListings')->name('member-single-lists.index');
    Route::get('members/group-listings', 'MemberController@getMemberGroupListings')->name('member-group-lists.index');
    Route::get('members/wishlists', 'MemberController@getMemberWishlists')->name('member-wishlists.index');
    Route::post('members/add-wishlist', 'MemberController@getMemberAddToWishlists')->name('member-add-wishlist.index');
    Route::delete('members/delete-wishlist', 'MemberController@getMemberDeleteFromWishlists')->name('member-delete-wishlist.index');
    Route::get('members/trips', 'MemberController@getMemberTrips')->name('member-trips.index');
    Route::get('members/approved-bookings', 'MemberController@getMemberApprovedBookings')->name('member-approved-bookings.index');
    Route::get('members/pending-requests', 'MemberController@getMemberPendingRequests')->name('member-pending-requests.index');
    Route::get('members/connections', 'MemberController@getMemberConnections')->name('member-connections.index');
    Route::get('members/reviews-as-host', 'MemberController@getMemberReviewsAsHost')->name('member-reviews-as-host.index');
    Route::get('members/reviews-as-guest', 'MemberController@getMemberReviewsAsGuest')->name('member-reviews-as-guest.index');
    Route::post('members/add-requests', 'MemberController@getAddConnection')->name('member-add-requests.index');
    Route::patch('members/accept-requests', 'MemberController@getAcceptConnection')->name('member-accept-requests.index');
    Route::delete('members/cancel-requests', 'MemberController@getCancelConnection')->name('member-cancel-requests.index');
    Route::delete('members/remove-requests', 'MemberController@getRemoveConnection')->name('member-remove-requests.index');
    Route::get('members/inbox-as-host', 'MemberController@getAllConversationSentToMeAsHost')->name('member-inbox-as-host.index');
    Route::get('members/inbox-as-guest', 'MemberController@getAllConversationSentToMeAsGuest')->name('member-inbox-as-guest.index');
    Route::post('members/login', 'MemberController@login');
    Route::patch('members', 'MemberController@update');
    Route::resource('members', 'MemberController', ['only' => [
        'show'
    ]]);
    Route::resource('lists', 'ListingController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('group-lists', 'GroupListingController', [
        'except' => ['edit', 'create']
    ]);
    Route::patch('lists/{list_id}/photos/cover', 'ListingPhotoController@setCover');
    Route::resource('lists/{list_id}/photos', 'ListingPhotoController', [
       'except' => ['edit', 'create']
    ]);
    Route::get('amenities', 'AmenitiesController@getAmenities')->name('amenities');
    Route::get('amenities/{category_id}', 'AmenitiesController@getAmenitiesByCategory')
        ->name('amenities.category');
    Route::get('amenities-categories', 'AmenitiesCategoryController@getAmenitiesCategories')->name('amenities-category');
    Route::get('listing-rules', 'ListingRuleController@getListingRules')->name('listing-rules');
    Route::patch('lists/{list_id}/amenities', 'ListingAmenityController@getUpdateAmenities');
    Route::get('lists/{list_id}/amenities', 'ListingAmenityController@getAllAmenities');

    Route::post('lists/{list_id}/address', 'ListingAddressController@postListingAddress');
    Route::patch('lists/{list_id}/address', 'ListingAddressController@updateListingAddress');
    Route::post('lists/{list_id}/season', 'ListingSeason1Controller@postListingSeason');
    Route::patch('lists/{list_id}/season/{season_id}', 'ListingSeason1Controller@updateListingSeason');
    Route::post('lists/{list_id}/rate', 'ListingRateController@postListingRate');
    Route::patch('lists/{list_id}/rate', 'ListingRateController@updateListingRate');
    Route::post('lists/{list_id}/calender', 'ListingCalenderController@postListingCalender');
    Route::patch('lists/{list_id}/calender/{exception_date_id}', 'ListingCalenderController@updateListingCalender');

    Route::get('phone-codes','PhoneCodesController@getPhoneCodes');
    Route::get('group-lists-types','GroupListsTypesController@getGroupListsTypes');
    Route::resource('lists/{list_id}/linked-date', 'ListingLinkedDateController', [
        'except' => ['edit', 'create']
    ]);
    Route::resource('lists/{list_id}/blocked-date', 'ListingBlockedDateController', [
        'except' => ['edit', 'create']
    ]);
    Route::resource('lists/{list_id}/seasons', 'ListingSeasonController', [
        'except' => ['edit', 'create']
    ]);

});
