<?php

namespace App\Http\Controllers;

use App\ListingPhoto;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Listing;
use Validator;

class ListingPhotoController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'store','index','destroy','setCover'
        ]]);
        $this->middleware('check.list.access');
    }

    /**
     * @api {get} /lists/{list_id}/photos [step 3] Get List Photos
     * @apiName [step 3] Get List Photos
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function index($list_id)
    {
        $listingPhotos = ListingPhoto::where('list_id', $list_id)->get();

        $response = [
            'message' => __('listing/api.get_list_photos_success'),
            'result' => $listingPhotos
        ];
        return response()->json($response, 200);
    }

    /**
     * @api {post} /lists/{list_id}/photos [step 3] Add List Photos
     * @apiName [step 3] Add List Photos
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String} photo base64 of the list .
     */
    public function store($list_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo'         => 'required',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }
        $listing_photo = new ListingPhoto;

        $data = $request->input('photo');

        $listing_photos = ListingPhoto::where('list_id', $list_id)->get();

        if(count($listing_photos) === 0){
            $listing_photo->cover = 1;
        }else{
            $listing_photo->cover = 0;
        }

        $max_order = ListingPhoto::where('list_id', $list_id)->orderBy('order_no', 'desc')->first();
        if($max_order == null){
            $order_no = 0;
        }else{
            $order_no = intval($max_order->order_no) + 1;
        }
        $directory = 'listing';
        $sizes = [270, 51];
        $listing_photo->photo     = uploadAndSave($data, $directory, $sizes);
        $listing_photo->list_id   = $list_id;
        $listing_photo->order_no  = $order_no;

        if($listing_photo->save()) {
            $listing_photos = ListingPhoto::where('list_id', $list_id)->get();
            $response = [
                'message' => __('listing/api.add_photo_to_list_success'),
                'result' => $listing_photos
            ];
            return response()->json($response, 201);
        }
        else {
            $response = [
                'message' => __('listing/api.add_photo_to_list_error'),
            ];
            return response()->json($response, 500);
        }


    }

    /**
     * @api {patch} /lists/{list_id}/photos/cover [step 3] Add List Photos cover
     * @apiName [step 3] Add List Photos cover
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String} photo_id id of the photo's list .
     */
    public function setCover($list_id,Request $request)
    {
        $photo_check = ListingPhoto::where('list_id',$list_id)->where('id',$request->input('photo_id'))->get();
        if(count($photo_check) === 0){
            return response()->json([
                'message' => __('listing/api.photo_id_not_correct'),
            ],400);
        }
        $listing_photos = ListingPhoto::where('list_id',$list_id)->get();

        foreach ($listing_photos as $listing_photo) {
            $listing_photo->cover = 0;
            $listing_photo->update();
        }

        $listing_photo = ListingPhoto::find($request->input('photo_id'));
        $listing_photo->cover = 1;
        if ($listing_photo->update()) {
          $response = [
              'message' => __('listing/api.photo_set_cover_success'),
          ];
          return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('listing/api.photo_set_cover_error'),
            ];
            return response()->json($response, 500);
        }

    }

    /**
     * @api {delete} /lists/{list_id}/photos/{photo_id} [step 3] Delete List Photos
     * @apiName [step 3] Delete List Photos
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String} photo_id id of the photo's list .
     */
    public function destroy($list_id, $id)
    {
        $delete_check = ListingPhoto::where('list_id',$list_id)->whereNotNull('deleted_at')->get();

        if(count($delete_check) === 1){
            return response()->json([
                'message' => __('listing/api.can_not_delete_last_photo'),
            ],400);
        }

        $cover_check = ListingPhoto::where('list_id',$list_id)->where('cover',1)->get();

        if(count($cover_check) === 1){
            return response()->json([
                'message' => __('listing/api.can_not_delete_cover_photo'),
            ],400);
        }

        $photo_check = ListingPhoto::where('list_id',$list_id)->where('id',$id)->get();
        if(count($photo_check) === 0){
            return response()->json([
                'message' => __('listing/api.photo_id_not_correct'),
            ],400);
        }

        $listing_photo = ListingPhoto::find($id);

        $listing_photo->delete();

        $response = [
            'message' => __('listing/api.delete_photo_success'),
        ];
        return response()->json($response, 200);

    }
}
