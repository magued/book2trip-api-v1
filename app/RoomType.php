<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class RoomType extends Model {

    protected $table = 'room_type';

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name
        ];
    }
}
