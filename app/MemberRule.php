<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;



/**
 *
 * model of member table
 */
class MemberRule extends Model
{
    protected $table = 'member_rules';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'rule', 'list_id',   'updated_at',  'deleted_at', 'created_at',
    ];

    public function toArray()
    {
        return [
//            'id' => $this->id,
            'rule' => $this->rule ,
        ];
    }


}
