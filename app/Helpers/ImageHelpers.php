<?php
use Intervention\Image\Facades\Image;

function uploadAndSave($data, $directory, $sizes)
{
    list($type, $data) = explode(';', $data);

    list(, $data)      = explode(',', $data);

    $data = base64_decode($data);

    $f = finfo_open();

    $mime_type = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);

    $split = explode( '/', $mime_type );

    $type = $split[1];

    $image_name     = rand(99, 999999) . date('YmdHis') . "." . $type;

    $image_path     = public_path() . "/uploads"."/".$directory."/".$image_name;

    file_put_contents($image_path, $data);

    //original photo fetching to Intervention package Object
    $img = Image::make($image_path);

    //resize & mirror Original Image to Medium
    $destinationPath_medium = public_path() . "/uploads"."/".$directory."/"."medium/";
    $img->resize(null, $sizes[0], function ($constraint) {
        $constraint->aspectRatio();
    });
    $img->save($destinationPath_medium . $image_name);

    //resize & mirror Original Image to Thumbenial
    $destinationPath_thumb = public_path() . "/uploads"."/".$directory."/"."thumb/";
    $img->resize(null, $sizes[1], function ($constraint) {
        $constraint->aspectRatio();
    });
    $img->save($destinationPath_thumb . $image_name);
    return $image_name;
}