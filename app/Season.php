<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Season extends Model {

    protected $table = 'seasons';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable =[ 'id', 'title', 'start_date', 'end_date', 'price', 'day_price', 'minimum_stay', 'weekend_price',
        'weekend_days', 'weekly_discount', 'monthly_discount', 'created_at', 'updated_at', 'deleted_at', 'list_id' ];


    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id');
    }

    public function toArray()
    {
        return [
          'id' => $this->id,
          'title' => $this->title,
          'start_date' => $this->start_date,
          'end_date' => $this->end_date,
          'price' => $this->price,
          'minimum_stay' => $this->minimum_stay,
          'weekend_price' => $this->weekend_price,
          'weekly_discount' => $this->weekly_discount,
          'monthly_discount' => $this->monthly_discount,
        ];
    }
}
