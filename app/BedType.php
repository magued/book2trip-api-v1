<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;


class BedType extends Model
{
    protected $table = 'beds_type';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function listing(){

    	 return $this->belongsToMany('App\Listing', 'list_beds_type', 'bed_type_id', 'list_id');
    }

    public function toArray()
    {
        $listBedTypes = ListBedType::where('bed_type_id', $this->id )->get();

        foreach ($listBedTypes as $listBedType) {
            $amount = $listBedType->amount;
        }

        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name,
            'amount' => $amount
        ];
    }

}
