<?php

namespace App\AbstractClasses;

use App\ExceptionDate;
use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingAmenity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


Abstract class Search extends Controller
{
    /**
     * all queries has been inserted here
     * @param $request
     * @return object
     */
    public function searchQuery($request)
    {
        if(isset($request['pagination']) && $request['pagination'] > 0){
            $pagination = $request['pagination'];
        }else{
            $pagination = 10;
        }

        if(! isset($request['guests'])){
            $request['guests'] = 1;
        }
        $query = Listing::select('lists.*');

        if (isset($request['amenities'])) {
            $amenities_array = explode(',',$request['amenities']);
            $listAmenitiesIds = ListingAmenity::where('amenity_id', $amenities_array)->pluck('list_id');
        }

        if (isset($request['start_date']) || isset($request['end_date'])) {
            if(!isset($request['start_date'])){
                $request['start_date'] = date('Y-M-D');
            }
            if(!isset($request['end_date'])){
                $request['end_date'] = date('Y-M-D',strtotime("+1 day", strtotime($request['start_date'])));
            }
            $listExceptionDatesIds = ExceptionDate::select('list_id')->where('exception_date.start_date', '=', $request['start_date'])
                ->where('exception_date.end_date', '=', $request['end_date'])
                ->where('exception_date.status', '=', 2)->pluck('list_id')->toArray();
        }

        foreach ($request as $key => $value) {
//            ($key === 'keyword') ? $query->keyword($value) : null;
            ($key === 'country') ? $query->country($value) : null;
            ($key === 'governorate') ? $query->governorate($value) : null;
//            ($key === 'city') ? $query->city($value) : null;
//            ($key === 'guests') ? $query->guests($value) : null;
            ($key === 'list_type_id') ? $query->listTypeId($value) : null;
            ($key === 'amenities') ? $query->listIds($listAmenitiesIds) : null;
            ($key === 'managed_by') ? $query->managedBy($value) : null;
            ($key === 'beds') ? $query->beds($value) : null;
            ($key === 'bedrooms') ? $query->bedrooms($value) : null;
            ($key === 'bathrooms') ? $query->bathrooms($value) : null;
            ($key === 'price') ? $query->price($request['currency_id'],is_array($value) ? $value : explode(',', $value)) : null;
            ($key === 'start_date' && isset($listExceptionDatesIds)) ? $query->exceptionDates($listExceptionDatesIds) : null;


            if ($key === 'room_type_id') {
                $query->where(function ($query) use ($request) {
                    for ($i = 0, $len = count($request['room_type_id']); $i < $len; $i++) {
                        $query->orWhere('room_type_id', '=', $request['room_type_id'][$i]);
                    }
                });
            }

            if ($key === 'keyword') {
                $query->orWhere('lists.keywords', 'like', '%' . $request['keyword'] . '%');
            }

            if ($key === 'city') {
                $query->orWhere('lists.city', 'like', '%' . $request['city'] . '%');

            }
        }

        if(isset($request['city'])){
            $city = $request['city'];
        }else{
            $city ='';
        }

        return $query->guests($request['guests'])->published()->orderByRaw("FIELD(city , ?) DESC",[$city])->orderBy('id', 'desc')->paginate($pagination)->appends($request);

    }
}
