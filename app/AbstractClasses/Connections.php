<?php


namespace App\AbstractClasses;

use App\Http\Controllers\Controller;
use App\Currency;
use Illuminate\Support\Facades\Auth;
use App\Member;
use App\Connection ;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Validator;
use App\Listing;
use App\Booking;

abstract class Connections extends Controller
{

    public function MyConnection($username ,$numberOfUser = 1 )
    {
        $member = Member::where("username" , $username)->first();
        if(count($member) == 0){
            return "404";

        }
        $connectionsAsMember = Connection::where("member_id" , $member->id)->where("status" , '1' )->get();
        $connectionsAsFriend = Connection::where("friend_id" , $member->id)->where("status" , '1' )->get();
        $connectionsArr = array();
        foreach ($connectionsAsMember as $con){
            array_push($connectionsArr, $con->friend_id);
        }

        foreach ($connectionsAsFriend as $con){
            array_push($connectionsArr, $con->member_id);
        }

        $connections = Member::whereIn("id" , $connectionsArr)->paginate($numberOfUser);
        $eachFriendStatus = array();
        $myId = (Auth::check()) ? Auth::user()->id : 0 ;
        foreach($connections as $connection){
            $eachFriendStatus[$connection->id] = $this->checkConnection($myId , $connection->id);
        }

       // dd($connections);
        $connections = $this->mutualFriend($member->id ,$connections ,$connectionsArr );
        //dd($connections);
        return array ('member' => $member,'connections' =>$connections ,"friendsRelationToMe" => $eachFriendStatus);

    }

    public function checkConnection($friend_id,$member_id)
    {
        $ourConnection = 0;
        if(Auth::check()){

            $checkConnectionAsFriend = Connection::where("member_id", $member_id)->where("friend_id", $friend_id)->get();
            $checkConnectionAsMember = Connection::where("friend_id", $member_id)->where("member_id", $friend_id)->get();
            if (count($checkConnectionAsMember) > 0 || count($checkConnectionAsFriend) > 0) {
                if (count($checkConnectionAsFriend) > 0) {
    //                    return $checkConnectionAsFriend;
                    if ($checkConnectionAsFriend[0]->status == 0) {
                        $ourConnection = 2;
                    } elseif ($checkConnectionAsFriend[0]->status == 1) {
                        $ourConnection = 4;
                    }

                } elseif (count($checkConnectionAsMember) > 0) {
    //                    return $checkConnectionAsMember;
                    if ($checkConnectionAsMember[0]->status == 0) {
                        $ourConnection = 1;
                    } elseif ($checkConnectionAsMember[0]->status == 1) {
                        $ourConnection = 5;
                    }

                }
            }

        }
            return $ourConnection;
    }


    public function mutualFriend($user_id,$opjectOfUserMutualFriend , $ArrayOfUserMutualFriend)
    {
        $i = 0  ;

        foreach ($opjectOfUserMutualFriend as $user) {

            $Mutual = Connection::where("status" , '1' )->

               where("member_id" , $user->id)->
                whereIn("friend_id" , $ArrayOfUserMutualFriend)->

                        orWhere("friend_id" , $user->id)->
                        whereIn("member_id" , $ArrayOfUserMutualFriend)->
                       get();
            $mutualIdsArray = array();
            foreach ($Mutual as $frend){

                if($frend->member_id == $user->id && $frend->friend_id !=$user_id){

                    array_push($mutualIdsArray, $frend->friend_id);
                }elseif($frend->friend_id == $user->id && $frend->member_id !=$user_id){
                    array_push($mutualIdsArray, $frend->member_id);
                }

            }

            $opjectOfUserMutualFriend[$i]['mutualFrend'] = Member::whereIn("id" , $mutualIdsArray)->paginate(5);

            $i++ ;
        }

        return $opjectOfUserMutualFriend ;
    }

    /**
     * @param $user_id
     * @param $friend_id
     * @return int
     */
    public function addConnection($user_id , $friend_id)
    {
        $checkConnection = Connection::where("member_id", $user_id)->where("friend_id", $friend_id)->first();

        if(count($checkConnection) != 0){
            return 0;
        }

        $newConnection = new Connection();

        if($newConnection->member_id === $newConnection->friend_id){
            return 1;
        }

        $newConnection->member_id = $user_id;
        $newConnection->friend_id = $friend_id;
        $newConnection->status = 0;
        $newConnection->save();

        return $newConnection;
    }

    public function acceptConnection($friend_id, $member_id)
    {
        $checkConnection = Connection::where("member_id" , $member_id)->where("friend_id",$friend_id)->first();

        if(count($checkConnection) ===  0){
            return  0  ;
        }

        $checkConnection->status = 1;
        $checkConnection->save();

        return $checkConnection;
    }


    public function cancelConnection($friend_id ,$member_id)
    {
        $checkConnection = Connection::where("member_id" , $member_id)
                          ->where("friend_id",$friend_id)
                          ->where('status', 0)
                          ->first();
        if(count($checkConnection) === 1){
            $checkConnection->delete();
            return 1;
        }
        else {
            return 0;
        }
    }

    public function removeConnection($friend_id ,$member_id)
    {
        $checkConnection = Connection::where("member_id" , $member_id)
                          ->where("friend_id",$friend_id)
                          ->where('status', 1)
                          ->first();
        if(count($checkConnection) == 1){
            $checkConnection->delete();
            return 1 ;
        } else {
            return 0;
        }
    }



}
