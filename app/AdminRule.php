<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class AdminRule extends Model
{
    // Table Name
    protected $table = 'admin_rules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' , 'rule_en', 'rule', 'created_at', 'updated_at', 'deleted_at',
    ];

    use SoftDeletes;


    public function listRules()
    {
        return $this->hasMany('App\ListRules', 'rule_id');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'rule' => (App::getLocale() == 'en') ? $this->rule_en : $this->rule,
        ];
    }
}
