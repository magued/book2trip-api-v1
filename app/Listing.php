<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use JWTException;

class Listing extends Model
{
    protected $table = 'lists';

    protected $fillable = [
      'id', 'title', 'description', 'more_place_desc',  'video', 'title_seo',
      'metadata', 'keywords', 'address', 'governorate',  'city', 'country','country_short',
      'building_number', 'zip_code', 'unit_number', 'latitude',  'longitude', 'accommodates',
      'bedrooms', 'bathrooms', 'beds', 'guest',  'minimum_stay', 'size','room_type_id',
      'size_type', 'price', 'search_price', 'weekly_discount',  'monthly_discount', 'cleaning_fees',
      'additional_guest', 'daily_price', 'page_viewed', 'viewed', 'phone','security_fees',  'managed_by', 'weekend_fees',
      'weekend_days', 'approved', 'step', 'member_id',  'list_type_id', 'currency_id','rate',
      'token', 'amount', 'is_group', 'group_id',   'updated_at',  'deleted_at', 'created_at','r'
    ];

    public function currency(){
        return $this->belongsTo('App\Currency', 'currency_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member','member_id');
    }

    public function photos()
    {
        return $this->hasMany('App\ListingPhoto','list_id')->orderBy('cover' , 'Desc')->orderBy('id', 'ASC');
    }

    public function listingType()
    {
        return $this->belongsTo('App\ListingType', 'list_type_id');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomType','room_type_id');
    }

    public function bedType(){

    	 return $this->belongsToMany('App\BedType', 'list_beds_type', 'list_id', 'bed_type_id');
    }

    public function amenity(){

    	 return $this->belongsToMany('App\Amenity', 'list_amenities', 'list_id', 'amenity_id')->whereNull('list_amenities.deleted_at');
    }

    public function seasons()
    {
        return $this->hasMany('App\Season','list_id');
    }

    public function listingRules()
    {
        return $this->hasMany('App\ListingRule', 'list_id');
    }

    public function memberRules()
    {
        return $this->hasMany('App\MemberRule', 'list_id');
    }

    public function groupListing()
    {
        return $this->belongsTo('App\GroupListing', 'group_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking','list_id');
    }

    public function exceptionDates()
    {
        return $this->hasMany('App\ExceptionDate','list_id')->where('end_date',">=",date('Y-m-d'));
    }

    public function scopePublished($query)
    {
        return $query->where('lists.approved', '=', 2);
    }

    public function scopeCountry($query, $country)
    {
        return $query->where('lists.country', 'like', '%' . $country . '%');
    }

//    public function scopeCity($query, $city)
//    {
//        return $query->orWhere('lists.city', 'like', '%' . $city . '%');
//    }

    public function scopeGovernorate($query, $governorate)
    {
        return $query->where('lists.governorate', 'like', '%' . $governorate . '%');
    }

    public function scopeGuests($query, $guestCounts)
    {
        return $query->where('lists.guest','>=', $guestCounts);
    }

    public function scopeRoomTypeId($query, $roomTypeId)
    {
        return $query->where('lists.room_type_id', $roomTypeId);
    }

    public function scopeListIds($query, $listIds)
    {
        return $query->whereIn('lists.id', $listIds);
    }

    public function scopeManagedBy($query, $managedBy)
    {
        return $query->where('lists.managed_by', $managedBy);
    }

    public function scopeListTypeId($query, $listTypeId)
    {
        return $query->where('lists.list_type_id', $listTypeId);
    }

    public function scopeBeds($query, $beds)
    {
        return $query->where('lists.beds', $beds);
    }

    public function scopeBedrooms($query, $bedrooms)
    {
        return $query->where('lists.bedrooms', $bedrooms);
    }

    public function scopeBathrooms($query, $bathrooms)
    {
        return $query->where('lists.bathrooms', $bathrooms);
    }

    public function scopeExceptionDates($query, Array $dates)
    {
        return $query->whereNotIn('lists.id', $dates);
    }

    public function scopePrice($query,$currencyId, Array $price)
    {
        $price_convert[0]=\App\Currency::change($currencyId,2,  $price[0]);
        $price_convert[1]=\App\Currency::change($currencyId,2,  $price[1]);
        if($price_convert[1] === 1000.0){
            return $query->where('lists.search_price','>', $price_convert[0]);
        }else {
            return $query->whereBetween('lists.search_price', $price_convert);
        }
    }

    public function toArray()
    {
        $wishlist_value = 0;

        if(isset(\Illuminate\Support\Facades\Request::header()['authorization'])) {

            $user = JWTAuth::parseToken()->authenticate();

            $wishlist = Wishlist::where('list_id', $this->id)->where('member_id', $user->id)->first();

            if ($wishlist !== NULL) {
                $wishlist_value = 1;
            }

        }

        $public_price = 0;

        if(Input::get('currency_id')){
            if(isset($this->currency)){
                $public_price = Currency::change(intval($this->currency->id),intval(Input::get('currency_id')),intval($this->price));
            }
        }

        switch ($this->approved) {
            case 0:
                $this->approved = 'Incomplete data';
                break;
            case 1:
                $this->approved = 'Waiting for approval & edit';
                break;
            case 2:
                $this->approved = 'Published';
                break;
            case 3:
                $this->approved = 'Stopped by user';
                break;
        }

        return [
          'id'                => $this->id,
          'title'             => $this->title,
          'description'       => $this->description,
          'keywords'          => $this->keywords,
          'landmark'          => $this->more_place_desc,
          'address'           => $this->address,
          'governorate'       => $this->governorate,
          'city'              => $this->city,
          'country'           => $this->country,
          'building_number'   => $this->building_number,
          'latitude'          => $this->latitude,
          'longitude'         => $this->longitude,
          'accommodates'      => $this->accommodates,
          'bedrooms'          => $this->bedrooms,
          'bathrooms'         => $this->bathrooms,
          'beds'              => $this->beds,
          'guest'             => $this->guest,
          'minimum_stay'      => $this->minimum_stay,
          'amount'            => $this->amount,
          'public_price'      => $public_price,
          'zip_code'          => $this->zip_code,
//          'search_price'      => $this->search_price,
          'status'            => $this->approved,
          'price'             => $this->price,
          'step'              => $this->step,
          'weekend_fees'      => $this->weekend_fees,
          'weekly_discount'   => $this->weekly_discount,
          'monthly_discount'  => $this->monthly_discount,
          'additional_guest'  => $this->additional_guest,
          'security_fees'     => $this->security_fees,
          'cleaning_fees'     => $this->cleaning_fees,
          'rate'              => $this->rate,

          //ORM
          'owner'             => $this->member,
          'photos'            => $this->photos,
          'managed_by'        => ($this->managed_by == 1) ? __('api.owner') : __('api.manager') ,
          'listing_type'      => $this->listingType,
          'room_type'         => $this->roomType,
          'bed_type'          => $this->bedType,
          'amenities'         => $this->amenity,
          'currency'          => $this->currency,
          'seasons'           => $this->seasons,
          'admin_rules'       => $this->listingRules,
          'member_rules'      => $this->memberRules,
          'exception_dates'   => $this->exceptionDates,
          'is_wishlisted'     => $wishlist_value,
        ];
    }
}
