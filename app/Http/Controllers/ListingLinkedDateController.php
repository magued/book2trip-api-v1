<?php

namespace App\Http\Controllers;

use App\Listing;
use App\ExceptionDate;
use Illuminate\Http\Request;
use JWTAuth;
use JWTException;
use Validator;

class ListingLinkedDateController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
        $this->middleware('check.list.access');
    }

    /**
     * @api {get} /lists/{list_id}/linked-date [step 7] get All Linked Dates
     * @apiName [step 7] get All Linked Dates
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     */
    public function index($list_id)
    {
        $linked_date = ExceptionDate::where('list_id',$list_id)->where('status',1)->get();
        return response()->json([
            'message' => __('listing/api.get_linked_date_list_success'),
            'result'  => $linked_date
        ],200);
    }

    /**
     * @api {post} /lists/{list_id}/linked-date [step 7] add Linked Date
     * @apiName [step 7] add Linked Date
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Date='yyyy-mm-dd'} [start_date] Start Date.
     * @apiParam {Date='yyyy-mm-dd'} [end_date] End Date.
     * @apiParam {Number} [discount] discount.
     */
    public function store($list_id,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date'         => 'required|date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'required|date|date_format:Y-m-d|after:start_date',
            'discount'         => 'required|integer|min:0|max:100',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.parameters_not_correct'),
                'result' => $validator->errors()
            ],400);
        }

        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $discount = $request->input('discount');

        $check_dates = $this->checkDates($list_id,$start_date,$end_date);

        if(count($check_dates) > 0){
            return response()->json([
                'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
            ],400);
        }

        $linked_dates = new ExceptionDate([
            'list_id' => $list_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'status' => 1,
            'reason' => 1,
            'discount' => $discount,
            'amount' => 1
        ]);

        if($linked_dates->save()){
            return response()->json([
                'message' => __('listing/api.add_linked_days_list_success'),
                'result' => $linked_dates
            ],201);
        }else{
            return response()->json([
                'message' => __('listing/api.add_linked_days_list_error'),
            ],500);
        }

    }

    /**
     * @api {get} /lists/{list_id}/linked-date/{linked_date_id} [step 7] Get Linked Date by id
     * @apiName [step 7] Get Linked Date by id
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} linked_date_id Id of the blocked date .
     */
    public function show($list_id, $id)
    {

        $linked_date = ExceptionDate::where('id', $id)->where('list_id', $list_id)->where('status', 1)->whereNull('deleted_at')->get();

        if (count($linked_date) === 0) {
            return response()->json([
                'message' => __('listing/api.linked_date_not_found'),
            ], 400);
        }

        $view_date = ExceptionDate::where('id',$id)->get();

        if ($view_date) {
            return response()->json([
                'message' => __('listing/api.get_linked_date_list_success'),
                'result' => $view_date,
            ], 200);
        } else {
            return response()->json([
                'message' => __('listing/api.get_linked_date_list_error'),
            ], 500);
        }
    }

    /**
     * @api {patch} /lists/{list_id}/linked-date/{linked_date_id} [step 7] update Linked Date
     * @apiName [step 7] update Linked Date
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} linked_date_id Id of the Linked Date.
     * @apiParam {Date='yyyy-mm-dd'} [start_date] Start Date.
     * @apiParam {Date='yyyy-mm-dd'} [end_date] End Date.
     * @apiParam {Number} [discount] discount.
     */
    public function update($list_id, Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'start_date'         => 'date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'date|date_format:Y-m-d',
            'discount'         => 'integer|min:0|max:100',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.parameters_not_correct'),
                'result' => $validator->errors()
            ],400);
        }

        $date_id = ExceptionDate::where('id',$id)->where('list_id',$list_id)->where('status',1)->get();

        if(count($date_id) === 0){
            return response()->json([
                'message' => __('listing/api.date_id_not_found'),
            ],400);
        }

        $linked_dates = ExceptionDate::find($id);

        if($request->input('start_date')){
            if($request->input('end_date')){
                if(strtotime($request->input('start_date')) >= strtotime($request->input('end_date'))){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }

                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }else{
                if(strtotime($request->input('start_date')) >= strtotime($linked_dates->end_date)){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }
                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$linked_dates->end_date);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }
            $linked_dates->start_date = $request->input('start_date');
        }

        if($request->input('end_date')){
            if($request->input('start_date')){
                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }else{
                $check_dates = $this->checkDates($list_id,$linked_dates->start_date,$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }
            $linked_dates->end_date = $request->input('end_date');
        }

        if($request->input('discount')){
            $linked_dates->discount = $request->input('discount');
        }

        if($linked_dates->update()){
            return response()->json([
                'message' => __('listing/api.update_linked_days_list_success'),
                'result' => $linked_dates
            ],201);
        }else{
            return response()->json([
                'message' => __('listing/api.update_linked_days_list_error'),
            ],500);
        }

    }

    /**
     * @api {delete} /lists/{list_id}/linked-date/{linked_date_id} [step 7] Delete Linked Date by id
     * @apiName [step 7] Delete Linked Date by id
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} linked_date_id Id of the blocked date .
     */
    public function destroy($list_id, $id)
    {
        $linked_date = ExceptionDate::where('id', $id)->where('list_id', $list_id)->where('status', 1)->whereNull('deleted_at')->get();

        if (count($linked_date) === 0) {
            return response()->json([
                'message' => __('listing/api.linked_date_not_found'),
            ], 400);
        }

        $delete_date = ExceptionDate::where('id',$id)->delete();

        if ($delete_date) {
            return response()->json([
                'message' => __('listing/api.delete_linked_date_list_success'),
            ], 200);
        } else {
            return response()->json([
                'message' => __('listing/api.delete_linked_date_list_error'),
            ], 500);
        }
    }

    public function checkDates($list_id,$start_date,$end_date){
        $dates = getAllDatesBetweenSpecificDates($start_date,$end_date);
        $exception_days = ExceptionDate::where('list_id',$list_id)->get();
        $results = array();
        foreach ($exception_days as $key => $value){
            $exception_days_dates = getAllDatesBetweenSpecificDates($value['start_date'],$value['end_date']);
            $results[] = array_intersect($exception_days_dates,$dates);
        }
        $results_arr = array();
        foreach ($results as $key1 => $value1){
            foreach ($value1 as $key2 => $value2){
                $results_arr[] = $value2;
            }
        }
        return array_unique($results_arr);
    }
}
