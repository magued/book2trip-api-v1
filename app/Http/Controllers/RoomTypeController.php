<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomType;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\App;
/**
 * @resource GET Static Lists
 *
 * Here You Can Get All Static Lists.
 */
class RoomTypeController extends Controller
{
  /**
   * @api {get} /room-types?:locale:pagination Get All Room Types
   * @apiName Get All Room Types
   * @apiGroup Static Lists
   *
   * @apiParam {Number} [pagination=5] Number Of room types that you want to return .
   * @apiParam {String=en,ar} locale Symbol of locales.
   */
  /**
   * List Room types.
   *
   * @param  \App\Currency  $currencies
   * @return \Illuminate\Http\Response
   */
  public function getRoomTypes() {

      (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 5;

      $result = RoomType::paginate($pagination);

      $result->setPath(route('room-types.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

      return [
        'message'         => __('api.roomTypes_success'),
        'result'          => $result
      ];
      return response()->json($response, 200);

   }
}
