<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExceptionDate;
use App\Listing;
use Validator;

class ListingCalendarController extends Controller
{
    /**
     * @api {post} /lists/{list_id}/calender Add List Calender [step 7]
     * @apiName Add List Calender [step 7]
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} title The price of Night .
     * @apiParam {Number} price id of currencies .
     * @apiParam {Number} minimum_stay The Number of minimum stay .
     * @apiParam {Number} [weekend_fees] fees of the weekend .
     * @apiParam {Number} [weekly_discount] The value of weekly discount .
     * @apiParam {Number} [monthly_discount] The value of monthly discount .
     */
    public function postListingCalendar(Request $request, $list_id)
    {

        $listing = Listing::find($list_id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ],400);
        }

        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $check_linking = Listing::where("member_id" , $user_id)->where("id",$list_id)->first();

        if(count($check_linking) === 0){
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ],401);
        }

        $get_steps =  explode(",", $listing->step);

        if(!in_array("7", $get_steps)){

            $validator = Validator::make($request->all(), [
                'start_date'        => 'required|date',
                'end_date'          => 'required|date',
                'status'            => 'required|in:1,2',
                'discount'          => 'numeric',
            ]);

            if ($validator->fails())
            {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Parameters Not Correct',
                    'result' => $validator->errors()
                ]);
            }

            $exception_date = new ExceptionDate;

            $exception_date->start_date     =    $request->input('start_date');
            $exception_date->end_date       =    $request->input('end_date');
            $exception_date->status         =    $request->input('status');
            $exception_date->reason         =    2 ;
            $exception_date->list_id         =   $list_id ;

            if ($request->input('status') == 1) {
                $exception_date->discount   =    $request->input('discount');
            }

            if($exception_date->save()){

                $listing->step              =    $listing->step.',7';
                $listing->update();

                $response = [
                    'status' => 'success',
                    'msg' => 'Listing Dates Successfully Updated',
                    'result' => $listing
                ];
                return response()->json($response, 200);
            }

            else {
                $response = [
                    'status' => 'error',
                    'msg' => 'An error occurred while Updating Listing Dates'
                ];
                return response()->json($response, 404);
            }

        } else {
            $response = [
                'message' => __('listing/api.list_rate_added_before')
            ];
            return response()->json($response, 400);
        }
    }

    /**
     * @api {patch} /lists/{list_id}/calender add List calender
     * @apiName add List season
     * @apiGroup Lists
     *
     */
    public function updateListingCalendar(Request $request, $list_id, $exception_date_id)
    {
      $listing = Listing::find($list_id);
      if ($listing == '') {
          return response()->json([
              'status' => 'error',
              'msg' => 'Listing Not Exist',
              'result' => ''
          ]);
      }

      $get_steps =  explode(",", $listing->step);

      if(in_array("7", $get_steps)){

          $validator = Validator::make($request->all(), [
              'start_date'        => 'date',
              'end_date'          => 'date',
              'status'            => 'in:1,2',
              'discount'          => 'numeric',
          ]);

          if ($validator->fails())
          {
              return response()->json([
                  'status' => 'error',
                  'msg' => 'Parameters Not Correct',
                  'result' => $validator->errors()
              ]);
          }

          $exception_date = ExceptionDate::find($exception_date_id);
          if ($exception_date == '') {
              return response()->json([
                  'status' => 'error',
                  'msg' => 'Listing Dates Not Exist',
                  'result' => ''
              ]);
          }

          $exception_date->update($request->all());

          if($exception_date->update()){

              $response = [
                  'status' => 'success',
                  'msg' => 'Listing Dates Successfully Updated',
                  'result' => $listing
              ];
              return response()->json($response, 200);
          }

          else {
              $response = [
                  'status' => 'error',
                  'msg' => 'An error occurred while Updating Listing Dates'
              ];
              return response()->json($response, 404);
          }

      }

      else {
          $response = [
              'status' => 'error',
              'msg' => 'The User have stored his Listing Dates before'
          ];
          return response()->json($response, 404);
      }
    }
}
