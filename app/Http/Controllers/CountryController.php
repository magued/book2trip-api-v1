<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

/**
* @resource Static Lists
 *
 * Here You Can Get All Static Lists.
 */
class CountryController extends Controller
{

  /**
   * @api {get} /countries?:locale:pagination Get All Countries
   * @apiName Get All Countries
   * @apiGroup Static Lists
   *
   * @apiParam {Number} [pagination=10] Number Of countries that you want to return .
   * @apiParam {String=en,ar} locale Symbol of locales.
   */
  /**
   * List paginated countries
   *
   * @param  \App\Country  $countries
   * @return \Illuminate\Http\Response
   */
    public function getCountries() {

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $result = Country::where('availability', 1)->paginate($pagination);

        $result->setPath(route('countries.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        return [
          'message'         => __('api.countriesAll_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);

    }

}
