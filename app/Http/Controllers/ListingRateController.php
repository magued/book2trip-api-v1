<?php

namespace App\Http\Controllers;

use App\AdminRule;
use Illuminate\Http\Request;
use App\Listing;
use App\ListingRule;
use App\MemberRule;
use Validator;
use Input;
use JWTAuth;
use JWTException;

class ListingRateController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'postListingRate','updateListingRate'
        ]]);
        $this->middleware('check.list.access');
    }

    /**
     * @api {post} /lists/{list_id}/rate [step 5] Add List Rate
     * @apiName [step 5] Add List Rate
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} night_rate The price of Night .
     * @apiParam {Number} currency_id id of currencies .
     * @apiParam {Number} minimum_stay The Number of minimum stay .
     * @apiParam {Number} [weekend_fees] fees of the weekend .
     * @apiParam {String[]} [weekend_days] days of the weekend example 1,0 values 0 is sunday , 1 for Monday , 2 for Tuesday , 3 for Wednesday , 4 for Thursday , 5 for Friday , 6 for Saturday .
     * @apiParam {Number} [weekly_discount] The value of weekly discount .
     * @apiParam {Number} [monthly_discount] The value of monthly discount .
     * @apiParam {Number} [additional_guest] The price of additional guest .
     * @apiParam {Number} [cleaning_fees] The price of cleaning .
     * @apiParam {Number} [security_fees] The price of security .
     * @apiParam {String[]} [rules] ids of rules You can get it form <a href="#api-Static_Lists-Get_All_Listing_Rules">Get All Listing Rules</a> example 1,2 in case no rules send 0 .
     * @apiParam {String} [member_rule] more rules from member.
     */
    public function postListingRate(Request $request, $list_id)
    {
        $listing = Listing::find($list_id);

        $get_steps =  explode(",", $listing->step);

        if(!in_array("5", $get_steps)){
            $validator = Validator::make($request->all(), [
                'night_rate'        => 'required|numeric|min:0',
                'currency_id'       => 'required|exists:currencies,id',
                'minimum_stay'      => 'required|numeric',
                'weekend_fees'      => 'numeric|max:night_rate',
                'weekly_discount'   => 'numeric',
                'monthly_discount'  => 'numeric',
                'additional_guest'  => 'numeric',
                'cleaning_fees'     => 'numeric',
                'security_fees'     => 'numeric',
            ]);

            if ($validator->fails())
            {
                return response()->json([
                    'message' => __('listing/api.add_rate_to_list_parametes_not_correct'),
                    'error_fields' => $validator->errors()
                ],400);
            }

            $listing->price           =    $request->input('night_rate');
            $listing->currency_id     =    $request->input('currency_id');
            $listing->minimum_stay    =    $request->input('minimum_stay');
            $listing->step            =    $listing->step.',5,6,7';

            if($request->input('weekend_fees')){
                $listing->weekend_fees    =    $request->input('weekend_fees');
            }

            if($request->input('weekend_days')){
                $days   =   str_replace(',','|',$request->input('weekend_days'));
                $listing->weekend_days    = $days;
            }

            if($request->input('weekly_discount')){
                $listing->weekly_discount    =    $request->input('weekly_discount');
            }

            if($request->input('monthly_discount')){
                $listing->monthly_discount    =    $request->input('monthly_discount');
            }

            if($request->input('additional_guest')){
                $listing->additional_guest    =    $request->input('additional_guest');
            }

            if($request->input('cleaning_fees')){
                $listing->cleaning_fees    =    floatval($request->input('cleaning_fees'));
            }

            if($request->input('security_fees')){
                $listing->security_fees    =    $request->input('security_fees');
            }

            if($listing->update()){
                if($request->input('rules') !== NULL) {
                    $rules = $request->input('rules');
                    $rules_array = array_map('intval', explode(',', $rules));
                    if (count($rules_array) === 1 && $rules_array[0] === 0) {
                        $delete_rules = ListingRule::where('list_id', $list_id)->delete();
                    } else {
                        $array_check = $this->checkRules($rules_array);
                        if (count($array_check) > 0) {
                            $response = [
                                'message' => __('listing/api.this_ids_not_correct') . ' ' . implode(',', $array_check),
                            ];
                            return response()->json($response, 400);
                        }
                        foreach ($rules_array as $key => $value) {
                            $listing_rule = new ListingRule();
                            $listing_rule->rule_id = intval($value);
                            $listing_rule->list_id = intval($list_id);
                            $listing_rule->save();
                        }
                    }
                }

                $listingMemberRule = new MemberRule([
                    'list_id'     => $list_id,
                    'rule'  => $request->input('member_rule')
                ]);
                $listingMemberRule->save();

                $response = [
                    'message' => __('listing/api.list_rate_added_success'),
                    'result' => $listing
                ];
                return response()->json($response, 200);
                } else {
                    $response = [
                        'message' => __('listing/api.list_rate_added_error')
                    ];
                    return response()->json($response, 500);
                }
        } else {
            $response = [
                'message' => __('listing/api.list_rate_added_before')
            ];
            return response()->json($response, 400);
        }
    }

    /**
     * @api {patch} /lists/{list_id}/rate [step 5] Update List Rate
     * @apiName [step 5] Update List Rate
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} [night_rate] The price of Night .
     * @apiParam {Number} [currency_id] id of currencies .
     * @apiParam {Number} [minimum_stay] The Number of minimum stay .
     * @apiParam {Number} [weekend_fees] fees of the weekend .
     * @apiParam {String[]} [weekend_days] days of the weekend example 1,0 values 0 is sunday , 1 for Monday , 2 for Tuesday , 3 for Wednesday , 4 for Thursday , 5 for Friday , 6 for Saturday .
     * @apiParam {Number} [weekly_discount] The value of weekly discount .
     * @apiParam {Number} [monthly_discount] The value of monthly discount .
     * @apiParam {Number} [additional_guest] The price of additional guest .
     * @apiParam {Number} [cleaning_fees] The price of cleaning .
     * @apiParam {Number} [security_fees] The price of security .
     * @apiParam {String[]} [rules] ids of rules You can get it form <a></a>.
     * @apiParam {String} [member_rule] more rules from member.
     */
    public function updateListingRate(Request $request, $list_id)
    {
        $listing = Listing::find($list_id);

        $get_steps =  explode(",", $listing->step);

        if(in_array("5", $get_steps)){
            $validator = Validator::make($request->all(), [
                'night_rate'        => 'numeric|min:0',
                'currency_id'       => 'exists:currencies,id',
                'minimum_stay'      => 'numeric',
                'weekend_fees'      => 'numeric',
                'weekly_discount'   => 'numeric',
                'monthly_discount'  => 'numeric',
                'additional_guest'  => 'numeric',
                'cleaning_fees'     => 'numeric',
                'security_fees'     => 'numeric',
            ]);

            if ($validator->fails())
            {
                return response()->json([
                    'message' => __('listing/api.add_rate_to_list_parametes_not_correct'),
                    'error_fields' => $validator->errors()
                ],400);
            }

            if($request->input('night_rate')){
                if($request->input('weekend_fees')){
                    if($request->input('night_rate') > $request->input('weekend_fees')){
                        return response()->json([
                            'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                        ],400);
                    }
                }else{
                    if($listing->weekend_fees > $request->input('night_rate')){
                        return response()->json([
                            'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                        ],400);
                    }
                }
                $listing->price           =    $request->input('night_rate');
            }

            if($request->input('currency_id')){
                $listing->currency_id     =    $request->input('currency_id');
            }

            if($request->input('minimum_stay')){
                $listing->minimum_stay    =    $request->input('minimum_stay');
            }

            if($request->input('weekend_fees')){
                if($request->input('night_rate')){
                    if($request->input('night_rate') > $request->input('weekend_fees')){
                        return response()->json([
                            'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                        ],400);
                    }
                }else{
                    if($listing->price > $request->input('weekend_fees')){
                        return response()->json([
                            'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                        ],400);
                    }
                }
                $listing->weekend_fees    =    $request->input('weekend_fees');
            }

            if($request->input('weekend_days')){
                $days   =   str_replace(',','|',$request->input('weekend_days'));
                $listing->weekend_days    = $days;
            }

            if($request->input('weekly_discount')){
                $listing->weekly_discount    =    $request->input('weekly_discount');
            }

            if($request->input('monthly_discount')){
                $listing->monthly_discount    =    $request->input('monthly_discount');
            }

            if($request->input('additional_guest')){
                $listing->additional_guest    =    $request->input('additional_guest');
            }

            if($request->input('cleaning_fees')){
                $listing->cleaning_fees    =    floatval($request->input('cleaning_fees'));
            }

            if($request->input('security_fees')){
                $listing->security_fees    =    $request->input('security_fees');
            }

            if($listing->update()){

                if($request->input('rules') !== NULL) {
                    $rules = $request->input('rules');
                    $rules_array = array_map('intval', explode(',', $rules));
                    if (count($rules_array) === 1 && $rules_array[0] === 0) {
                        $delete_rules = ListingRule::where('list_id', $list_id)->delete();
                    } else {
                        $delete_rules = ListingRule::where('list_id', $list_id)->delete();
                        $array_check = $this->checkRules($rules_array);
                        if (count($array_check) > 0) {
                            $response = [
                                'message' => __('listing/api.this_ids_not_correct') . ' ' . implode(',', $array_check),
                            ];
                            return response()->json($response, 400);
                        }
                        foreach ($rules_array as $key => $value) {
                            $listing_rule = new ListingRule();
                            $listing_rule->rule_id = intval($value);
                            $listing_rule->list_id = intval($list_id);
                            $listing_rule->save();
                        }
                    }
                }

                if($request->input('member_rule')) {
                    $listingMemberRule = ListingRule::where('list_id',$list_id);
                    $listingMemberRule->rule = $request->input('member_rule');
                    $listingMemberRule->save();
                }

                $response = [
                    'message' => __('listing/api.list_rate_added_success'),
                    'result' => $listing
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'message' => __('listing/api.list_rate_added_error')
                ];
                return response()->json($response, 500);
            }
        } else {
            $response = [
                'message' => __('listing/api.list_rate_added_not_before')
            ];
            return response()->json($response, 400);
        }
    }

    public function checkRules($array){
        $exist = array();
        foreach ($array as $key => $value){
            $amenity = AdminRule::where('id',$value)->where('deleted_at',null)->first();
            if($amenity === null){
                $exist[] = $value;
            }
        }
        return $exist;
    }
}
