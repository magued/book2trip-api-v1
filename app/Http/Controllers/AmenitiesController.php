<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amenity;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class AmenitiesController extends Controller
{
    /**
     * @api {get} /amenities?:locale:pagination Get All Amenities
     * @apiName Get All Amenities
     * @apiGroup Static Lists
     *
     * @apiParam {Number} [pagination=10] Number Of cities that you want to return .
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function getAmenities()
    {
        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $result = Amenity::paginate($pagination);

        $result->setPath(route('amenities'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        $response = [
          'message'         => __('api.amenitiesAll_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);

    }

    /**
     * @api {get} /amenities/:category_id?:locale:pagination Get All Amenities In Category
     * @apiName Get All Amenities In Category
     * @apiGroup Static Lists
     *
     * @apiParam {Number} [pagination=10] Number Of cities that you want to return .
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} category_id Id of category You Can get it from <a href="#api-Static_Lists-Get_All_Amenities_Categories">Get All Amenities Categories</a> .
     */
    public function getAmenitiesByCategory($category_id){

      (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

      $result = Amenity::where('category_id', $category_id)->paginate($pagination);

      $result->setPath(route('amenities.category',['category_id'=> $category_id]))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

      $response = [
        'message'         => __('api.amenitiesByCategories_success'),
        'result'          => $result
      ];
      return response()->json($response, 200);

    }


}
