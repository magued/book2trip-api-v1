<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use Illuminate\Support\Facades\Input;

/**
 * @resource Static Lists
 *
 * Here You Can Get All Static Lists.
 */
class CurrencyController extends Controller
{

    /**
     * @api {get} /currencies?:locale:pagination Get All Currencies
     * @apiName Get All Currencies
     * @apiGroup Static Lists
     *
     * @apiParam {Number} [pagination=5] Number Of currencies that you want to return .
     * @apiParam {String=en,ar} locale Symbol of locales.
     */

   /**
    * List paginated currencies
    *
    * @param  \App\Currency  $currencies
    * @return \Illuminate\Http\Response
    */
    public function getCurrencies() {

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 5;

        $result = Currency::paginate($pagination);

        $result->setPath(route('currencies.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        return [
          'status'          => 'Success',
          'msg'             => 'Data retrieved properly',
          'result'          => $result
        ];

    }
}
