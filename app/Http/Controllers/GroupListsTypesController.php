<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupListsTypesController extends Controller
{
    /**
     * @api {get} /group-lists-types?:locale Get Group Lists Types
     * @apiName Get Group Lists Types
     * @apiGroup Static Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function getGroupListsTypes()
    {
        $result = [
            ['id' => 1,
                'title' => __('listing/api.resort')],
            ['id' => 2,
                'title' => __('listing/api.building')],
            ['id' => 3,
                'title' => __('listing/api.hotel')]
        ];


        return [
          'message'         => __('listing/api.get_group_lists_types_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);
    }
}
