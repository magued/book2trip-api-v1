<?php

namespace App\Http\Controllers;

use App\Amenity;
use Illuminate\Http\Request;
use App\Listing;
use App\ListingAmenity;
use JWTAuth;
use Validator;

class ListingAmenityController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'getUpdateAmenities','getAllAmenities'
        ]]);
        $this->middleware('check.list.access');
    }

    /**
     * @api {get} /lists/{list_id}/amenities?:locale [step 4] get List Amenities
     * @apiName [step 4] get List Amenities
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales
     * @apiParam {Number} list_id id of the list
     */
    public function getAllAmenities($list_id){
        $amenities = ListingAmenity::where('list_id',$list_id)->get();
        return response()->json([
            'message' => __('listing/api.get_amenities_list_success'),
            'result'  => $amenities
        ],200);
    }

    /**
     * @api {patch} /lists/{list_id}/amenities [step 4] update List Amenities
     * @apiName [step 4] update List Amenities
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales
     * @apiParam {String[]} [amenities_ids] ids of amenities example 21,22 or "no" in case delete all amenities.
     * @apiParam {Number} list_id id of the list
     */
    public function getUpdateAmenities($list_id, Request $request)
    {
        $list = Listing::find($list_id);
        $validator = Validator::make($request->all(), [
            'amenities_ids'         => 'required',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_amenities_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }
        $get_steps =  explode(",", $list->step);
        if(!in_array("4", $get_steps)) {
            $list->step = $list->step.',4';
            $list->update();
        }
        $amenities_ids = $request->input('amenities_ids');
        if($amenities_ids == 'no'){
            $delete_amentites = ListingAmenity::where('list_id',$list_id)->delete();
            $response = [
                'message' => __('listing/api.update_listing_amenities_success'),
                'result'  => $list
            ];
            return response()->json($response, 200);
        }else{
            $amenities_array = array_map('intval',explode(',', $amenities_ids));
            $array_check = $this->checkAmenities($amenities_array);
            if(count($array_check) > 0){
                $response = [
                    'message' => __('listing/api.this_ids_not_correct').' '.implode(',',$array_check),
                ];
                return response()->json($response, 400);
            }
            $amenities_exsit = ListingAmenity::where('list_id',$list_id)->where('deleted_at',null)->get()->toArray();
            $amenities_in_table = array();
            foreach ($amenities_exsit as $key => $value){
                $amenities_in_table[$key] = $value['amenity_id'];
            }
            $amenities_add = array_diff($amenities_array,$amenities_in_table);
            $amenities_remove = array_diff($amenities_in_table,$amenities_array);
            if(!in_array(0, $amenities_add)){
                foreach ($amenities_add as $key_add => $value_add){
                    $listing_amenity = new ListingAmenity();
                    $listing_amenity->amenity_id = $value_add;
                    $listing_amenity->list_id = $list_id;
                    $listing_amenity->save();
                }
            }
            if(count($amenities_remove)>0){
                foreach ($amenities_remove as $key_remove => $value_remove){
                    ListingAmenity::where('list_id',$list_id)->where('amenity_id',$value_remove)->where('deleted_at', null)->delete();
                }
            }
            $response = [
                'message' => __('listing/api.update_listing_amenities_success'),
                'result'  => $list
            ];
            return response()->json($response, 200);
        }
    }

    public function checkAmenities($array){
        $exist = array();
        foreach ($array as $key => $value){
            $amenity = Amenity::where('id',$value)->where('deleted_at',null)->first();
            if($amenity === null){
                $exist[] = $value;
            }
        }
        return $exist;
    }
}
