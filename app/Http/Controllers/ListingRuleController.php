<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminRule;
use Illuminate\Support\Facades\Input;

class ListingRuleController extends Controller
{
    /**
     * @api {get} /listing-rules?:locale Get All Listing Rules
     * @apiName Get All Listing Rules
     * @apiGroup Static Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function getListingRules()
    {
        $result = AdminRule::get();
        $response = [
          'message'         => __('api.get_listing_rules_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);
    }

}
