<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

/**
 * @resource GET Static Lists
 *
 * Here You Can Get All Static Lists.
 */
class ListingManagerTypeController extends Controller
{
  /**
   * @api {get} /listing-manager-types?:locale Get All Listing Manager Types
   * @apiName Get All Listing Manager Types
   * @apiGroup Static Lists
   *
   * @apiParam {String=en,ar} locale Symbol of locales.
   */
   public function getListingManagerTypes() {
        $result = [
              ['id' => 1,
               'title' => __('api.owner')],
              ['id' => 2,
               'title' => __('api.manager')]
          ];

        return [
          'message'         => __('api.listingManagerTypes_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);

    }
}
