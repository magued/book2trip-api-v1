<?php

namespace App\Http\Controllers;

use App\AbstractClasses\Search;
use App\Amenity;
use App\GroupAmenity;
use App\GroupListing;
use App\Listing;
use App\ListingPhoto;
use App\ListBedType;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use JWTAuth;
use JWTException;

class GroupListingController extends Search
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'store','update'
        ]]);
    }

    public function checkAmenities($array){
        $exist = array();
        foreach ($array as $key => $value){
            $amenity = Amenity::where('id',$value)->where('category_id',1)->where('deleted_at',null)->first();
            if($amenity === null){
                $exist[] = $value;
            }
        }
        return $exist;
    }

    /**
     * @api {post} /group-lists Create Group List
     * @apiName Create Group List
     * @apiGroup Group Lists
     *
     * @apiParam {String{..66}} title Title of the list .
     * @apiParam {Number} multi_unit_type_id Id of Multi-unit type <a href="#api-Static_Lists-Get_Group_Lists_Types">Get Group Lists Types</a> .
     * @apiParam {Number} [photo] base64 of the photo .
     * @apiParam {Number} [website] website of the list .
     * @apiParam {Number} unit_number no of the units.
     * @apiParam {String} [check_in] checkin time ex 10:30:AM
     * @apiParam {String} [check_out] checkout time ex 10:30:PM
     * @apiParam {String} address address of the list .
     * @apiParam {String} country country of the list.
     * @apiParam {String} governorate governorate of the list.
     * @apiParam {String} city city of the list.
     * @apiParam {String} country_short short code of list country.
     * @apiParam {String} latitude latitude of list.
     * @apiParam {String} longitude longitude of list.
     * @apiParam {Number} [building_number] number of the building .
     * @apiParam {Number} [zip_code] code of the zip .
     * @apiParam {Number} [property_number] number of the property .
     * @apiParam {String} [landmark] landmark of the list .
     * @apiParam {String[]} [amenities_ids] ids of amenities example 21,22.
     */
    public function store(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:66',
            'multi_unit_type_id'    => 'required|in:1,2,3',
            'unit_number'   => 'required|numeric|min:1',
            'address'       => 'required',
            'country'       => 'required',
            'governorate'     => 'required',
            'city'          => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required',
            'country_short' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }
        if($request->input('amenities_ids')){
            $amenities_ids = $request->input('amenities_ids');
            $amenities_array = array_map('intval',explode(',', $amenities_ids));
            $array_check = $this->checkAmenities($amenities_array);

            if(count($array_check) > 0){
                $response = [
                    'message' => __('listing/api.this_ids_not_correct').' '.implode(',',$array_check),
                ];
                return response()->json($response, 400);
            }

        }

        $listing['name'] = $request->input('title');
        $listing['member_id'] = $user_id;
        $listing['type'] = $request->input('multi_unit_type_id');
        $listing['total_lists'] = $request->input('unit_number');
        $listing['address'] = $request->input('address');
        $listing['governorate'] = $request->input('governorate');
        $listing['city'] = $request->input('city');
        $listing['country'] = $request->input('country');
        $listing['country_short'] = $request->input('country_short');
        $listing['latitude'] = $request->input('latitude');
        $listing['longitude'] = $request->input('longitude');
        $listing['step'] = '1,2,3';

        if($request->input('photo')){
            $image = $request->input('photo');
            $directory = 'grouplisting';
            $sizes = [270, 51];
            $listing['picture'] = uploadAndSave($image, $directory, $sizes);
        }else{
            $listing['picture'] = '';
        }

        if($request->input('website')){
            $listing['website'] = $request->input('website');
        }else{
            $listing['website'] = '';
        }

        if($request->input('check_in')){
            $listing['check_in'] = $request->input('check_in');
        }else{
            $listing['check_in'] = '';
        }

        if($request->input('check_out')){
            $listing['check_out'] = $request->input('check_out');
        }else{
            $listing['check_out'] = '';
        }

        if($request->input('building_number')){
            $listing['building_number'] = $request->input('building_number');
        }else{
            $listing['building_number'] = '';
        }

        if($request->input('zip_code')){
            $listing['zip_code'] = $request->input('zip_code');
        }else{
            $listing['zip_code'] = '';
        }

        if($request->input('property_number')){
            $listing['unit_number'] = $request->input('property_number');
        }else{
            $listing['unit_number'] = '';
        }

        if($request->input('landmark')){
            $listing['more_place_desc'] = $request->input('landmark');
        }else{
            $listing['more_place_desc'] = '';
        }

        $listing_save = new GroupListing($listing);

        if($listing_save->save()){
            if($request->input('amenities_ids')){
                $amenities_ids = $request->input('amenities_ids');
                $amenities_array = array_map('intval',explode(',', $amenities_ids));
                foreach ($amenities_array as $key => $value){
                    $listing_amenity = new GroupAmenity();
                    $listing_amenity->amenity_id = $value;
                    $listing_amenity->group_id = $listing_save->id;
                    $listing_amenity->save();
                }
            }
            $response = [
                'message' => __('listing/api.list_creation_success'),
                'result'  => $listing_save,
            ];
            return response()->json($response, 200);
        }

        $response = [
            'message' => __('listing/api.list_creation_failure'),
        ];

        return response()->json($response, 500);

    }

    /**
     * @api {get} /group-lists/:id?:locale:currency_id Show Group List Details
     * @apiName Show Group Lists
     * @apiGroup Group Lists
     *
     * @apiParam {Number} id Id of the group list .
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} currency_id The id of currency <a href="#api-Static_Lists-Get_All_Currencies">Get All Currencies</a>.
     */
    public function show($id)
    {
        if(Input::get('currency_id')){

            $list = GroupListing::find($id);

            if ($list) {

                if(isset(\Illuminate\Support\Facades\Request::header()['authorization'])){
                    try{
                        if(! $user = JWTAuth::parseToken()->authenticate()){
                            return response()->json(['message'=> __('listing/api.invalid_credentials')], 401);
                        }
                    }catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
                        return response()->json(['message'=> __('listing/api.token_expired')], 401);
                    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                        return response()->json(['message'=> __('listing/api.token_invalid')], 401);
                    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                        return response()->json(['message'=> __('listing/api.jwt_exception')], 401);
                    }
                }

                $response = [
                    'message'     => __('listing/api.show_list_success'),
                    'result'      => $list,
                ];
                return response()->json($response, 200);
            }
            else {
                $response = [
                    'message'     => __('listing/api.list_not_found'),
                ];
                return response()->json($response, 404);
            }

        }else{

            $response = [
                'message'     => __('listing/api.currency_not_found'),
            ];

            return response()->json($response, 400);

        }
    }

    /**
     * @api {patch} /group-lists/:list_id Update Group List
     * @apiName Update Group List
     * @apiGroup Group Lists
     *
     * @apiParam {Number} list_id Id of lists.
     * @apiParam {String{..66}} [title] Title of the list .
     * @apiParam {Number} [multi_unit_type_id] Id of Multi-unit type <a href="#api-Static_Lists-Get_Group_Lists_Types">Get Group Lists Types</a> .
     * @apiParam {Number} [photo] base64 of the photo .
     * @apiParam {Number} [website] website of the list .
     * @apiParam {Number} [unit_number] no of the units.
     * @apiParam {String} [check_in] checkin time ex 10:30:AM
     * @apiParam {String} [check_out] checkout time ex 10:30:PM
     * @apiParam {String} [address] address of the list .
     * @apiParam {String} [country] country of the list.
     * @apiParam {String} [governorate] governorate of the list.
     * @apiParam {String} [city] city of the list.
     * @apiParam {String} [country_short] short code of list country.
     * @apiParam {String} [latitude] latitude of list.
     * @apiParam {String} [longitude] longitude of list.
     * @apiParam {Number} [building_number] number of the building .
     * @apiParam {Number} [zip_code] code of the zip .
     * @apiParam {Number} [property_number] number of the property .
     * @apiParam {String} [landmark] landmark of the list .
     * @apiParam {String[]} [amenities_ids] ids of amenities example 21,22 or "no" in case delete all amenities.
     */
    public function update(Request $request, $id)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'title'         => 'max:66',
            'multi_unit_type_id'    => 'in:1,2,3',
            'unit_number'   => 'numeric|min:1',
        ]);

        $listing = GroupListing::find($id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ],400);
        }

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.update_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $member_list = GroupListing::where('id',$id)->where('member_id',$user_id);

        if ($member_list === NULL){
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ],401);
        }
        if($request->input('amenities_ids')){

            if($request->input('amenities_ids') == 'no'){
                $delete_amentites = GroupAmenity::where('group_id',$id)->delete();
            }else{
                $amenities_ids = $request->input('amenities_ids');
                $amenities_array = array_map('intval',explode(',', $amenities_ids));
                $array_check = $this->checkAmenities($amenities_array);
                if(count($array_check) > 0){
                    $response = [
                        'message' => __('listing/api.this_ids_not_correct').' '.implode(',',$array_check),
                    ];
                    return response()->json($response, 400);
                }

                $amenities_exsit = GroupAmenity::where('group_id',$id)->where('deleted_at',null)->get()->toArray();
                $amenities_in_table = array();
                foreach ($amenities_exsit as $key => $value){
                    $amenities_in_table[$key] = $value['amenities']['id'];
                }
                $amenities_add = array_diff($amenities_array,$amenities_in_table);
                $amenities_remove = array_diff($amenities_in_table,$amenities_array);
                if(!in_array(0, $amenities_add)){
                    foreach ($amenities_add as $key_add => $value_add){
                        $listing_amenity = new GroupAmenity();
                        $listing_amenity->amenity_id = $value_add;
                        $listing_amenity->group_id = $id;
                        $listing_amenity->save();
                    }
                }
                if(count($amenities_remove)>0){
                    foreach ($amenities_remove as $key_remove => $value_remove){
                        GroupAmenity::where('group_id',$id)->where('amenity_id',$value_remove)->where('deleted_at', null)->delete();
                    }
                }
            }
        }


        if($request['title']){
            $listing->name = $request['title'];
        }

        if($request['multi_unit_type_id']){
            $listing->type = $request['multi_unit_type_id'];
        }

        if($request['photo']){
            if(file_exists('uploads/grouplisting/'.$listing->picture)){
                unlink('uploads/grouplisting/'.$listing->picture);
            }
            if(file_exists('uploads/grouplisting/thumb/'.$listing->picture)){
                unlink('uploads/grouplisting/thumb/'.$listing->picture);
            }
            if(file_exists('uploads/grouplisting/medium/'.$listing->picture)){
                unlink('uploads/grouplisting/medium/'.$listing->picture);
            }
            $image = $request->input('photo');
            $directory = 'grouplisting';
            $sizes = [270, 51];
            $listing['picture'] = uploadAndSave($image, $directory, $sizes);
        }

        if($request['website']){
            $listing->website = $request['website'];
        }

        if($request['unit_number']){
            $listing->total_lists = $request['unit_number'];
        }

        if($request['check_in']){
            $listing->check_in = $request['check_in'];
        }

        if($request['check_out']){
            $listing->check_out = $request['check_out'];
        }

        if($request['address']){
            $listing->address = $request['address'];
        }

        if($request['country']){
            $listing->country = $request['country'];
        }

        if($request['governorate']){
            $listing->governorate = $request['governorate'];
        }

        if($request['city']){
            $listing->city = $request['city'];
        }

        if($request['country_short']){
            $listing->country_short = $request['country_short'];
        }

        if($request['latitude']){
            $listing->latitude = $request['latitude'];
        }

        if($request['longitude']){
            $listing->longitude = $request['longitude'];
        }

        if($request['building_number']){
            $listing->building_number = $request['building_number'];
        }

        if($request['zip_code']){
            $listing->zip_code = $request['zip_code'];
        }

        if($request['property_number']){
            $listing->unit_number = $request['property_number'];
        }

        if($request['landmark']){
            $listing->more_place_desc = $request['landmark'];
        }

        if($listing->update()){
            $response = [
                'message' => __('listing/api.list_updated_success'),
                'result' => $listing,
            ];
            return response()->json($response, 200);
        }else{
            $response = [
                'message' => __('listing/api.list_updated_failure'),
            ];
            return response()->json($response, 500);
        }
    }

}
