<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

/**
 * @resource Static Lists
 *
 * Here You Can Get All Static Lists.
 */
class CityController extends Controller
{
  /**
   * @api {get} /cities?:locale:pagination Get All Cities
   * @apiName Get All Cities
   * @apiGroup Static Lists
   *
   * @apiParam {Number} [pagination=10] Number Of cities that you want to return .
   * @apiParam {String=en,ar} locale Symbol of locales.
   */

  /**
   * List paginated cities
   *
   * @param  \App\City  $cities
   * @return \Illuminate\Http\Response
   */
    public function getCities() {

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $result = City::paginate($pagination);

        $result->setPath(route('cities.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        return [
          'message'         => __('api.citiesAll_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);

    }

}
