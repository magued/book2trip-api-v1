<?php

namespace App\Http\Controllers;

use App\AbstractClasses\Search;
use App\Amenity;
use App\GroupListing;
use App\Listing;
use App\ListingPhoto;
use App\ListBedType;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use JWTAuth;
use JWTException;

class ListingController extends Search
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'store','update'
        ]]);
    }

    /**
     * @api {get} /lists?:locale:pagination:guests:amenities:startDate:endDate:country:governorate:list_type_id:managed_by:beds:bedrooms:bathrooms:price:room_type_id:keyword:city:currency_id Get All Lists And Search
     * @apiName Get All Lists And Search
     * @apiGroup Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of cities that you want to return .
     * @apiParam {Number} [guests=1] Number Of guests .
     * @apiParam {String[]} [amenities] Ids Of Amenities You can get it from <a href="#api-Static_Lists-Get_All_Amenities_Categories">Get All Amenities Categories</a> and <a href="#api-Static_Lists-Get_All_Amenities_In_Category">Get All Amenities In Category</a>.
     * @apiParam {Date='yyyy-mm-dd'} [start_date] Start Date.
     * @apiParam {Date='yyyy-mm-dd'} [end_date] End Date.
     * @apiParam {String} [country] The name Of country .
     * @apiParam {String} [governorate] The name of governorate.
     * @apiParam {Number} [list_type_id] Id of the list types You can get it from <a href="#api-Static_Lists-Get_All_Listing_Type">Get All Listing Type</a>.
     * @apiParam {Number} [managed_by] Id Of List Managed by You can get it from <a href="#api-Static_Lists-Get_All_Listing_Manager_Types">Get All Listing Manager Types</a>.
     * @apiParam {Number} [beds] Number Of beds .
     * @apiParam {Number} [bedrooms] Number Of bedrooms .
     * @apiParam {Number} [bathrooms] Number Of bedrooms .
     * @apiParam {String} [price] Range Of price example 100,200 .
     * @apiParam {Number} [room_type_id] Id of the room types You can get it from <a href="#api-Static_Lists-Get_All_Room_Types">Get All Room Types</a>.
     * @apiParam {String} [keyword] The name Of keyword .
     * @apiParam {String} [city] The name of city .
     * @apiParam {String} currency_id The id of currency <a href="#api-Static_Lists-Get_All_Currencies">Get All Currencies</a>.
     */
    public function index()
    {
        $validator = Validator::make(Input::get(), [
            'guests'         => 'numeric|min:1',
            'start_date'         => 'date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'date|date_format:Y-m-d|after:start_date',
            'list_type_id'  => 'exists:list_types,id',
            'managed_by'  => 'in:1,2',
            'beds'  => 'numeric|min:1',
            'bedrooms'  => 'numeric|min:1',
            'bathrooms'  => 'numeric|min:1',
            'room_type_id'  => 'exists:room_type,id',
            'currency_id'  => 'required|exists:currencies,id',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.parameters_not_correct'),
                'result' => $validator->errors()
            ],400);
        }

        if(Input::get('amenities')){
            $amenities_array = explode(',',Input::get('amenities'));
            $array_check = $this->checkAmenities($amenities_array);
            if(count($array_check) > 0){
                $response = [
                    'message' => __('listing/api.this_ids_not_correct').' '.implode(',',$array_check),
                ];
                return response()->json($response, 400);
            }
        }

        if(Input::get('price')){
            $price_array = explode(',',Input::get('price'));
            if($price_array[0] > $price_array[1]){
                return response()->json([
                    'message' => __('listing/api.price_not_correct'),
                ],400);
            }
        }

        $keywords = Input::get();

        $lists = $this->searchQuery($keywords);

        if ($lists !== null) {
            if (isset(\Illuminate\Support\Facades\Request::header()['authorization'])) {
                try {
                    if (!$user = JWTAuth::parseToken()->authenticate()) {
                        return response()->json(['message' => __('listing/api.invalid_credentials')], 401);
                    }
                } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['message' => __('listing/api.token_expired')], 401);
                } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['message' => __('listing/api.token_invalid')], 401);
                } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['message' => __('listing/api.jwt_exception')], 401);
                }
            }
            $response = [
                'message' => __('api.listingSearch_success'),
                'result' => $lists
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'message' => __('api.listingSearch_error'),
            ];
            return response()->json($response, 500);
        }
    }

    public function checkAmenities($array){
        $exist = array();
        foreach ($array as $key => $value){
            $amenity = Amenity::where('id',$value)->where('deleted_at',null)->first();
            if($amenity === null){
                $exist[] = $value;
            }
        }
        return $exist;
    }

    /**
     * @api {post} /lists [step 1] Create List
     * @apiName [step 1] Create List
     * @apiGroup Add & Update Lists
     *
     * @apiParam {String{..66}} title Title of the list .
     * @apiParam {Number} managed_by Id of manager by you Can get from <a href="#api-Static_Lists-Get_All_Listing_Manager_Types">Get All Listing Manager Types</a> .
     * @apiParam {Number} room_type_id id of the room types you can get it from <a href="#api-Static_Lists-Get_All_Room_Types">Get All Room Types</a> .
     * @apiParam {Number} list_type_id id of the list types you can get it from <a href="#api-Static_Lists-Get_All_Listing_Type">Get All Listing Type</a>.
     * @apiParam {Number} bedrooms no of the bedrooms.
     * @apiParam {Number} phone phone of the member's list .
     * @apiParam {Number} bathrooms no of the bathrooms .
     * @apiParam {Number} guest no of the guests .
     * @apiParam {Number} single_bed no of the single_bed .
     * @apiParam {Number} double_bed no of the double_bed .
     * @apiParam {Number} sofa_bed no of the sofa_bed .
     * @apiParam {Number} group_id id of the group list .
     * @apiParam {String} [description] description of the list .
     */
    public function store(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:66',
            'managed_by'    => 'required|in:1,2',
            'room_type_id'  => 'required|exists:room_type,id',
            'list_type_id'  => 'required|exists:list_types,id',
            'bedrooms'      => 'required|numeric',
            'phone'         => 'required|numeric',
            'bathrooms'     => 'required|numeric',
            'guest'         => 'required|numeric',
            'single_bed'    => 'required|numeric',
            'double_bed'    => 'required|numeric',
            'sofa_bed'      => 'required|numeric',
            'group_id'      => 'required',
        ]);

        $beds = $request->input('single_bed') + $request->input('double_bed') + $request->input('sofa_bed');

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        } elseif ( $request->input('guest') < $beds )
        {
            return response()->json([
                'message' => __('listing/api.number_of_beds_not_correct'),
            ],400);
        }

        $group_id = $request->input('group_id');
        if($group_id != 0){
            $group = GroupListing::where('id', '=', $group_id)->first();
            if($group === null){
                return response()->json([
                    'message' => __('listing/api.adding_list_parameters_not_correct'),
                    'error_fields' => 'group_id not correct'
                ],400);
            }

        }

        $listing = new Listing([
          'title'         =>    $request->input('title'),
          'managed_by'    =>    $request->input('managed_by'),
          'room_type_id'  =>    $request->input('room_type_id'),
          'list_type_id'  =>    $request->input('list_type_id'),
          'bedrooms'      =>    $request->input('bedrooms'),
          'member_id'     =>    $user_id,
          'phone'         =>    $request->input('phone'),
          'bathrooms'     =>    $request->input('bathrooms'),
          'guest'         =>    $request->input('guest'),
          'beds'          =>    $beds,
          'description'   =>    $request->input('description'),
          'is_group'      =>    $request->input('group_id'),
          'group_id'      =>    $request->input('group_id'),
          'step'          =>    '1',
          'approved'      =>    '1',
          'building_number'=>   '',
          'more_place_desc'=>   '',
          'rate'=>   0.00,
          'video'=>   '',
          'zip_code'       =>    '',
          'country_short'       =>    '',
          'unit_number'    =>    '',
          'token'          =>    '',
          'viewed'         =>    0,
        ]);

        if($listing->save()){

            $listBedSingle = new ListBedType([
                'list_id'       =>    $listing->id,
                'bed_type_id'   =>    1,
                'amount'        =>    $request->input('single_bed'),
            ]);
            $listBedDouble = new ListBedType([
                'list_id'       =>    $listing->id,
                'bed_type_id'   =>    2,
                'amount'        =>    $request->input('double_bed'),
            ]);
            $listBedSofa = new ListBedType([
                'list_id'       =>    $listing->id,
                'bed_type_id'   =>    3,
                'amount'        =>    $request->input('sofa_bed'),
            ]);

            if($listBedSingle->save() && $listBedDouble->save() && $listBedSofa->save()){
                $response = [
                    'message' => __('listing/api.list_creation_success'),
                    'result'  => $listing,
                ];
              return response()->json($response, 200);
            }
        }

        $response = [
            'message' => __('listing/api.list_creation_failure'),
        ];

        return response()->json($response, 500);

    }

    /**
     * @api {get} /lists/:id?:locale:currency_id Show List Details
     * @apiName Show List Details
     * @apiGroup Lists
     *
     * @apiParam {Number} id Id of the list .
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} currency_id The id of currency <a href="#api-Static_Lists-Get_All_Currencies">Get All Currencies</a>.
     */
    public function show($id)
    {
        if(Input::get('currency_id')){

            $list = Listing::find($id);

            if ($list) {

                if(isset(\Illuminate\Support\Facades\Request::header()['authorization'])){
                    try{
                        if(! $user = JWTAuth::parseToken()->authenticate()){
                            return response()->json(['message'=> __('listing/api.invalid_credentials')], 401);
                        }
                    }catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
                        return response()->json(['message'=> __('listing/api.token_expired')], 401);
                    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                        return response()->json(['message'=> __('listing/api.token_invalid')], 401);
                    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                        return response()->json(['message'=> __('listing/api.jwt_exception')], 401);
                    }
                }

                $response = [
                    'message'     => __('listing/api.show_list_success'),
                    'result'      => $list,
                ];
                return response()->json($response, 200);
            }
            else {
                $response = [
                    'message'     => __('listing/api.list_not_found'),
                ];
                return response()->json($response, 404);
            }

        }else{

            $response = [
                'message'     => __('listing/api.currency_not_found'),
            ];

            return response()->json($response, 400);

        }
    }

    /**
     * @api {patch} /lists/:list_id [step 1] Update List
     * @apiName [step 1] Update List
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of lists.
     * @apiParam {String{..66}} [title] Title of the list .
     * @apiParam {Number} [managed_by] Id of manager by you Can get from <a href="#api-Static_Lists-Get_All_Listing_Manager_Types">Get All Listing Manager Types</a> .
     * @apiParam {Number} [room_type_id] id of the room types you can get it from <a href="#api-Static_Lists-Get_All_Room_Types">Get All Room Types</a> .
     * @apiParam {Number} [list_type_id] id of the list types you can get it from <a href="#api-Static_Lists-Get_All_Listing_Type">Get All Listing Type</a>.
     * @apiParam {Number} [bedrooms] no of the bedrooms.
     * @apiParam {Number} [phone] phone of the member's list .
     * @apiParam {Number} [bathrooms] no of the bathrooms .
     * @apiParam {Number} [guest] no of the guests .
     * @apiParam {Number} [single_bed] no of the single_bed .
     * @apiParam {Number} [double_bed] no of the double_bed .
     * @apiParam {Number} [sofa_bed] no of the sofa_bed .
     * @apiParam {Number} [group_id] id of the group list .
     * @apiParam {String} [description] description of the list .
     */
    public function update(Request $request, $id)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'title'         => 'max:66',
            'managed_by'    => 'in:0,1',
            'room_type_id'  => 'exists:room_type,id',
            'list_type_id'  => 'exists:list_types,id',
            'bedrooms'      => 'numeric',
            'phone'         => 'alpha_num',
            'bathrooms'     => 'numeric',
            'guest'         => 'numeric',
            'single_bed' => 'numeric',
            'double_bed' => 'numeric',
            'sofa_bed'   => 'numeric',
        ]);

        $listing = Listing::find($id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ],400);
        }

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.update_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $member_list = Listing::where('id',$id)->where('member_id',$user_id);

        if ($member_list === NULL){
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ],401);
        }

        if($request['title']){
            $listing->title = $request['title'];
        }

        if($request['managed_by']){
            $listing->managed_by = $request['managed_by'];
        }

        if($request['room_type_id']){
            $listing->room_type_id = $request['room_type_id'];
        }

        if($request['list_type_id']){
            $listing->list_type_id = $request['list_type_id'];
        }

        if($request['bedrooms']){
            $listing->bedrooms = $request['bedrooms'];
        }

        if($request['phone']){
            $listing->phone = $request['phone'];
        }

        if($request['bathrooms']){
            $listing->bathrooms = $request['bathrooms'];
        }

        if($request['guest']){
            $listing->guest = $request['guest'];
        }

        if($listing->save()){
            if($request['single_bed']){
                $listBedSingle = ListBedType::where('list_id', $id)->where('bed_type_id',1)->first();
                $listBedSingle->amount = $request['single_bed'];
                $listBedSingle->save();
            }
            if($request['double_bed']){
                $listBedDouble = ListBedType::where('list_id', $id)->where('bed_type_id',2)->first();
                $listBedDouble->amount = $request['double_bed'];
                $listBedDouble->save();
            }
            if($request['sofa_bed']){
                $listBedSofa = ListBedType::where('list_id', $id)->where('bed_type_id',3)->first();
                $listBedSofa->amount = $request['sofa_bed'];
                $listBedSofa->save();
            }
            $response = [
                'message' => __('member/api.list_updated_success'),
                'result' => $listing,
            ];
            return response()->json($response, 200);
        }else{
            $response = [
                'message' => __('member/api.list_updated_failure'),
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
