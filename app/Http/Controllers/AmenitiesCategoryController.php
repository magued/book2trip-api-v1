<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryAmenity;
use Illuminate\Support\Facades\Input;

class AmenitiesCategoryController extends Controller
{
    /**
     * @api {get} /amenities-categories?:locale Get All Amenities Categories
     * @apiName Get All Amenities Categories
     * @apiGroup Static Lists
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function getAmenitiesCategories()
    {
        $result = CategoryAmenity::get();
        $response = [
          'message'         => __('api.amenitiesAllCategories_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);
    }

}
