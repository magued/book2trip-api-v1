<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListingType;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

/**
 * @resource GET Listings types
 *
 * Here Members can be retrieved all or once. In addition to it Login and Signup actions implemente properly.
 */
class ListingTypeController extends Controller
{
  /**
   * @api {get} /listing-types?:locale:pagination Get All Listing Type
   * @apiName Get All Listing Type
   * @apiGroup Static Lists
   *
   * @apiParam {Number} [pagination=5] Number Of listing type that you want to return .
   * @apiParam {String=en,ar} locale Symbol of locales.
   */
   public function getListingTypes() {

       (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 5;

       $result = ListingType::paginate($pagination);
       $result->setPath(route('listing-types.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        return [
          'message'         => __('api.listingTypes_success'),
          'result'          => $result
        ];
        return response()->json($response, 200);

    }
}
