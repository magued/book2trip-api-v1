<?php

namespace App\Http\Controllers;

use App\Season;
use Illuminate\Http\Request;
use Validator;

class ListingSeasonController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
        $this->middleware('check.list.access');
    }

    /**
     * @api {get} /lists/{list_id}/seasons [step 6] get All List Seasons
     * @apiName [step 6] get All List Seasons
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     */
    public function index($list_id)
    {
        $seasons = Season::where('list_id',$list_id)->get();
        return response()->json([
            'message' => __('listing/api.get_seasons_list_success'),
            'result'  => $seasons
        ],200);
    }

    /**
     * @api {post} /lists/{list_id}/seasons [step 6] add List Season
     * @apiName [step 6] add List Season
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {String} title Title of Season .
     * @apiParam {Date='yyyy-mm-dd'} start_date Start Date of Season .
     * @apiParam {Date='yyyy-mm-dd'} end_date End Date of Season.
     * @apiParam {Number} price Price of the night.
     * @apiParam {Number} minimum_stay Minmum nights of season.
     * @apiParam {Number} weekend_price Price of the weekend.
     * @apiParam {Number} weekly_discount Discount of week.
     * @apiParam {Number} monthly_discount Discount of month.
     */
    public function store($list_id,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'            => 'required|max:66',
            'start_date'       => 'required|date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'required|date|date_format:Y-m-d|after:start_date',
            'price'            => 'required|integer|min:1',
            'minimum_stay'     => 'required|integer|min:1',
            'weekend_price'    => 'required|integer|max:price',
            'weekly_discount'  => 'required|integer|between:0,100',
            'monthly_discount' => 'required|integer|between:0,100',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_seasons_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $check_dates = $this->check_dates($request->input('start_date'),$request->input('end_date'),$list_id);

        if(count($check_dates) > 0){
            return response()->json([
                'message' => __('listing/api.this_days_in_another_season').' '.implode(',',$check_dates),
            ],400);
        }

        $datediff = strtotime($request['end_date']) - strtotime($request['start_date']);

        $datediff = floor($datediff / (60 * 60 * 24));

        if($datediff < $request['minimum_stay'] ){
            return response()->json([
                'message' => __('listing/api.diff_dates_must_be_equal_or_more_minmum_stay'),
            ],400);
        }

        $season = new Season([
            'title' => $request->input('title'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'price' => $request->input('price'),
            'minimum_stay' => $request->input('minimum_stay'),
            'weekend_price' => $request->input('weekend_price'),
            'weekly_discount' => $request->input('weekly_discount'),
            'monthly_discount' => $request->input('monthly_discount'),
            'list_id' => $list_id,
        ]);

        if($season->save()){
            return response()->json([
                'message' => __('listing/api.add_season_list_success'),
                'result' => $season
            ],201);
        }else{
            return response()->json([
                'message' => __('listing/api.add_season_list_error'),
            ],500);
        }
    }

    /**
     * @api {get} /lists/{list_id}/seasons/{season_id} [step 6] get List Season
     * @apiName [step 6] get List Season
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} season_id Id of the season .
     */
    public function show($list_id, $id)
    {
        $season_id = Season::where('id',$id)->where('list_id',$list_id)->get();
        if(count($season_id) === 0){
            return response()->json([
                'message' => __('listing/api.season_id_not_found'),
            ],400);
        }
        $seasons = Season::find($id);
        return response()->json([
            'message' => __('listing/api.get_season_details_success'),
            'result'  => $seasons
        ],200);
    }

    /**
     * @api {patch} /lists/{list_id}/seasons/{season_id} [step 6] update List Season
     * @apiName [step 6] update List Season
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} season_id Id of the season .
     * @apiParam {String} title Title of Season .
     * @apiParam {Date='yyyy-mm-dd'} start_date Start Date of Season .
     * @apiParam {Date='yyyy-mm-dd'} end_date End Date of Season.
     * @apiParam {Number} price Price of the night.
     * @apiParam {Number} minimum_stay Minmum nights of season.
     * @apiParam {Number} weekend_price Price of the weekend.
     * @apiParam {Number} weekly_discount Discount of week.
     * @apiParam {Number} monthly_discount Discount of month.
     */
    public function update($list_id, Request $request, $id)
    {
        $season_id = Season::where('id',$id)->where('list_id',$list_id)->get();
        if(count($season_id) === 0){
            return response()->json([
                'message' => __('listing/api.season_id_not_found'),
            ],400);
        }

        $validator = Validator::make($request->all(), [
            'title'            => 'max:66',
            'start_date'       => 'date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'date|date_format:Y-m-d',
            'price'            => 'integer|min:1',
            'minimum_stay'     => 'integer|min:1',
            'weekend_price'    => 'integer|max:price',
            'weekly_discount'  => 'integer|between:0,100',
            'monthly_discount' => 'integer|between:0,100',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.update_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $season = Season::find($id);

        if($request['title']){
            $season->title = $request['title'];
        }
        if($request['start_date']){
            if(!$request['end_date']) {
                $check_dates = $this->check_dates($request['start_date'],$season->end_date,$list_id);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_in_another_season').' '.implode(',',$check_dates),
                    ],400);
                }

                if (strtotime($request['start_date']) >= strtotime($season->end_date)) {
                    return response()->json([
                        'message' => __('listing/api.start_date_must_less_than_end_date'),
                    ], 400);
                }
            }else{
                $check_dates = $this->check_dates($request['start_date'],$request['end_date'],$list_id);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_in_another_season').' '.implode(',',$check_dates),
                    ],400);
                }
                if (strtotime($request['start_date']) >= strtotime($request['end_date'])) {
                    return response()->json([
                        'message' => __('listing/api.start_date_must_less_than_end_date'),
                    ], 400);
                }
            }
            $season->start_date = $request['start_date'];
        }
        if($request['end_date']){
            if(!$request['start_date']){
                $check_dates = $this->check_dates($request['end_date'],$season->start_date,$list_id);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_in_another_season').' '.implode(',',$check_dates),
                    ],400);
                }

                if(strtotime($request['end_date']) <= strtotime($season->start_date)){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }
            }else{
                $check_dates = $this->check_dates($request['start_date'],$request['end_date'],$list_id);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_in_another_season').' '.implode(',',$check_dates),
                    ],400);
                }
                if(strtotime($request['end_date']) <= strtotime($request['start_date'])){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }
            }
            $season->end_date = $request['end_date'];
        }
        if($request['price']){
            if(!$request['weekend_price']){
                if($request['price'] > $season->weekend_price){
                    return response()->json([
                        'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                    ],400);
                }
            }else{
                if($request['price'] > $request['weekend_price']){
                    return response()->json([
                        'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                    ],400);
                }
            }
            $season->price = $request['price'];
        }
        if($request['minimum_stay']){
            if(!$request['start_date']&&$request['end_date']){
                $datediff = strtotime($request['end_date']) - strtotime($season->start_date);

                $datediff = floor($datediff / (60 * 60 * 24));

                if($datediff < $request['minimum_stay'] ){
                    return response()->json([
                        'message' => __('listing/api.diff_dates_must_be_equal_or_more_minmum_stay'),
                    ],400);
                }
            }elseif ($request['start_date']&&!$request['end_date']){
                $datediff = strtotime($season->end_date) - strtotime($request['start_date']);

                $datediff = floor($datediff / (60 * 60 * 24));

                if($datediff < $request['minimum_stay'] ){
                    return response()->json([
                        'message' => __('listing/api.diff_dates_must_be_equal_or_more_minmum_stay'),
                    ],400);
                }
            }elseif (!$request['start_date']&&!$request['end_date']){
                $datediff = strtotime($season->end_date) - strtotime($season->start_date);

                $datediff = floor($datediff / (60 * 60 * 24));

                if($datediff < $request['minimum_stay'] ){
                    return response()->json([
                        'message' => __('listing/api.diff_dates_must_be_equal_or_more_minmum_stay'),
                    ],400);
                }
            }elseif ($request['start_date']&&$request['end_date']){
                $datediff = strtotime($request['end_date']) - strtotime($request['start_date']);

                $datediff = floor($datediff / (60 * 60 * 24));

                if($datediff < $request['minimum_stay'] ){
                    return response()->json([
                        'message' => __('listing/api.diff_dates_must_be_equal_or_more_minmum_stay'),
                    ],400);
                }
            }
            $season->minimum_stay = $request['minimum_stay'];
        }
        if($request['weekend_price']){
            if(!$request['price']){
                if($request['weekend_price'] < $season->price){
                    return response()->json([
                        'message' => __('listing/api.weekend_price_must_greater_or_equal_price'),
                    ],400);
                }
            }
            $season->weekend_price = $request['weekend_price'];
        }
        if($request['weekly_discount']){
            $season->weekly_discount = $request['weekly_discount'];
        }
        if($request['monthly_discount']){
            $season->monthly_discount = $request['monthly_discount'];
        }

        if($season->update()){
            return response()->json([
                'message' => __('listing/api.update_season_list_success'),
                'result' => $season,
            ],200);
        }else{
            return response()->json([
                'message' => __('listing/api.update_season_list_error'),
            ],500);
        }

    }

    /**
     * @api {delete} /lists/{list_id}/seasons/{season_id} [step 6] delete List Season
     * @apiName [step 6] delete List Season
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} season_id Id of the season .
     */
    public function destroy($list_id, $id)
    {
        $season_id = Season::where('id',$id)->where('list_id',$list_id)->get();
        if(count($season_id) === 0){
            return response()->json([
                'message' => __('listing/api.season_id_not_found'),
            ],400);
        }
        $seasons = Season::where('id',$id)->delete();
        return response()->json([
            'message' => __('listing/api.delete_season_success'),
        ],200);
    }

    public function check_dates($start_date,$end_date,$list_id){
        $dates = getAllDatesBetweenSpecificDates($start_date,$end_date);
        $seasons = Season::where('list_id',$list_id)->get();
        $results = array();
        foreach ($seasons as $key => $value){
            $dates_in_table = getAllDatesBetweenSpecificDates($value['start_date'],$value['end_date']);
            $results[] = array_intersect($dates_in_table,$dates);
        }

        $results_arr = array();
        foreach ($results as $key1 => $value1){
            foreach ($value1 as $key2 => $value2){
                $results_arr[] = $value2;
            }
        }
        return $results_arr;
    }
}
