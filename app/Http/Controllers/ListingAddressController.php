<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use Validator;
use JWTAuth;
use JWTException;

class ListingAddressController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'postListingAddress','updateListingAddress'
        ]]);
        $this->middleware('check.list.access');
    }

    /**
     * @api {post} /lists/{list_id}/address [step 2] Add List Address
     * @apiName [step 2] Add List Address
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {String} address Address of the list .
     * @apiParam {String} country Country of the list .
     * @apiParam {String} country_short short code of list country.
     * @apiParam {String} governorate Governorate of the list .
     * @apiParam {String} city City of the list .
     * @apiParam {String} building_number Building Number of the list .
     * @apiParam {String} [zip_code] Zip Code of the list .
     * @apiParam {String} unit_number Property Number of the list .
     * @apiParam {Number} longitude longitude of the list .
     * @apiParam {Number} latitude latitude of the list .
     * @apiParam {Number} [landmark] landmark of the list .
     */
    public function postListingAddress(Request $request, $list_id)
    {

        $listing = Listing::find($list_id);

        $validator = Validator::make($request->all(), [
            'address'         => 'required',
            'country'         => 'required',
            'country_short'   => 'required',
            'governorate'     => 'required',
            'city'            => 'required',
            'building_number' => 'required',
            'zip_code'        => 'numeric',
            'unit_number'     => 'required',
            'longitude'       => 'required|numeric',
            'latitude'        => 'required|numeric',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        if($request->input('zip_code')){
            $listing->zip_code        =    $request->input('zip_code');
        }

        if($request->input('landmark')){
            $listing->more_place_desc        =    $request->input('landmark');
        }
        
        $listing->address         =    $request->input('address');
        $listing->country         =    $request->input('country');
        $listing->country_short         =    $request->input('country_short');
        $listing->governorate     =    $request->input('governorate');
        $listing->city            =    $request->input('city');
        $listing->building_number =    $request->input('building_number');
        $listing->unit_number     =    $request->input('unit_number');
        $listing->longitude       =    $request->input('longitude');
        $listing->latitude        =    $request->input('latitude');
        $listing->step            =    $listing->step.',2';

        if($listing->save()){
            $response = [
                'message' => __('listing/api.list_address_added_success'),
                'result' => $listing
            ];
            return response()->json($response, 200);
        }

        else {
            $response = [
                'message' => __('listing/api.list_address_added_failure')
            ];
            return response()->json($response, 500);
        }

    }

    /**
     * @api {patch} /lists/{list_id}/address [step 2] Edit List Address
     * @apiName [step 2] Edit List Address
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {String} [address] Address of the list .
     * @apiParam {String} [country] Country of the list .
     * @apiParam {String} [country_short] Country Short of the list .
     * @apiParam {String} [governorate] Governorate of the list .
     * @apiParam {String} [city] City of the list .
     * @apiParam {String} [building_number] Building Number of the list .
     * @apiParam {String} [zip_code] Zip Code of the list .
     * @apiParam {String} [unit_number] Property Number of the list .
     * @apiParam {Number} [longitude] longitude of the list .
     * @apiParam {Number} [latitude] latitude of the list .
     * @apiParam {Number} [landmark] landmark of the list .
     */
    public function updateListingAddress(Request $request, $list_id)
    {
        $listing = Listing::find($list_id);

        $validator = Validator::make($request->all(), [
            'zip_code'        => 'numeric',
            'longitude'       => 'numeric',
            'latitude'        => 'numeric',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.adding_list_parameters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        if($request->input('address')){
            $listing->address        =    $request->input('address');
        }

        if($request->input('country')){
            $listing->country        =    $request->input('country');
        }

        if($request->input('country_short')){
            $listing->country_short  =    $request->input('country_short');
        }

        if($request->input('governorate')){
            $listing->governorate        =    $request->input('governorate');
        }

        if($request->input('city')){
            $listing->city        =    $request->input('city');
        }

        if($request->input('building_number')){
            $listing->building_number        =    $request->input('building_number');
        }

        if($request->input('unit_number')){
            $listing->unit_number        =    $request->input('unit_number');
        }

        if($request->input('longitude')){
            $listing->longitude        =    $request->input('longitude');
        }

        if($request->input('latitude')){
            $listing->latitude        =    $request->input('latitude');
        }

        if($request->input('zip_code')){
            $listing->zip_code        =    $request->input('zip_code');
        }

        if($request->input('landmark')){
            $listing->more_place_desc        =    $request->input('landmark');
        }

        if($listing->save()){

            $response = [
                'message' => __('listing/api.list_address_updated_success'),
                'result' => $listing
            ];
            return response()->json($response, 200);
        }

        else {
            $response = [
                'message' => __('listing/api.list_address_updated_failure')
            ];
            return response()->json($response, 500);
        }
    }

}
