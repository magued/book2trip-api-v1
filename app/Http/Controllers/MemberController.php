<?php

namespace App\Http\Controllers;

use App\Member;
use App\Listing;
use App\Connection;
use App\Wishlist;
use App\Booking;
use App\Review;
use App\GroupListing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Validator;
use Illuminate\Support\Facades\Input;
use App\AbstractClasses\Connections;

class MemberController extends Connections
{
    public function __construct(){
        $this->middleware('jwt.auth',['only' => [
            'getMemberGroupListings',
            'getMemberWishlists',
            'getMemberSingleListings',
            'getMemberTrips',
            'getMemberApprovedBookings',
            'getMemberPendingRequests',
            'update',
            'getMemberConnections',
            'getAddConnection',
            'getAcceptConnection',
            'getCancelConnection',
            'changePassword',
            'getMemberReviewsAsHost',
            'getMemberReviewsAsGuest',
            'getMemberAddToWishlists',
            'getMemberDeleteFromWishlists',
            'getMemberPendingRequests',
            'getInboxConversations'
        ]]);
    }

    /**
     * @api {post} /signup Sign Up
     * @apiName Sign Up
     * @apiGroup Members
     *
     * @apiParam {String{..30}} first_name First Name of the member .
     * @apiParam {String{..30}} last_name Last Name of the member .
     * @apiParam {String="male","female"} gender gender of the member .
     * @apiParam {String} username username of the member .
     * @apiParam {String} email email of the member .
     * @apiParam {String{..8}} password password of the member .
     * @apiParam {String="en","ar"} locale Symbol of locales.
     * @apiParam {Number} currency_id Number of the currency That Member Used You can get it from <a href="#api-Static_Lists-Get_All_Currencies">Get All Currencies</a> .
     */
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'  => 'alpha_num|required|max:30',
            'last_name'   => 'alpha_num|required|max:30',
            'gender'      => 'required|in:male,female',
            'username'    => 'alpha_num|required|unique:members,username,' . $request->id,
            'email'       => 'email|required|unique:members,email,' . $request->id,
            'password'    => 'required|min:8',
            'currency_id'    => 'required|exists:currencies,id',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.signup_paramters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $gender = $request->input('gender');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $last_language = $request->input('locale');
        $currency_id = $request->input('currency_id');
        $code = str_random(60);

        $member = new Member([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'gender' => $gender,
            'username' => $username,
            'email' => $email,
            'password' => Hash::make($password),
            'email_code' => $code,
            'status' => 0,
            'currency_id' => $currency_id,
            'last_language' => $last_language
        ]);

        if($member->save()){
            $credentials = $request->only('email','password');
            try {
                if(! $token = JWTAuth::attempt($credentials)){
                    return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
                }
            } catch (JWTException $e){
                return response()->json(['message'=> __('member/api.error_create_token')], 500);
            }

            $response = [
                'message' => __('member/api.member_created'),
                'result' => [
                    'token' => $token,
                    'member' => $member
                ]
            ];
            return response()->json($response,201);

        }else{
            $response = [
                'message' => __('member_creation_error')
            ];

            return response()->json($response,500);
        }
    }

    /**
     * @api {get} /members/:id?:locale Show Member Details
     * @apiName Show Member Details
     * @apiGroup Members
     *
     * @apiParam {Number} id Id of the member .
     * @apiParam {String=en,ar} locale Symbol of locales.
     */
    public function show($id)
    {
        $member = Member::where('id', '=', $id)->first();
        if ($member === null) {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response,404);
        }else{
            $member = Member::findOrFail($id);
            if($member){
                $response = [
                    'message' => __('member/api.member_details'),
                    'result' => ['member' => $member]
                ];
                return response()->json($response,200);
            }else{
                $response = [
                    'message' => __('member/api.member_details_error')
                ];

                return response()->json($response,500);
            }
        }
    }

    /**
     * @api {patch} /members Update Member details
     * @apiName Update Member
     * @apiGroup Members
     *
     * @apiParam {String{..30}} [first_name] First Name of the member .
     * @apiParam {String{..30}} [last_name] Last Name of the member .
     * @apiParam {String="male","female"} [gender] gender of the member .
     * @apiParam {String="english","arabic"} [speaks] speaks of the member .
     * @apiParam {Date={'yyyy-mm-dd'}} [birth_date] birthday of the member yyyy-mm-dd Format .
     * @apiParam {String} [address] address of the member .
     * @apiParam {Number} [country_code_phone] Country Code of member's phone You can get <a href="#api-Static_Lists-Get_All_Phone_Codes">Get All Phone Codes</a> .
     * @apiParam {Number} [phone] phone of the member .
     * @apiParam {String} [aboutme] Biography of the member .
     * @apiParam {String} [job] Profession of the member .
     * @apiParam {String} [website] website's url of the member .
     * @apiParam {String} [photo] base64 of the image .
     * @apiParam {Number} [country_id] id of the country You can get from <a href="#api-Static_Lists-Get_All_Countries">Get All Countries</a> .
     */
    public function update(Request $request)
     {
         if(! $user = JWTAuth::parseToken()->authenticate()){
             return response()->json([
                 'status' => 'error',
                 'msg'=> 'Token Not Correct'
             ]);
         }

         $user_id = $user->id;

         $validator = Validator::make($request->all(), [
             'first_name'      => 'alpha_num|max:30',
             'last_name'       => 'alpha_num|max:30',
             'birth_date'       => 'date_format:"Y-m-d"',
             'address'       => 'alpha_num',
             'country_code_phone'       => 'numeric',
             'phone'       => 'numeric',
             'aboutme'       => 'alpha',
             'job'       => 'alpha',
             'website'       => 'url',
             'gender'          => 'in:male,female',
             'speaks'   => 'in:arabic,english',
             'country_id'     => 'exists:country,id',
         ]);
         if ($validator->fails())
         {
             return response()->json([
                 'message' => __('member/api.update_member_paramters_not_correct'),
                 'error_fields' => $validator->errors()
             ],400);
         }

         $member = Member::findOrFail($user_id);

        //photo upload
        if($request['photo']) {
            $image = $request['photo'];
            $directory = 'user';
            $sizes = [270, 51];
            $member->photo = uploadAndSave($image, $directory, $sizes);

            // Remove old Pictures
//            if(file_exists(public_path() . "/uploads"."/".$directory."/" . $member->photo)){
//                unlink(public_path() . "/uploads"."/".$directory."/" . $member->photo);
//            }
//            if(file_exists($destinationPath_medium . $member->photo)){
//                unlink($destinationPath_medium . $member->photo);
//            }
//            if(file_exists($destinationPath_thumb . $member->photo)){
//                unlink($destinationPath_thumb . $member->photo);
//            }

        }

        if($request['first_name']){
            $member->first_name = $request['first_name'];
        }

         if($request['last_name']){
             $member->last_name = $request['last_name'];
         }

         if($request['birth_date']){
             $member->birth_date = $request['birth_date'];
         }

         if($request['address']){
             $member->address = $request['address'];
         }

         if($request['country_code_phone']){
             $member->country_code_phone = $request['country_code_phone'];
         }

         if($request['phone']){
             $member->phone = $request['phone'];
         }

         if($request['aboutme']){
             $member->aboutme = $request['aboutme'];
         }

         if($request['job']){
             $member->job = $request['job'];
         }

         if($request['website']){
             $member->website = $request['website'];
         }

         if($request['speaks']){
             $member->speaks = $request['speaks'];
         }

         if($request['country_id']){
             $member->country_id = $request['country_id'];
         }

         if($member->update()){

             $response = [
                 'message' => __('member/api.member_updated'),
                 'result' => $member
             ];
             return response()->json($response, 200);

         }

         $response = [
             'message' => __('member/api.member_updated_error')
         ];

         return response()->json($response, 500);


     }

    /**
     * @api {post} /signin Member Sign In
     * @apiName Member Sign In
     * @apiGroup Members
     *
     * @apiParam {String="en","ar"} locale Symbol of locales.
     * @apiParam {String} email email of the member .
     * @apiParam {String{..8}} password password of the member .
     */
    public function signin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'       => 'required|email|exists:members,email',
            'password'    => 'required|min:6',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => $validator->errors()
            ],400);
        }

        $credentials = $request->only('email','password');

        try {
            if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
            }
        } catch (JWTException $e){
            return response()->json(['message'=> __('member/api.error_create_token')], 500);
        }

        return response()->json([
            'message' => __('member/api.login_success'),
            'result' => ['token'=> $token]
        ],200);
    }

    /**
     * @api {patch} /change-password Change Password
     * @apiName Change Password
     * @apiGroup Members
     *
     *
     * @apiParam {String="en","ar"} locale Symbol of locales.
     * @apiParam {String{8..}} old_password Old Password of the member .
     * @apiParam {String{8..}} new_password New Password of the member .
     */
    public function changePassword(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);

        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'old_password'    => 'required|min:8',
            'new_password'    => 'required|min:8',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.change_password_paramters_not_correct'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $member = Member::find($user_id);

        if ($member != null) {
            if(Hash::check($request->old_password,$member->password)){
                $member->password = Hash::make($request->new_password);
                if($member->update()){
                    $response = [
                        'message' => __('member/api.change_password_success'),
                    ];
                    return response()->json($response,200);
                }else{
                    $response = [
                        'message' => __('member/api.change_password_failure'),
                    ];
                    return response()->json($response,500);
                }
            }else{
                $response = [
                    'message' => __('member/api.old_password_not_correct'),
                ];
                return response()->json($response,400);
            }
        } else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response,404);
        }
    }

    /**
     * Register or Login a Member with Facebook, Google or Twitter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function social(Request $request, Facebook $fb, GoogleServiceProvider $google)
    {
        $validator = Validator::make($request->all(), [
            'facebook_access_token'  => 'alpha_num',
            // 'google_access_token'    => 'alpha_num',
            'twitter_screen_name'   => 'alpha_num',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'msg' => $validator->errors()
            ]);
        }

        if ($request->input('facebook_access_token') != null ) {

            $member = Member::where('facebook_verified', $request->input('facebook_access_token'))->first();

            if(count($member)) {

                $token = JWTAuth::fromUser($member);

                // all good so return the token
                $response = [
                    'status' => 'Success',
                    'msg' => 'Member Logged in using Facebook Successfully',
                    'token' => $token
                ];
                return response()->json($response, 200);
            }
            else {
                $response = $fb->get('/me?fields=id,email,first_name,last_name,picture.width(800).height(800),gender', $request->input('token'));

                $member = new Member;

                $member->facebook_verified = $response->getGraphUser()->getId();
                $member->first_name = $response->getGraphUser()->getFirstName();
                $member->last_name = $response->getGraphUser()->getLastName();
                $member->gender = $response->getGraphUser()->getGender();
                $member->photo = $response->getGraphUser()->getFirstName() . time() . '.jpg';
                copy($response->getGraphUser()->getPicture()['url'] , public_path() . '/uploads/user/' . $member->photo ) ;
                $member->email = ($response->getGraphUser()->getEmail() != null) ? $response->getGraphUser()->getEmail() : $response->getGraphUser()->getId().'@facebook.com' ;
                $member->facebook = "https://fb.com/".$response->getGraphUser()->getId();
                $member->password = null;
                $member->email_verified = 1;
                $member->email_code = str_random(60);

                if ($member->save()) {
                    //original photo fetching to Intervention package Object
                    $img = Image::make(public_path() . '/uploads/user/' . $member->photo);

                    //resize & mirror Original Image to Medium
                    $img->resize(null, 270, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save(public_path() . '/uploads/user/medium/' . $member->photo);

                    //resize & mirror Original Image to Thumbenial
                    $img->resize(null, 51, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save(public_path() . '/uploads/user/thumb/' . $member->photo);

                    $token = JWTAuth::fromUser($member);

                    $response = [
                        'status' => 'success',
                        'msg' => 'Member Created',
                        'result' => [
                            'token' => $token,
                            'member' => $member
                        ]
                    ];
                    return response()->json($response, 201);
                }

            }
        }

        elseif ($request->input('google_access_token') != null ) {

            $member = Member::where('google_verified', $request->input('google_access_token'))->first();

            if(count($member)) {
            }
            else {


              // //Create Client Request to access Google API
              // $client = new Google_Client();
              // $client->setApplicationName('Book2Trip');
              // $client->setClientId('499608557036-3d9nc19iv4207lucikdvnnbe05igbire.apps.googleusercontent.com');
              // $client->setClientSecret('arN9XstUB6GCRDO28dps1cdL');
              // $client->setRedirectUri('http://localhost/api/public');
              // $client->setDeveloperKey('AIzaSyDiFJeNzOkKj-v6gGXdkTnNVwwlsKmNvSA');
              // $client->addScope("https://www.googleapis.com/auth/userinfo.profile");
              //
              // //Send Client Request
              // $objOAuthService = new Google_Service_Oauth2($client);



              // $client = new Google_Client();
              // $plus = new Google_Service_Plus($client);

              $result = $google->getPlus();
              print_r($result);

              // print "ID: {$me['id']}\n";
              // print "Display Name: {$me['displayName']}\n";
              // print "Image Url: {$me['image']['url']}\n";
              // print "Url: {$me['url']}\n";

            }
        }

        elseif ($request->input('twitter_screen_name') != null ) {

            $member = Member::where('username', $request->input('twitter_screen_name'))->first();

            if(count($member)) {

            $token = JWTAuth::fromUser($member);

            // all good so return the token
            $response = [
                'status' => 'Success',
                'msg' => 'Member logged in using Twitter successfully',
                'token' => $token
            ];
            return response()->json($response, 200);


            }
            else {

              /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
              $settings = array(
                'oauth_access_token' => "269730858-5dlsiJrHoHpbdiujGlQ6m1kJjmxwtGczrExmQfcU",
                'oauth_access_token_secret' => "207755lazQlCxmtTjZa8b2udWTlG0wbOiq7Llj1o1ilop",
                'consumer_key' => "0zgTi2RJnakvFhl6mXZCqSiGn",
                'consumer_secret' => "qaUMbx8icT3Sp0s0jBYrHJOAv3pJLWV2ug0YBmToQlg0ZuMr45"
              );
              /** Perform a GET request and echo the response **/
              /** Note: Set the GET field BEFORE calling buildOauth(); **/
              $url = 'https://api.twitter.com/1.1/users/show.json';
              $getfield = '?screen_name='.$request->input('twitter_screen_name');
              $requestMethod = 'GET';
              $twitter = new TwitterServiceProvider($settings);
              $twitterString =  $twitter->setGetfield($getfield)
                         ->buildOauth($url, $requestMethod)
                         ->performRequest();
              $twitterObject = json_decode($twitterString);

              $member = new Member;

              $name = explode(' ', $twitterObject->name);
              $photo = str_replace('_normal', '', $twitterObject->profile_image_url);

              $member->twitter_verified = $twitterObject->id;
              $member->first_name = $name[0];
              $member->last_name = end($name);
              $member->username = $twitterObject->screen_name;
              $member->photo = $name[0] . time() . '.jpg';
              copy($photo , public_path() . '/uploads/user/' . $member->photo ) ;
              $member->email = $twitterObject->id.'@twitter.com' ;
              $member->twitter = "https://twitter.com/@".$twitterObject->screen_name;
              $member->password = null;
              $member->email_verified = 1;
              $member->email_code = str_random(60);

              if ($member->save()) {
                  //original photo fetching to Intervention package Object
                  $img = Image::make(public_path() . '/uploads/user/' . $member->photo);

                  //resize & mirror Original Image to Medium
                  $img->resize(null, 270, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $img->save(public_path() . '/uploads/user/medium/' . $member->photo);

                  //resize & mirror Original Image to Thumbenial
                  $img->resize(null, 51, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $img->save(public_path() . '/uploads/user/thumb/' . $member->photo);

                  $token = JWTAuth::fromUser($member);

                  $response = [
                      'status' => 'success',
                      'msg' => 'Member Created',
                      'result' => [
                          'token' => $token,
                          'member' => $member
                      ]
                  ];
                  return response()->json($response, 201);
              }




            }
        }
        else {
            // $response = [
            //     'status' => 'error',
            //     'msg' => 'No Access Token Found'
            // ];
            // return response()->json($response);


        }
    }


    /**
     * @api {get} members/single-listings?:locale:pagination Member's Single Listings
     * @apiName Get Member's Single Listings
     * @apiGroup Members Units Manager
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Single Listings that you want to return .
     */
    public function getMemberSingleListings()
    {
      if(! $user = JWTAuth::parseToken()->authenticate()){
          return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
      }

      $user_id = $user->id;

      (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

      $memberSingleListings = Listing::where('is_group', '0')->where('member_id', $user_id)->orderBy('id','DESC')->paginate($pagination);

      $memberSingleListings->setPath(route('member-single-lists.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

      if ($memberSingleListings != null) {
          $response = [
              'message' => __('member/api.get_single_listing'),
              'result' => $memberSingleListings,
          ];
          return response()->json($response, 200);
      } else {
          $response = [
              'message' => __('member/api.member_not_found')
          ];
          return response()->json($response, 404);
      }

    }

    /**
     * @api {get} members/wishlists?:locale:pagination Get Member's Wishlists
     * @apiName Get Member's Wishlists
     * @apiGroup Members Units Manager
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Wishlists that you want to return .
     */
    public function getMemberWishlists()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberWishlists = Wishlist::where('member_id', $user_id)->paginate($pagination);

        $memberWishlists->setPath(route('member-wishlists.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberWishlists  != null) {
            $response = [
                'message' => __('member/api.get_wishlist_listing'),
                'result' => $memberWishlists,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }

    /**
     * @api {get} members/group-listings?:locale:pagination Get Member's Group Listings
     * @apiName Get Member's Group Listings
     * @apiGroup Members Units Manager
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Group Listings that you want to return .
     */
    public function getMemberGroupListings()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberGroupListings = GroupListing::where('member_id', $user_id)->orderBy('id','DESC')->paginate($pagination);

        $memberGroupListings->setPath(route('member-group-lists.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberGroupListings != null) {
            $response = [
                'message' => __('member/api.get_group_listing'),
                'result' => $memberGroupListings,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }

    /**
     * @api {get} members/trips?:locale:pagination Get Member's Trips
     * @apiName Get Member's Trips
     * @apiGroup Members Units Manager
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Trips that you want to return .
     */
    public function getMemberTrips()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberTrips = Booking::where('member_id', $user_id)->orderBy('id','DESC')->paginate($pagination);

        $memberTrips->setPath(route('member-trips.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberTrips  != null) {
            $response = [
                'message' => __('member/api.get_my_trip_member'),
                'result' => $memberTrips,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }

    /**
     * @api {get} members/approved-bookings?:locale:pagination Get Member's Approved Bookings
     * @apiName Get Member's Approved Bookings
     * @apiGroup Members Units Manager
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Approved Bookings that you want to return .
     */
    public function getMemberApprovedBookings()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberListsIds = Listing::where('member_id', $user_id)->pluck('id')->toArray();
        $memberApprovedBookings = Booking::whereIn('list_id', $memberListsIds)->where('status', 2)->orderBy('id','DESC')->paginate($pagination);

        $memberApprovedBookings->setPath(route('member-approved-bookings.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberApprovedBookings != null) {
            $response = [
                'message' => __('member/api.get_my_approval_booking_member'),
                'result' => $memberApprovedBookings,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }



    /**
     * @api {get} members/pending-requests?:locale:pagination Get Member's Pending Requests
     * @apiName Get Member's Connections
     * @apiGroup Members Connections
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Pending Requests that you want to return .
     */
    public function getMemberPendingRequests()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberPendingRequests = Connection::where('friend_id', $user_id)->where('status', 0)->paginate($pagination);

        $memberPendingRequests->setPath(route('member-pending-requests.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberPendingRequests  != null) {
            $response = [
                'message' => __('member/api.get_my_pending_requests_member'),
                'result' => $memberPendingRequests,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }

    /**
     * @api {get} members/connections?:locale:pagination Get Member's Connections
     * @apiName Get Member's Connections
     * @apiGroup Members Connections
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Member's Connections that you want to return .
     */
    public function getMemberConnections()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberConnectionsIds = Connection::where('friend_id', $user_id)->where('status', 1)
                                        ->pluck('member_id')->toArray();

        $memberConnections = Member::whereIn('id', $memberConnectionsIds)->paginate($pagination);

        $memberConnections->setPath(route('member-connections.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        if ($memberConnections  != null) {
            $response = [
                'message' => __('member/api.get_connections_member'),
                'result' => $memberConnections,
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.member_not_found')
            ];
            return response()->json($response, 404);
        }
    }

    /**
     * @api {post} members/add-requests Post Member's Add Connections Requests
     * @apiName Post Member's Add Connections Requests
     * @apiGroup Members Connections
     *
     * @apiParam {number} friend_id Id of the Member to be Connected as a Friend.
     */
    public function getAddConnection(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'friend_id'    => 'required|exists:connections,friend_id',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.add_request_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $newConnection = $this->addConnection($request->input('friend_id'), $user_id);

        if ($newConnection === 0) {
            $response = [
                'message' => __('member/api.member_already_connected'),
            ];
            return response()->json($response, 400);
        } elseif($newConnection === 1){
            $response = [
                'message' => __('member/api.member_same_user'),
            ];
            return response()->json($response, 400);
        }else {
            $response = [
                'message' => __('member/api.connection_request_sent_success'),
            ];
            return response()->json($response, 200);
        }

    }

    /**
     * @api {patch} /accept-requests Accept Member's Add Connections Requests
     * @apiName Accept Member's Add Connections Requests
     * @apiGroup Members Connections
     *
     * @apiParam {number} member_id Id of the Member to be Connected as a Friend.
     */
    public function getAcceptConnection(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }

        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'member_id'    => 'required|exists:connections,member_id',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.accept_request_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $checkConnection = $this->acceptConnection($user_id ,$request->input('member_id'));

        if ($checkConnection === 0) {
            $response = [
                'message' => __('member/api.connection_not_found'),
            ];
            return response()->json($response, 400);
        } else {
            $response = [
                'message' => __('member/api.connection_accepted_success'),
            ];
            return response()->json($response, 200);
        }
    }

    /**
     * @api {delete} members/cancel-requests  Member's Cancel Connections Requests
     * @apiName Cancel Member's Add Connections Requests
     * @apiGroup Members Connections
     *
     * @apiParam {number} member_id Id of the Member to be Connected as a Friend.
     */
    public function getCancelConnection(Request $request )
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'member_id'    => 'required|exists:connections,member_id',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.cancel_request_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $checkConnection = $this->cancelConnection($user_id ,$request->input('member_id'));

        if ($checkConnection === 1) {
            $response = [
                'message' =>  __('member/api.connection_cancelled_success'),
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'message' => __('member/api.connection_not_found'),
            ];
            return response()->json($response, 400);
        }
    }

    /**
     * @api {delete} members/remove-requests Remove a Member's Connection
     * @apiName Remove a Member's Connections
     * @apiGroup Members Connections
     *
     * @apiParam {number} member_id Id of the Member to be remove as a Friend.
     */
    public function getRemoveConnection(Request $request )
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'member_id'    => 'required|exists:connections,member_id',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.remove_request_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $checkConnection = $this->removeConnection($user_id ,$request->input('member_id'));

        if ($checkConnection === 1) {
            $response = [
                'message' => __('member/api.connection_removed_success'),
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.connection_not_found'),
            ];
            return response()->json($response, 400);
        }
    }



    /**
     * @api {get} members/reviews-as-host?:locale:pagination Get Member's Reviews as a Host
     * @apiName Get Member's Reviews as a Host
     * @apiGroup Members Reviews
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Reviews that you want to return .
     */
    public function getMemberReviewsAsHost()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberReviewsAsHost =  Review::where('review_type', 2)
                      ->where('member_id', $user_id)->where('approved', 1)->paginate($pagination);

        $memberReviewsAsHost->setPath(route('member-reviews-as-host.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        $response = [
            'message' => __('member/api.get_members_reviews_as_host_success'),
            'result' => $memberReviewsAsHost,
        ];
        return response()->json($response, 200);
    }

    /**
     * @api {get} members/reviews-as-guest?:locale:pagination Get Member's Reviews as a Guest
     * @apiName Get Member's Reviews as a Guest
     * @apiGroup Members Reviews
     *
     * @apiParam {String=en,ar} locale Symbol of locales.
     * @apiParam {Number} [pagination=10] Number Of Reviews that you want to return .
     */
    public function getMemberReviewsAsGuest()
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

        $memberReviewsAsGuest =  Review::where('review_type', 1)
                      ->where('member_id', $user_id)->where('approved', 1)->paginate($pagination);

        $memberReviewsAsGuest->setPath(route('member-reviews-as-guest.index'))->appends(['locale'=> Input::get('locale'),'pagination'=>$pagination]);

        $response = [
            'message' => __('member/api.get_members_reviews_as_guest_success'),
            'result' => $memberReviewsAsGuest,
        ];
        return response()->json($response, 200);
    }



    /**
     * @api {post} members/add-wishlist Add wishlist to Member.
     * @apiName  Add wishlist to Member.
     * @apiGroup Members
     *
     * @apiParam {number} list_id Id of the list to be wishlist.
     */
    public function getMemberAddToWishlists(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            if(! $user = JWTAuth::parseToken()->authenticate()){
                return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
            }
        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'list_id'    => 'required|exists:lists,id',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.add_wishlist_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $memberAddToWishlists = Wishlist::where('member_id', $user_id)
            ->where('list_id', $request->input('list_id'))
            ->first();

        if(count($memberAddToWishlists) > 0) {
            $response = [
                'message' => __('member/api.list_already_wishlisted'),
            ];
            return response()->json($response, 400);
        }

        $memberAddToWishlists = new Wishlist;
        $memberAddToWishlists->member_id = $user_id;
        $memberAddToWishlists->wishlist_name = '';
        $memberAddToWishlists->wishlist_public = 1;
        $memberAddToWishlists->list_id = $request->input('list_id');
        $memberAddToWishlists->save();

        if ($memberAddToWishlists->save()) {
            $response = [
                'message' => __('member/api.list_wishlisted_success'),
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.add_to_wishlist_error'),
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * @api {delete} members/delete-wishlist Remove wishlist from Member.
     * @apiName Remove wishlist from Member.
     * @apiGroup Members
     *
     * @apiParam {number} list_id Id of the list to be wishlist.
     */
    public function getMemberDeleteFromWishlists(Request $request)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message'=> __('member/api.invalid_credentials')], 401);
        }
        $user_id = $user->id;

        $validator = Validator::make($request->all(), [
            'list_id'    => 'required|exists:wishlist,list_id',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('member/api.delete_wishlist_error_fields'),
                'error_fields' => $validator->errors()
            ],400);
        }

        $memberAddToWishlists = Wishlist::where('member_id', $user_id)
            ->where('list_id', $request->input('list_id'))
            ->first();

        if ($memberAddToWishlists != null) {
            $memberAddToWishlists->delete();
            $response = [
                'message' => __('member/api.remove_wishlist_success'),
            ];
            return response()->json($response, 200);
        }
        else {
            $response = [
                'message' => __('member/api.remove_wishlist_error'),
            ];
            return response()->json($response, 400);
        }
    }


    /**
     * Used to return all conversation has been sent to me as host
     * @param $memberId
     * @param int $paginate
     * @return array
     */
    public function getInboxConversations(Request $request)
    {
        // return $request->input('type');
        if($request->input('type') == 'host') {
            if(! $user = JWTAuth::parseToken()->authenticate()){

                return response()->json([
                    'message'=> __('api.token_error')
                ]);
            }

            (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

            $result = Member::find($user->id)->conversationHosts($user->id ,$paginate = 10);

            $result->setPath(route('member-inbox.index'))->appends('pagination',$pagination);

            if ($result != null) {
                $response = [
                    'message' => __('api.inboxAsHost_success'),
                    'result'  => $result
                ];
                return response()->json($response, 200);
            }
            else {
                $response = [
                    'message' => __('api.inboxAsHost_error'),
                ];
                return response()->json($response, 404);
            }
        }
        elseif($request->input('type') == 'guest') {
            if(! $user = JWTAuth::parseToken()->authenticate()){

                return response()->json([
                    'message'=> __('api.token_error')
                ]);
            }

            (Input::get('pagination') > 0) ? $pagination = Input::get('pagination') : $pagination = 10;

            $result = Member::find($user->id)->conversationGuests($user->id ,$paginate = 10);

            $result->setPath(route('member-inbox.index'))->appends('pagination',$pagination);

            if ($result != null) {
                $response = [
                    'message' => __('api.inboxAsGuest_success'),
                    'result'  => $result
                ];
                return response()->json($response, 200);
            }
            else {
                $response = [
                    'message' => __('api.inboxAsGuest_error'),
                ];
                return response()->json($response, 404);
            }
        }
        else {
            $response = [
                'message' => __('api.inboxType_error'),
            ];
            return response()->json($response, 400);
        }
    }

}
