<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\ExceptionDate;
use JWTAuth;
use JWTException;
use Validator;

class ListingBlockedDateController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
    }

    /**
     * @api {get} /lists/{list_id}/blocked-date [step 7] get All Blocked Dates
     * @apiName [step 7] get All Blocked Dates
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     */
    public function index($list_id)
    {
        $listing = Listing::find($list_id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ],400);
        }

        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $check_linking = Listing::where("member_id" , $user_id)->where("id",$list_id);

        if(count($check_linking) === 0){
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ],401);
        }

        $blocked_date = ExceptionDate::where('list_id',$list_id)->where('status',2)->get();
        return response()->json([
            'message' => __('listing/api.get_blocked_date_list_success'),
            'result'  => $blocked_date
        ],200);
    }

    /**
     * @api {post} /lists/{list_id}/blocked-date [step 7] add Blocked Date
     * @apiName [step 7] add Blocked Date [step 7]
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Date='yyyy-mm-dd'} [start_date] Start Date.
     * @apiParam {Date='yyyy-mm-dd'} [end_date] End Date.
     */
    public function store($list_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date'         => 'required|date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'required|date|date_format:Y-m-d|after:start_date',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.parameters_not_correct'),
                'result' => $validator->errors()
            ],400);
        }

        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $check_dates = $this->checkDates($list_id,$start_date,$end_date);

        if(count($check_dates) > 0){
            return response()->json([
                'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
            ],400);
        }

        $blocked_dates = new ExceptionDate([
            'list_id' => $list_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'status' => 2,
            'reason' => 2,
            'discount' => NULL,
            'amount' => 1
        ]);

        if($blocked_dates->save()){
            return response()->json([
                'message' => __('listing/api.add_blocked_days_list_success'),
                'result' => $blocked_dates
            ],201);
        }else{
            return response()->json([
                'message' => __('listing/api.add_blocked_days_list_error'),
            ],500);
        }
    }

    /**
     * @api {get} /lists/{list_id}/blocked-date/{blocked_date_id} [step 7] Get Blocked Date by id
     * @apiName [step 7] Get Blocked Date by id
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} linked_date_id Id of the blocked date .
     */
    public function show($list_id, $id)
    {
        $listing = Listing::find($list_id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ], 400);
        }

        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json([
                'message' => __('listing/api.invalid_credentials')
            ], 401);
        }

        $user_id = $user->id;

        $check_linking = Listing::where("member_id", $user_id)->where("id", $list_id)->get();

        if (count($check_linking) === 0) {
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ], 401);
        }

        $blocked_date = ExceptionDate::where('id', $id)->where('list_id', $list_id)->where('status', 2)->whereNull('deleted_at')->get();

        if (count($blocked_date) === 0) {
            return response()->json([
                'message' => __('listing/api.blocked_date_not_found'),
            ], 400);
        }

        $view_date = ExceptionDate::where('id',$id)->get();

        if ($view_date) {
            return response()->json([
                'message' => __('listing/api.get_blocked_date_list_success'),
                'result' => $view_date,
            ], 200);
        } else {
            return response()->json([
                'message' => __('listing/api.get_blocked_date_list_error'),
            ], 500);
        }
    }

    /**
     * @api {patch} /lists/{list_id}/blocked-date/{blocked_date_id} [step 7] update Blocked Date
     * @apiName [step 7] update Blocked Date
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} blocked_date_id Id of the Blocked Date.
     * @apiParam {Date='yyyy-mm-dd'} [start_date] Start Date.
     * @apiParam {Date='yyyy-mm-dd'} [end_date] End Date.
     */
    public function update($list_id, Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'start_date'         => 'date|date_format:Y-m-d|after:yesterday',
            'end_date'         => 'date|date_format:Y-m-d',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'message' => __('listing/api.parameters_not_correct'),
                'result' => $validator->errors()
            ],400);
        }

        $date_id = ExceptionDate::where('id',$id)->where('list_id',$list_id)->where('status',2)->get();

        if(count($date_id) === 0){
            return response()->json([
                'message' => __('listing/api.date_id_not_found'),
            ],400);
        }

        $linked_dates = ExceptionDate::find($id);

        if($request->input('start_date')){
            if($request->input('end_date')){
                if(strtotime($request->input('start_date')) >= strtotime($request->input('end_date'))){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }

                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }else{
                if(strtotime($request->input('start_date')) >= strtotime($linked_dates->end_date)){
                    return response()->json([
                        'message' => __('listing/api.end_date_must_greater_than_start_date'),
                    ],400);
                }
                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$linked_dates->end_date);

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }
            $linked_dates->start_date = $request->input('start_date');
        }

        if($request->input('end_date')){
            if($request->input('start_date')){
                $check_dates = $this->checkDates($list_id,$request->input('start_date'),$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }else{
                $check_dates = $this->checkDates($list_id,$linked_dates->start_date,$request->input('end_date'));

                if(count($check_dates) > 0){
                    return response()->json([
                        'message' => __('listing/api.this_days_blocked_linked').' '.implode(',',$check_dates),
                    ],400);
                }
            }
            $linked_dates->end_date = $request->input('end_date');
        }

        if($linked_dates->update()){
            return response()->json([
                'message' => __('listing/api.update_blocked_days_list_success'),
                'result' => $linked_dates
            ],201);
        }else{
            return response()->json([
                'message' => __('listing/api.update_blocked_days_list_error'),
            ],500);
        }

    }

    /**
     * @api {delete} /lists/{list_id}/blocked-date/{blocked_date_id} [step 7] Delete Blocked Date by id
     * @apiName [step 7] Delete Blocked Date by id
     * @apiGroup Add & Update Lists
     *
     * @apiParam {Number} list_id Id of the list .
     * @apiParam {Number} blocked_date_id Id of the blocked date .
     */
    public function destroy($list_id,$id)
    {
        $listing = Listing::find($list_id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ], 400);
        }

        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json([
                'message' => __('listing/api.invalid_credentials')
            ], 401);
        }

        $user_id = $user->id;

        $check_linking = Listing::where("member_id", $user_id)->where("id", $list_id)->get();

        if (count($check_linking) === 0) {
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ], 401);
        }

        $blocked_date = ExceptionDate::where('id', $id)->where('list_id', $list_id)->where('status', 2)->whereNull('deleted_at')->get();

        if (count($blocked_date) === 0) {
            return response()->json([
                'message' => __('listing/api.blocked_date_not_found'),
            ], 400);
        }

        $delete_date = ExceptionDate::where('id',$id)->delete();

        if ($delete_date) {
            return response()->json([
                'message' => __('listing/api.delete_blocked_date_list_success'),
            ], 200);
        } else {
            return response()->json([
                'message' => __('listing/api.delete_blocked_date_list_error'),
            ], 500);
        }
    }

    public function checkDates($list_id,$start_date,$end_date){
        $dates = getAllDatesBetweenSpecificDates($start_date,$end_date);
        $exception_days = ExceptionDate::where('list_id',$list_id)->get();
        $results = array();
        foreach ($exception_days as $key => $value){
            $exception_days_dates = getAllDatesBetweenSpecificDates($value['start_date'],$value['end_date']);
            $results[] = array_intersect($exception_days_dates,$dates);
        }
        $results_arr = array();
        foreach ($results as $key1 => $value1){
            foreach ($value1 as $key2 => $value2){
                $results_arr[] = $value2;
            }
        }
        return array_unique($results_arr);
    }
}
