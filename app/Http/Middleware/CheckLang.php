<?php

namespace App\Http\Middleware;

use Validator;
use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class CheckLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('get')){
            if(Input::get('locale')){
                if(!in_array(Input::get('locale') ,['en','ar'])){
                    return response()->json([
                        'message' => 'locale Not Correct',
                    ],400);
                }else{
                    App::setLocale(Input::get('locale'));
                    return $next($request);
                }
            }else{
                return response()->json([
                    'message' => 'locale Not Found',
                ],400);
            }
        }else if($request->isMethod('post') || $request->isMethod('patch')){
            $validator = Validator::make($request->all(), [
                'locale'    => 'required|in:ar,en',
            ]);
            if ($validator->fails())
            {
                return response()->json([
                    'message' => 'locale Not Correct',
                    'error_fields' => $validator->errors()
                ],400);
            }else{
                App::setLocale($request->input('locale'));
                return $next($request);
            }
        }
        return $next($request);
    }
}
