<?php

namespace App\Http\Middleware;

use App\Listing;
use Closure;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use JWTException;

class CheckListAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $list_id = $request->route()->parameter('list_id');
        $listing = Listing::find($list_id);

        if ($listing === NULL) {
            return response()->json([
                'message' => __('listing/api.list_not_found'),
            ],400);
        }

        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json([
                'message'=> __('listing/api.invalid_credentials')
            ],401);
        }

        $user_id = $user->id;

        $check_linking = Listing::where("member_id" , $user_id)->where("id",$list_id)->get();

        if(count($check_linking) === 0){
            return response()->json([
                'message' => __('listing/api.list_not_linked_to_member'),
            ],401);
        }
        return $next($request);
    }
}
