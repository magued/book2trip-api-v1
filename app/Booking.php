<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    protected $table = 'bookings';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'check_in', 'check_out', 'total', 'guests', 'status', 'created_at', 'updated_at', 'deleted_at',
        'guest_read', 'host_read', 'member_id', 'list_id', 'currency_id', 'is_offer', 'rate', 'amount',
    ];


    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_id');
    }


    public function toArray()
    {
        return $this->listing;
    }


}
