<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupAmenity extends Model
{
    protected $table = 'group_amenties';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'amenity_id', 'group_id', 'updated_at',  'deleted_at', 'created_at',
    ];

    /**
     * Return Group Listing Data As Object
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function amenities()
    {
        return $this->belongsTo('App\Amenity' , 'amenity_id');
    }

    public function toArray()
    {
        return [
            'amenities' => $this->amenities,
        ];

    }

}
