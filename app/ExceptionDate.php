<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExceptionDate extends Model
{

    protected $table = 'exception_date';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'start_date', 'end_date', 'status', 'reason', 'list_id',  'discount', 'amount',
        'updated_at',  'deleted_at', 'created_at',
    ];

    /**
     * Get Listing Data As Object
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'discount' => $this->discount
        ];
    }
}
