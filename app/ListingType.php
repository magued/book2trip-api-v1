<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class ListingType extends Model
{
    public $table = 'list_types';

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name
        ];
    }

}
