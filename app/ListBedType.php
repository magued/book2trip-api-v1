<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListBedType extends Model
{
    protected $table = 'list_beds_type';
    protected $fillable = ['list_id', 'bed_type_id', 'amount'];
    public function toArray()
    {

        return [
            'id' => $this->id,
            'list_id' => $this->list_id,
            'bed_type_id' => $this->bed_type_id,
            'amount' => $this->amount,
        ];
    }

}
