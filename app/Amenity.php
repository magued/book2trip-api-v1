<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Amenity extends Model
{
    // Table Name
    protected $table = 'amenities';


    public function listing(){

    	 return $this->belongsToMany('App\Listing', 'list_amenities', 'amenity_id', 'list_id');
    }

    /**
     * Get The Parent Amenitie Category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\CategoryAmenity', 'category_id');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name,
            'category_id' => $this->category_id,
            'category_name' => (App::getLocale() == 'en') ? $this->category->name_en : $this->category->name
        ];

    }

}
