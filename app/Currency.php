<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class Currency extends Model
{
    protected $table = 'currencies';

    /**
     * Used to convert any currency to dollar and return total amount in dollar
     * @param $currencyName
     * @param $amount
     * @return float <total amount>
     */
    public static function changeCurrencyToDollar($currencyId, $amount)
    {
        $currency = Currency::select('id', 'symbol', 'name_en', 'dollar_price')->where('id', $currencyId)->first();

        return round($amount * $currency['dollar_price'],1,PHP_ROUND_HALF_UP);
    }


    /**
         * Used to convert dollar to another currency
         * @param $currencyName
         * @param $amount
         * @return float
         */
    public static function changeDollarToAnotherCurrency($currencyId, $amount)
    {
        $currency = Currency::select('dollar_price')->where('id', $currencyId)->first();
        return round($amount / $currency->dollar_price,1,PHP_ROUND_HALF_UP);
    }


    /**
     * Used to change the currency by determined the currency name from and to
     * @param $from
     * @param $to
     * @param $amount
     * @return float
     */
    public static function change($from, $to, $amount)
    {
        if ($from === $to) {
            return $amount;
        }
        $amountInDollar = Currency::changeCurrencyToDollar($from, $amount);
        return Currency::changeDollarToAnotherCurrency($to, $amountInDollar);
    }

    /**
     * Used to convert any currency to dollar by id and return total amount in dollar
     * @param $currencyName
     * @param $amount
     * @return float <total amount>
     */
    public static function changeCurrencyToDollarById($currencyId, $amount)
    {
        $currency = Currency::select('id', 'symbol', 'name_en', 'dollar_price')->where('id', $currencyId)->first();

        return round($amount * $currency['dollar_price'],1,PHP_ROUND_HALF_UP);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name,
            'symbol' => $this->symbol,
        ];
    }



}
