<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Wishlist extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'wishlist';

    protected $fillable = [
        'id', 'list_id', 'member_id', 'wishlist_name', 'wishlist_public', 'created_at', 'updated_at', 'deleted_at'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id')->orderBy("id" , "DESC");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function toArray()
    {
        return $this->listing ;

    }
}
