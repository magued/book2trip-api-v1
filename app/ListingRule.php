<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class ListingRule extends Model
{
    protected $table = 'list_rules';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'list_id', 'rule_id', 'updated_at',  'deleted_at', 'created_at',
    ];

    public function adminRule()
    {
        return $this->belongsTo('App\AdminRule', 'rule_id');
    }

    public function toArray()
    {
        return [
//            'id' => $this->id,
            'rule' => (App::getLocale() == 'en') ? $this->adminRule->rule_en : $this->adminRule->rule,
        ];
    }


}
