<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingPhoto extends Model
{
    protected $table = 'list_photos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'photo', 'order_no', 'list_id', 'cover', 'updated_at',  'deleted_at', 'created_at',
    ];

    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id');
    }

    public function toArray()
    {
        return [
          'id'        => $this->id,
          'order_no'  => $this->order_no,
          'cover'     => $this->cover,
          'thumb'     => 'https://www.book2trip.com' . '/uploads/listing/thumb/'.$this->photo,
          'medium'    => 'https://www.book2trip.com' . '/uploads/listing/medium/'.$this->photo,
          'original'  => 'https://www.book2trip.com' . '/uploads/listing/'.$this->photo
        ];
    }
}
