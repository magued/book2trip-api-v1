<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class Member extends Authenticatable
{
    protected $table = 'members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'first_name', 'last_name','username', 'password', 'aboutme', 'address', 'address_public', 'email', 'email_public',
        'email_verified', 'website', 'gender', 'phone', 'phone_verified', 'phone_public', 'phone_verification_code', 'job',
        'job_public', 'speaks', 'photo', 'timezone', 'last_ip', 'country_id', 'city_id', 'facebook', 'facebook_verified',
        'facebook_public', 'twitter', 'twitter_verified', 'twitter_public', 'linkedin', 'linkedin_verified', 'linkedin_public',
        'identity', 'identity_verified', 'country_code', 'subscription', 'status', 'response_rate', 'response_time',
        'acceptance_rate', 'decline_rate', 'cancel_rate', 'remember_token', 'forget_token', 'email_code', 'created_at',
        'updated_at', 'deleted_at', 'currency_id', 'message_sms_setting', 'reservation_updates_sms_setting',
        'message_email_setting', 'reservation_updates_email_setting', 'last_country', 'last_currency', 'last_language',
        'birth_date', 'country_code_phone', 'google', 'google_public', 'google_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be protected from mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'password', 'username'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review','member_id');
    }

    public function groupListings()
    {
            return $this->hasMany('App\GroupListing','member_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking','member_id');
    }

    public function connectionsAsFriend()
    {
        return $this->hasMany('App\Connection','friend_id')->where("status", 1);
    }

    public function connectionsAsMembers()
    {
        return $this->hasMany('App\Connection','member_id')->where("status", 1);
    }

    public function myConnections($id = 0, $paginate = 10)
    {
        return Connection::where("member_id" , $id)->orWhere("friend_id" , $id)->where("status" , 1)->paginate($paginate);
    }


    public function matualconnections($id=0 , $id1=0){
        $matualIds = array();
        if($id != $id1){
            $friendConIds = $this->getfriendsIds($id);
            $myConIds = $this->getfriendsIds($id1);

            $matualIds = array_intersect($myConIds, $friendConIds);
        }

        $members = Member::whereIn("id" , $matualIds)->get();
        return $members;// $matualIds;
    }

    public function allmyconnections($id=0){

        $myArr = $this->getfriendsIds($id);
        $connections = Member::whereIn("id" , $myArr)->get();
        return $connections;
    }

    function getfriendsIds($id){
        $connectionsAsMember = Connection::where("member_id" , $id)->where("status" , '1' )->get();//->orWhere("friend_id" , $id)->where("status" , "=" , '1')->get();
        $connectionsAsFriend = Connection::where("friend_id" , $id)->where("status" , '1' )->get();
        $connectionsArr = array();
        foreach ($connectionsAsMember as $con){
            array_push($connectionsArr, $con->friend_id);
        }

        foreach ($connectionsAsFriend as $con){
            array_push($connectionsArr, $con->member_id);
        }
        return $connectionsArr;
    }

    public function conversationHosts ($memberId, $paginate = 10)
    {
        $list = Listing::where('member_id', $memberId)->pluck('id')->toArray();
        $booking = Booking::whereIn('list_id', $list)->orderBy('updated_at', 'desc')->paginate($paginate);

        return $booking ;
    }

    public function conversationGuests($memberId, $paginate = 10)
    {
        return Booking::where('member_id', $memberId)->orderBy('updated_at', 'desc')->paginate($paginate);
    }



    public function toArray()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'aboutme' => $this->aboutme,
            'address' => $this->address,
            'birth_date' => $this->birth_date,
            'job' => $this->job,
            'email' => $this->email,
            'email_verified' => $this->email_verified,
            'website' => $this->website,
            'gender' => $this->gender,
            'country_code_phone' => $this->country_code_phone,
            'phone' => $this->phone,
            'phone_verified' => $this->phone_verified,
            'last_language' => $this->last_language,
            'photo' => [
              'thumb'     => 'https://www.book2trip.com' . '/uploads/user/thumb/'.$this->photo,
              'medium'    => 'https://www.book2trip.com' . '/uploads/user/medium/'.$this->photo,
              'original'  => 'https://www.book2trip.com' . '/uploads/user/'.$this->photo
              ],
            'country' => $this->country,
            'city' => $this->city,
            'reviews' => $this->reviews->count(),
        ];
    }

}
