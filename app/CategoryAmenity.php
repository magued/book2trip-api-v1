<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class CategoryAmenity extends Model
{
    protected $table = 'amenities_category';

    /**
     * This Method Return All Amenity For Category
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function amenities()
    {
        return $this->hasMany('App\Amenity','category_id');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => (App::getLocale() == 'en') ? $this->name_en : $this->name
        ];

    }


}
