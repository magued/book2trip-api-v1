<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';

    public function member()
    {
        return $this->belongsTo('App\Member','member_id');
    }

    public function listing()
    {
        return $this->belongsTo('App\Listing', 'list_id');

    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'comment' => $this->comment,
            'rate' => $this->rate,
            'listing' => $this->listing,


        ];

    }
}
