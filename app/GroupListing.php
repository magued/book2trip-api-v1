<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupListing extends Model
{
    protected $table = 'group_listing';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'member_id', 'type', 'total_lists', 'picture',  'website', 'check_in',
        'check_out', 'address', 'governorate', 'city', 'country', 'building_number',  'zip_code', 'unit_number','country_short',
        'latitude', 'longitude', 'more_place_desc', 'step', 'updated_at',  'deleted_at', 'created_at',
    ];

    /**
     * Method Get All Member Data As Object
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo('App\Member','member_id');
    }

    /**
     * Method To Get  All Listings Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listings(){
        return $this->hasMany('App\Listing','group_id');
    }

    /**
     * Method to get All Amenities data As Object
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function amenities()
    {
        return $this->hasMany('App\GroupAmenity','group_id');
    }

    public function toArray()
    {
        switch ($this->type) {
            case 1:
                $this->type = ['id' => 1, 'title' => __('listing/api.resort')];
                break;
            case 2:
                $this->type = ['id' => 2, 'title' => __('listing/api.building')];
                break;
            case 3:
                $this->type = ['id' => 3, 'title' => __('listing/api.hotel')];
                break;
        }

        $this->picture = [
            'thumb'     => url('/') . '/uploads/grouplisting/thumb/'.$this->picture,
            'medium'    => url('/') . '/uploads/grouplisting/medium/'.$this->picture,
            'original'  => url('/') . '/uploads/grouplisting/'.$this->picture
        ];

        return [
            'id'                  => $this->id,
            'name'                => $this->name,
            'member'              => $this->member,
            'type'                => $this->type,
            'total_lists'         => $this->total_lists,
            'picture'             => $this->picture,
            'website'             => $this->website,
            'check_in'            => $this->check_in,
            'check_out'           => $this->check_out,
            'address'             => $this->address,
            'governorate'         => $this->governorate,
            'city'                => $this->city,
            'country'             => $this->country,
            'building_number'     => $this->building_number,
            'zip_code'            => $this->zip_code,
            'unit_number'         => $this->unit_number,
            'latitude'            => $this->latitude,
            'longitude'           => $this->longitude,
            'landmark'            => $this->more_place_desc,
            'step'                => $this->step,
            'amenities'           => $this->amenities,
            'lists'               => $this->listings,
        ];
    }
}
