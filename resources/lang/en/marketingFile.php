<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'hurghada' => 'Hurghada',
    'sharm'     => 'Sharm El Sheikh',
    'ainsokhna'     => 'Ain Sokhna ',
    'northCost'     => 'North Cost',
    'popularDestination'     => 'popular Destinations',

];
