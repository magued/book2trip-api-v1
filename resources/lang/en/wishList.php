<?php


return [
    'delWishList'=>'Do you want to remove this listing from your wish list?',
    'yes'=>'Yes',
    'no'=>'No',
    'doYouWantToAddThisListingToYourWishlisting'=>'Do you want to add this listing to your wish list? ',
    'close'=>'Close',
    'addToMyWishlisting'=>'Add to my wishlist',
    'addWishListing'=>'Add wishlist',
    'removeWishlisting'=>'Remove wishlist',
    'removeFromMyWishListing'=>'Remove from My Wish List',
    'wishlist'=>'Wish list',
    'updatedOn'=>'Updated On',
    'preview'=>'Preview',
    'delete'=>'Remove from my wishlist',
    'noWishlistingFound'=>'no wish listing found',
    'wish_del_success'=>' Wishlist Deleted',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];

