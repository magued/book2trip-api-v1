<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Language Lines
    |--------------------------------------------------------------------------
    */

    'manager'                                => 'Manager',
    'owner'                                  => 'Owner',

    'amenitiesAllCategories_success'         => 'Amenities Categories retrieved properly',
    'amenitiesByCategories_success'          => 'Amenities in this Category retrieved properly',
    'amenitiesAll_success'                   => 'All Amenities retrieved properly',

    'citiesAll_success'                      => 'All Cities retrieved properly',
    'countriesAll_success'                   => 'All Countries retrieved properly',
    'currenciesAll_success'                  => 'All Currencies retrieved properly',

    'listingManagerTypes_success'            => 'Listing Manager Types retrieved properly',
    'listingTypes_success'                   => 'Listing Types retrieved properly',
    'roomTypes_success'                      => 'Room Types retrieved properly',
    'phoneCodes_success'                     => 'All Phone Codes retrieved properly',

    'token_error'                            => 'Token Not Found',

    'listing_success'                        => 'Lisiting Details retrieved properly',
    'listing_error'                          => 'Listing Not Found',

    'listingSearch_success'                  => 'Search Results retrieved properly',
    'listingSearch_error'                    => 'No Search Results Found',

    'inboxType_error'                        => 'Inbox Type Not Provided',

    'inboxAsHost_success'                    => 'Members\' Inbox Conversations As a Host Retrieved Successfully',
    'inboxAsHost_error'                      => 'An Error Accured While Retrieving Members\' Inbox Conversations As a Host',

    'inboxAsGuest_success'                   => 'Members\' Inbox Conversations As a Guest Retrieved Successfully',
    'inboxAsGuest_error'                     => 'An Error Accured While Retrieving Members\' Inbox Conversations As a Guest',
    'get_listing_rules_success'              => 'Get Listing Rules Success',

];
