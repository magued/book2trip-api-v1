<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [
    'ready_to_list' => 'Ready to list your space! You need to choose between two types of units.',
    'single_listing' => 'Single Units',
    'single_listing_description' => 'A Single Unit is one that includes only one unit (e.g., apartment, villa, studio, etc.). You add the description of this unit and it is booked on its own.',
    'group_listing' => 'Multi-Units',
    'group_listing_description' => 'A Multi-Unit is one that includes a group of units that share the same address and shared amenities (e.g., a number of apartments in the same building or a number of chalets in the same resort). You add the common features of these units first and then you start adding each unit or units and their distinguished characteristics.',
    'select'=>'-Select-',
    'select_list_type'=>'Select Units Type',
    'singleUnit'=>'Single Unit',
    'groupListing'=>'Multi-Unit',
    'selectCreateType'=>'Select the units type to add your listing.',
    'close'=>'Close',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];
