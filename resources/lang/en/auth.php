<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'error6'      => 'Sorry, your account cannot be verified.',
    'error7'      => 'Sorry, this account has previously registered.',
    'error1'      => 'Invalid Login',
    'error5'      => 'Error in receiving email',
    'ban_msg'      => 'Your account has been suspended by the admin.',

];
