<?php

return array(
    'missingParameters' =>  'There are missing parameters.',
    'pleaseCompleteYourData' =>  'Please complete the missing data.',
    'youHaveToRefreshYourPageAgain' =>  'Please refresh the page.',
    'descriptionMustBeLessThan600Character' =>  'The description must be less than 600 characters.',
    'someThingWrong' =>  'Something went wrong.',
    'invalidGroupId' =>  'Invalid multi-unit ID',
    'invalidList' =>  'Invalid listing',
    'invalidListing' =>  'No listing is found with this data.',
    'uploadSuccess' =>  'Upload has been finished successfully.',
    'noPictureToUpload' =>  'No image is found to upload.',
    'notFoundPicture' =>  'No image is found with this data.',
    'thisListingDntHavePhotos' =>  'This listing doesn\'t contain any images.',
    'invalidYouTubeVideo' =>  'Invalid You Tube video; please check the URL.',
    'finishUpdatingPhotos' =>  'Images have been updated successfully.',
    'addPhotosTabTitle' =>  '3- Add Photos',
    'select_photos' =>  'Select Photos',
    'remove_file' =>  'Remove File',
    'cancel_uploads' =>  'Cancel',
    'minPics' =>  'You must choose 1 photo at least.',
    'dropfilesheretoupload' =>  'Upload',
    'ifPhotosDoesntAppear' =>  'If the photos do not appear, click ',
    'here' =>  'here.',
    'addVideoUrl' =>  'Add Video URL',
    'insertAYoutubeLink' =>  'Insert a You Tube link for a video displaying the real place to be booked.',
    'addYouTubeLink' =>  'Add You Tube link only',
    'uploadSuccessWithoutFiles' =>  'Some files are not uploaded because they are not photos.',
    'noPicturesFoundForThisListing' =>  'No photos are found for this listing.',
    'setCover' =>  'Set as Cover Photo',
    'cover' =>  'Cover Photo',
    'invalidYouTubeLink' =>  'This is an invalid You Tube URL.',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
	);

