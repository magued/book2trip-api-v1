<?php

return [
    'signup_paramters_not_correct' => 'signup_paramters_not_correct',
    'invalid_credentials' => 'Invalid credentials',
    'error_create_token' => 'Could not create token',
    'member_created' => 'Member create successfuly',
    'member_updated' => 'Member updated successfuly',
    'member_creation_error' => 'Member creation failure',
    'member_updated_error' => 'Member updated failure',
    'member_not_found' => 'Member Not Found',
    'member_details' => 'Member Details',
    'member_details_error' => 'get Member Details Failure',
    'login_success' => 'login success',
    'change_password_paramters_not_correct' => 'change_password_paramters_not_correct',
    'update_member_paramters_not_correct' => 'Update Member Parameters Not correct',
    'change_password_success' => 'change password success',
    'old_password_not_correct' => 'old password not correct',
    'get_single_listing' => 'get a single units of the member successfully',
    'get_group_listing' => 'get a group units of the member successfully',
    'get_wishlist_listing' => 'get a wishlist units of the member successfully',
    'get_my_trip_member' => 'get a my trips of the member successfully',
    'get_my_approval_booking_member' => 'get Approval Bookings of the member successfully',
    'get_my_pending_requests_member' => 'get Pending Requests of the member successfully',
    'get_connections_member' => 'get Connections of the member successfully',
    'add_request_error_fields' => 'add request error fields',
    'member_already_connected' => 'Member is already connected',
    'member_same_user' => 'Member is the same user',
    'connection_request_sent_success' => 'Connection Request Sent Successfully',
    'accept_request_error_fields' => 'accept request error fields',
    'cancel_request_error_fields' => 'cancel request error fields',
    'remove_request_error_fields' => 'remove request error fields',
    'add_wishlist_error_fields' => 'add wishlist error fields',
    'delete_wishlist_error_fields' => 'delete wishlist error fields',
    'connection_not_found' => 'Connection Not Found',
    'connection_accepted_success' => 'Connection Accepted Successfully',
    'connection_cancelled_success' => 'Connection Cancelled Successfully',
    'connection_removed_success' => 'Connection Removed Successfully',
    'get_members_reviews_as_host_success' => 'Get Member\'s Reviews as Host',
    'get_members_reviews_as_guest_success' => 'Get Member\'s Reviews as Guest',
    'list_already_wishlisted' => 'List Already Wishlisted',
    'list_wishlisted_success' => 'List Wishlisted Successfully',
    'add_to_wishlist_error' => 'List Wishlisted Failure',
    'remove_wishlist_success' => 'Remove Wishlisted Successfully',
    'remove_wishlist_error' => 'Remove Wishlisted Failure',
];
