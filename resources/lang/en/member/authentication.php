<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'DataNotCompleted' => 'Data Not Completed',
    'firstNameIsWrong' => 'first Name Is Wrong',
    'lastNameIsWrong' => 'last Name Is Wrong',
    'usernameIsWrong' => 'username Is Wrong',
    'EmailIsWrong' => 'Email is invalid.',
    'usernameOrEmailIsNotUnic' => 'user name Or Email Is Not Unic',
    'passwordMustBeGreaterThan8' => 'Password must contain more than 8 characters.',

];
