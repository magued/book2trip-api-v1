<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



return [

    "Post_Review_Guest"=>"Post a Review As Guest" ,
    "my_trips_reply_As_Guest"=>"my trips reply As Guest" ,
    "Guest_review_me"=>"Guest review to me" ,
    "reply_to_guest_review"=>"reply to guest review" ,
    "reviewsAsHost"=>"reviews as Host" ,
    "noReviewsFound"=>"No Reviews Found" ,
    "edit"=>"Edit" ,
    "view"=>"View" ,
    "reply"=>"Reply" ,
    "reviewsAsHost"=>"review as host" ,
    "reviewsAsGuest"=>"review as guest" ,
    "replyAsHost"=>"Reply as host" ,
    "replyAGuest"=>"Reply as guest" ,
    "clean"=>"Cleanliness" ,
    "location"=>"Location" ,
    "bedding"=>"Bedding" ,
    "value"=>"Pricing" ,
    "booking"=>"Booking" ,
    "overall"=>"Overall condition" ,
    "reviewAbout"=>"Review About" ,
    "yourReview"=>"your Review" ,
    "yourReply"=>"your Reply" ,
    "total"=>"total" ,
    "nights"=>"nights" ,
    "nights"=>"nights" ,
    "securityDeposit"=>"security Deposit" ,
    "cleaningFees"=>"cleaning Fees" ,
    "totalFees"=>"total Fees" ,
    "discount"=>"discount" ,
    "youhaveReportedThisListing"=>"you have Reported This Listing" ,
    "reportListing"=>"Report Listing" ,
];