<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [
	'Share_your_love_of_travel' =>  'Share your experience and love of travel and adventure',
    'When_friend_travels' =>  'Traveling is an adventure worth trying and it is even more enjoyable when you have your friends along.',
    'Import_your_contacts' =>  'Import your contacts from',
    'Gmail' =>  'Gmail',
    'Yahoo' =>  ' Yahoo! Mail',
    'Outlook' =>  'Outlook',
    'inviteSuccess' =>  'Your friend has been invited successfully.',
    'Enter_email_addresses'=>'Enter email address',
    'Invite_Friends'=>'Invite Friends',
    'Facebook'=>'Facebook',
    'putMoreThanOneEmail' => 'To add more than one email, insert (,).',
    'emailIsWrong'=> 'Please add email addresses.',
    'emailFormatisWrong' => 'Please check the emails you entered.',
    'Close'=>'Close',
    'submit'=>'Send',
    'checkAll' => 'Check All',
    'email'      => 'Email',
    'register'=>'Registered',
    'Success' =>  'Your friend has been invited successfully.',
    ];