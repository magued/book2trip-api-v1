<?php

return [

    'Login'=>'Sign In',
    'Email_Address'=>'Email Address',
    'Password'=>'Password',
    'Forgot_Password'=>'Forgot Password?',
    'remember_me'=>'Remember Me',
    'Sign_In'=>'Sign In',
    'Facebook'=>'Facebook',
    'Google'=>'Google+',
    'Twitter'=>'Twitter',
    'error'=>'Invalid username or password',
    'error2' => 'There is no such provider.',

] ;
