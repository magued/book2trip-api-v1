<?php

return [

    'contactUs'=>'Contact Us',
    'contactuscomment'=>'For further inquiries, please contact our team.',
    'optional'=>'Optional',
    'subject'=>'Subject',
    'guest'=>'Guest',
    'email'=>'Email',
    'phone'=>'Mobile Phone Number',
    'list_id'=>'Listing ID',
    'msg_text'=>'Message',
    'send'=>'Send',
    'facebook'=>'Facebook',
    'msg_success'=>'Your message has been sent successfully.',
    'SomeThing_else'=>'Other',
    'FeedBack'=>'Feedback',
    'I_want_to_list_my_place'=>'Listing my space',
    'My_holiday'=>'My holiday',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',

];
