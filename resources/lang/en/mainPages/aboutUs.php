  <?php

return array(

    'aboutus_body' =>  '<div class="col-md-12">
              <h2 class="section-title">About Us</h2>
            </div>
            <div class="col-md-6">

              <p class="inner-content">
                book2trip is the first Egyptian vacation rentals platform covering Egypt and the Middle East. Based in Cairo, Egypt, book2trip has decided to have a full Arabic version. Originating from the idea of combining traveling and profit, book2trip has built a platform that forms a meeting point for hosts and guests. Hosts list their spaces offering a full description of amenities and all the means of comfort that their spaces include to make guests’ stay enjoyable and pleasant. This vacation rentals platform allows hosts to display their properties for free.
              </p>
              <p class="inner-content">
                book2trip offers a suite of tools for travelers to search for the best convenient accommodations they wish to stay in, book online, write reviews on the properties and rate their performance, and get support. To gain all these benefits, travelers do not have to pay any charges neither for searching nor for booking. All what they need to do is to sign up.
              </p>
              

            </div>
            <div class="col-md-6">
              <p class="inner-content">
                book2trip also ensures hassle-free communication between hosts and guests through its internal messaging feature. This online platform enables travelers to meet their accommodation needs and offers access to unique rentals, allowing renters to explore various travel choices in different destinations around the world.
              </p>
              <p class="inner-content">
                For further inquiries, please email us on <a href="mailto:info@book2trip.com">info@book2trip.com</a>
              </p>
            </div>',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
);