  
<?php

return array(

    'TermsofUse_head' =>  'Terms of Use',
    'termsofuse_body' =>  ' <h4><strong>Guest Agreement</strong></h4>
              <h5><strong>Signing Up and Signing Into Your Account</strong></h5>
              <p class="inner-content">
                By creating an account as a guest, you agree to follow the Terms of Use and to enter correct and updated information about yourself according to the signing up process and maintain an up-to-date online profile. By requesting to book a certain property, you become a guest and you represent and guarantee the following: that you are eligible to book a property and that you are to abide by all the rules and payments for the property you are requesting to book; that the information you give about yourself when booking is not deceptive. By registering and signing into your account as well as requesting to book a property, you agree to provide proof of identification whenever asked for.
              </p>
              <h5><strong>Complaints and Suspension of Your Account</strong></h5>
              <p class="inner-content">
                In case book2trip knows about or receives any complaint from any person or entity regarding a certain booking activity which, according to its sole judgement, necessitates immediate suspension of your account (like engaging in activities that would be considered unfair practices in the renting industry), book2trip reserves the right to temporarily suspend your account without prior notice while investigating the complaint. If the complaint proves to be true, according to its sole discretion, book2trip shall suspend the account permanently without prior notice to the member.
              </p>
              <h5><strong>Restrictions on Content</strong></h5>
              <p class="inner-content">
                You guarantee that the content you add is solely your personal information, each piece of information in the designated place. You guarantee that no links to other websites, HTML, codes, or other developing languages are included in this information and if any are available, they will be removed without prior notice. You accept that no email addresses are written except in the places designated for them to prevent spam and email harvesting robots and if any are available, they will be removed without prior notice.
              </p>
              <h5><strong>Content License</strong></h5>
              <p class="inner-content">
                You hereby grant book2trip  the right to copyright and protect the content you enter from unauthorized illegal use or distribution by other entities that are unaffiliated and the right to pursue at law any person or entity that breaches this agreement. You shall fully indemnify, defend, and hold harmless book2trip from and against any and all actions of any member resulting from infringement or violation of copyright or other rights and of this agreement and Privacy Policy.
              </p>
              <h5><strong>Guest Responsibilities</strong></h5>
              <p class="inner-content">
                As a guest, you are responsible for and accept to abide by all the laws, rules, and regulations administering the rental business. book2trip is not a property management company. Renters willing to book properties will contact the property owners or hosts directly through their accounts or through the contact information that they added to their account. book2trip is not responsible for renters; guests are responsible to set their acceptable terms and conditions for the prospective properties they wish to rent and the prospective hosts they wish to deal with. Guests are responsible to abide by the rules of the properties they are booking and to keep safe all the contents and amenities of the property. Guests are also responsible to pay all the fees requested as agreed on through the website.
              </p>
              <h5><strong>Cancel Booking</strong></h5>
              <p class="inner-content">
                As a guest, you can cancel your booking at any time from your profile. In this case the cancellation fees policy set for the property you were booking applies.
              </p>
              <h5><strong>Banned Activities</strong></h5>
              <p class="inner-content">
                As a guest, you agree not to post or email any content that shows or contains any discrimination based on race, color, national origin, religion, sex, social status, and handicap. You agree not to post or email any content that violates the law or any content that is false, deceitful, misleading, or misinformative. You agree not to post or email any content that affects the right of any third party, including patents, copyright, trade secret, trademark, and any other proprietary right. You accept not to post any content that contains viruses, malware, any computer codes, or files and programs designed to interrupt, damage, or limit the functionality of any computer software or hardware or any telecommunication device. You accept not to post or email any content that disguises other illegal or forged content.
              </p>
              <h5><strong>Hosts’ Contact Information</strong></h5>
              <p class="inner-content">
                As a guest, you guarantee that you will always keep the contact information of your hosts confidential and under no circumstances will you disclose this information to other people, parties, or entities.
              </p>
              <h5><strong>Reviews and Ratings</strong></h5>
              <p class="inner-content">
                As a guest, you have the right to give ratings and write reviews for your hosts and properties booked concerning all that has to do with the renting process and to reply to the hosts’ reviews. Reviews and responses should not include any offensive, discriminatory, abusive language or any hints or expressions that breach the right of any individual or entity. You understand that book2trip has the right to delete any review that is by any means offensive according to its sole judgement.
              </p>
              <h5><strong>Bookings Confirmation</strong></h5>
              <p class="inner-content">
                A booking is confirmed when you receive an email and a notification through your profile after paying the fees agreed upon to the host. This will result in blocking the dates you booked on the property calendar.
              </p>
              <h5><strong>Terms of Payments</strong></h5>
              <p class="inner-content">
                As a guest, you are exclusively responsible for making your payments and you agree that payments are made directly to the host. The host can use the right to cancel your booking request if payments are not made within the due dates range. book2trip has no liability for any of your actions. Payments are discussed only between hosts and guests. book2trip is not and in no event is going to be a party of rental activity between you and the host.
              </p>
              <h5><strong>A Vacation Rental Platform</strong></h5>
              <p class="inner-content">
                book2trip does not charge neither listing fees nor booking charges. There are no hidden costs. book2trip is an online vacation rental platform which is a marketing tool connecting hosts and guests for booking vacation rentals.
              </p>
              <h5><strong>Guest Cancellation</strong></h5>
              <p class="inner-content">
                As a guest, you are able to cancel the booking at any time; if the booking is already confirmed, which means that payment is already made, then the host refunds you according to the cancellation policy set for the property booked.
              </p>
              <h5><strong>Liability of book2trip</strong></h5>
              <p class="inner-content">
                book2trip acts as a marketplace aiming at providing a marketing tool for travelers to book and rent properties. book2trip is not a part or party of your rental activities or transactions. book2trip is not responsible for chargebacks, cancellations, refunds, or any disputes between hosts and guests.
              </p>
              <h4><strong>Host Agreement</strong></h4>
              <h5><strong>Signing Up and Signing Into Your Account</strong></h5>
              <p class="inner-content">
                By signing up or signing into your account as a property owner or host, you agree to give accurate and updated data or information about yourself according to the signing up process and maintain an updated online profile to keep it accurate. By listing a property to your account, you become a host with a listing or listings and you represent and guarantee the following: that you own and control all the rights to the content posted including text, description, and photos and any other content you add or display in relation to the listings or you otherwise have unlawful right to add or distribute such content through this website; that the content you add including text, pictures, description, prices, and others is accurate and not confusing or deceptive; using, posting, or transmitting of such content does not breach any right of any person or entity. By signing up or signing into your account as well as adding a listing, you agree to provide proof of identification and ownership to the property or proof of authority to list it. book2trip has the right to reject, in its sole judgement, any listing or listings or remove any listing or listings, whether temporarily or permanently, based on the content and accuracy of such listing or listings or for any other reasons; and in such case, you expressly consent to releasing and holding book2trip harmless from any loss or liability that may result from such a decision.
              </p>
              <h5><strong>Complaints and Removing of Listings</strong></h5>
              <p class="inner-content">
                In case book2trip knows about or receives any complaint from any person or entity regarding a certain listing or renting activity which, according to its sole judgement, necessitates immediate deletion or suspension of this listing from the website (like engaging in activities that would be considered unfair practices in the renting industry), book2trip reserves the right to temporarily remove this allegedly offensive listing without prior notice to the member while investigating the complaint. If the complaint proves to be true, according to its sole discretion, book2trip shall remove this offensive listing from the site without prior notice to the member.
              </p>
              <h5><strong>Restrictions on Content</strong></h5>
              <p class="inner-content">
                You guarantee that each listing represents a certain and uniquely identified property available for rent for a short term. book2trip has the right to amend or remove any listing when more than one property is described. You accept that each listing is subject to being checked and reviewed and that book2trip has the right to edit or improve any listing according to the design and functionality of the website without prior notice. You guarantee that no links to other websites, HTML, codes, or other developing languages are included in the listings and if any are available, they will be removed without prior notice. You accept that no email addresses are written in the listings except in the places designated for them to prevent spam and email harvesting robots and if any are available, they will be removed without prior notice.
              </p>
              <h5><strong>Content License</strong></h5>
              <p class="inner-content">
                You hereby grant boo2trip a right to use, reproduce, modify, edit, translate, distribute, and publish the content of listings and to publicly display such content all over the world; the right to use the name affiliated with such content; the right to copyright and protect the content of the listings from unauthorized illegal use or distribution by other entities that are unaffiliated and the right to pursue at law any person or entity that breaches this agreement. You shall fully indemnify, defend, and hold harmless book2trip from and against any and all actions of any member resulting from infringement or violation of copyright or other rights of this agreement and Privacy Policy. You agree that book2trip may reproduce in whole or in part any material of the listings in the promotion of your property or the promotion of the website.
              </p>
              <h5><strong>Other Rights in Content</strong></h5>
              <p class="inner-content">
                You agree that the content you add to your listing is nonconfidential and that book2trip reserves the right to use the content as deemed appropriate including modifying, editing, rejecting, and refusing to post it. book2trip is under no condition to offer you any payment for content you submit or the right to modify, edit, or delete this content once submitted.
              </p>
              <h5><strong>Host Responsibilities</strong></h5>
              <p class="inner-content">
                As a host, you are responsible for and accept to abide by all the laws, rules, and regulations administering advertising for your property and the rental business. book2trip is not a property management company. Renters willing to book your property will contact you directly through your account or through the contact information that you added to your account. book2trip is not responsible for renters; you are responsible to set your acceptable terms and conditions for your prospective renters.
              </p>
              <h5><strong>Unlist Option</strong></h5>
              <p class="inner-content">
                As a host, you can “Unlist” any of your listings at any time from “My Listings” page in your profile. This option will make the listing you choose invisible to the guests.
              </p>
              <h5><strong>Banned Activities</strong></h5>
              <p class="inner-content">
                As a host, you agree not to post or email any content in your listings that shows or contains any discrimination based on race, color, national origin, religion, sex, social status, and handicap. You agree not to post or email any content that violates the law or any content that is false, deceitful, misleading, or misinformative or any content that constitutes bait-and-switch. You agree not to post or email any content that affects the right of any third party, including patents, copyright, trade secret, trademark, and any other proprietary right. You accept not to post any content that contains viruses, malware, any computer codes, or files and programs designed to interrupt, damage, or limit the functionality of any computer software or hardware or any telecommunication device. You accept not to post or email any content that disguises other illegal or forged content.
              </p>.
              <h5><strong>Listings Precision</strong></h5>
              <p class="inner-content">
                Being a host, the information that you add to your listings should be accurate, precise, and true and you guarantee that all the material added is trustworthy because the guests rely on this information in choosing their best place. You should be aware that adding false or misleading information results in liability and other consequences that include but are not limited to removing your listing or suspending your account. book2trip and its users are free of any responsibility or obligation that may arise as a consequence of your misleading content. You are able to provide rules and rental regulations for the use of your property but book2trip is not obliged to monitor or force any rules or regulations and is not responsible for the violation of such rules or regulations from your side or the guest’s. book2trip is not and in no event is going to be a party of rental activity between you and the guest.
              </p>
              <h5><strong>Guests’ Contact Information</strong></h5>
              <p class="inner-content">
                As you host, you guarantee that you will always keep the contact information of your guests confidential and under no circumstances will you disclose this information to other people, parties, or entities.
              </p>
              <h5><strong>Reviews</strong></h5>
              <p class="inner-content">
                As a host, you have the right to write reviews for your guests concerning all that has to do with the renting process and to reply to the guests’ reviews and ratings. Reviews and responses should not include any offensive, discriminatory, abusive language or any hints or expressions that breach the right of any individual or entity. You understand that book2trip has the right to delete any review or response that is by any means offensive according to its sole judgement.
              </p>
              <h5><strong>Bookings Confirmation</strong></h5>
              <p class="inner-content">
                Confirming a booking takes place when you press “Approve” and this should happen when you receive the fees from the guest. Approving a booking will result in blocking the dates on your calendar. You guarantee that you do not double-book any property because this will result in suspending your property listing and your account.
              </p>
              <h5><strong>Terms of Payments</strong></h5>
              <p class="inner-content">
                As a host, you are exclusively responsible on your behalf for collecting your listings payments and you agree that payments are made directly to you. You agree to make the property available to the guest as if you had received the total fee and if you do not receive the payment within the due dates range, you can use your right to cancel the request. book2trip has no liability for any of your actions. Payments are discussed only between hosts and guests. book2trip is not and in no event is going to be a party of rental activity between you and the host.
              </p>
              <h5><strong>A Vacation Rental Platform</strong></h5>
              <p class="inner-content">
                book2trip does not charge neither listing fees nor booking charges. There are no hidden costs. book2trip is an online vacation rental platform which is a marketing tool connecting hosts and guests for booking vacation rentals.
              </p>
              <h5><strong>Host Cancellation</strong></h5>
              <p class="inner-content">
                As a host, you are able to cancel the booking at any time; if the booking is already confirmed, then you accept to refund the fees paid to the guest according to the Cancellation Fees Policy set for the properties booked. On the other hand, the guest gets the right for writing a review stating the reason. Moreover, cancellation will affect your Cancellation Rate. Prospective guests are able to see your Cancellation Rate and a higher than average Cancellation Rate would reduce the number of bookings you receive and this would result in temporarily or permanently removing your listing without any prior notice.
              </p>
              <h5><strong>Liability of book2trip</strong></h5>
              <p class="inner-content">
                book2trip acts as a marketplace aiming at providing a marketing tool for travelers to book and rent properties. book2trip is not a part or party of your rental activities or transactions and does not ensure that guests make the payments due in relation to their bookings. book2trip is not responsible for chargebacks, cancellations, refunds, or any disputes between hosts and guests.
              </p>
              <p>
                Any rights not expressly granted herein are reserved.
              </p>',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
);