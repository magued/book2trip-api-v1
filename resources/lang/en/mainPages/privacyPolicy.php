<?php

return array(
'PrivacyPolicy_title'=>'Privacy Policy',
    'privacy_policy_body' =>  '
              <p class="inner-content">
                book2trip has compiled this Privacy Policy to better serve those who are concerned with how their personal information is being used online. Personal information is any piece of information which could be used on its own or in relation with other pieces of information to identify, contact, or locate an individual. book2trip is keen on keeping your privacy safe. Please read this Privacy Policy carefully to clearly understand how book2trip collects, uses, protects, or otherwise handles your personal information. Please note that this Privacy Policy governs your use of book2trip. By visiting book2trip, you agree to the practices described herein.
              </p>
              <p class="inner-content">
                TRUST: book2trip believes that online planning to travel or online renting as well as online advertising for your property involves a great deal of trust from your side. It takes this trust seriously, marking the privacy and confidentiality of your personal information as the highest priority.
              </p>
              <h4><strong>What personal information does book2trip collect from the users that visit the website?</strong></h4>
              <h5><strong>General Information</strong></h5>
              <p class="inner-content">
                book2trip gathers and keeps any information you enter on the website when you surf the website, sign up, request to book, add a listing, approve a booking, fill a form, sign up for the newsletter of the website, or do any other activity on the website. This information includes your first and last names, email address, physical address, phone number, ID number, photos, social media accounts, and other details from you. book2trip also requests information about your traveler preferences.
              </p>
              <p class="inner-content">
                Information from Other Sources
              </p>
              <p class="inner-content">
                book2trip does not currently collects information from independent third-party sources to add to your user account.
              </p>
              <h5><strong>Automatic Information</strong></h5>
              <p class="inner-content">
                book2trip gathers some information related to your computer automatically when you enter the website, like collecting your IP address, your online activities, for example, properties viewed, bookings sent, bookings approved.
              </p>
              <h4><strong>How does book2trip use your personal information?</strong></h4>
              <p class="inner-content">
                book2trip uses the information it gathers to personalize your user experience, allow delivering the type of content, listings, and offerings in which you are interested the most. It also uses this information to improve the website in order to better serve you in responding to your customer service requests, as well as providing you with travel confirmations and updates. book2trip uses this information to administer contest, promotion, surveys, and other site features, to ask for ratings and reviews of properties and property owners, and to follow up your inquiries. Most important, book2trip uses this information to verify the identity of the users whether guests or hosts to add more reliability to the website, help increase the level of safety and security of the users, and decrease chances of fraud that may take place. This feature of user verification allows you know more about the person you are dealing with whether as a guest or as a host. Generally, book2trip makes use of this information for your own benefit, contacting you, enforcing the Terms of Use, or as otherwise stated at the point of collection. If you agree to enter your information to the website, then you agree that this information is added to the database.
              </p>
              <h4><strong>How does book2trip protect your personal information?</strong></h4>
              <p class="inner-content">
                book2trip highly cares about you feeling confident using it in order to rent a property for your vacation or list your property for renting by others, so book2trip is committed to protecting the information it collects. book2trip does not sell user information including names, contact information, phone numbers, email addresses, or any other personal information to third parties. Despite the fact that no website can guarantee safety and security, book2trip has employed suitable security procedures to help protect the personal information you enter. Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have access rights to such systems and are required to keep the information confidential. book2trip implements  a variety of security measures when a user does any activity on the website or accesses the information to maintain the safety of this personal information. book2trip also implements firewalls and intrusion detection systems to help prevent unauthorized persons from gaining access to your information.
              </p>
              <h5><strong>External Links</strong></h5>
              <p class="inner-content">
                If any part of book2trip website links you to other websites, those other websites do not undergo this Privacy Policy, so you are highly recommended to check their privacy policies to know their procedures for gathering, using, and disclosing personal information.
              </p>
              <h4><strong>How can you access your personal information?</strong></h4>
              <p class="inner-content">
                You can choose not to provide any information although this would affect your ability to use all book2trip features of renting a property for your vacation or listing your property. You can access your personal information when you log in to your account through different pages including the following: My Profile page, where you can view all your personal information, Edit My Profile page, where you can edit or update all the information you provided and change your password, and Accounts page, where you can check your social media accounts that you have linked to your profile.
              </p>
              <h4><strong>What are your choices regarding gathering and using your information?</strong></h4>
              <p class="inner-content">
                As previously mentioned, you could choose not to provide any information, although this would affect your ability to use all book2trip features of renting a property, contacting owners, or listing your property and the other features that book2trip provides. You could also choose to add or update the information you entered as explained before.
              </p>
              <h4><strong>When would book2trip share your personal information with other entities?</strong></h4>
              <p class="inner-content">
                book2trip would share your information in order to respond to subpoenas, court orders, or other legal processes, to establish or exercise legal rights, to investigate, prevent, or take action regarding illegal or suspected illegal activities, to protect and defend the rights, properties, or safety of book2trip, its customers, or others, and to follow the Terms of Use and other agreements.
              </p>
              <h4><strong>Does book2trip use cookies?</strong></h4>
              <p class="inner-content">
                book2trip uses cookies. Cookies are small files that a website or its service provider transfers to your computer’s hard drive through your web browser (if you allow) that enable the website’s or service provider’s systems to recognize your browser and capture and remember certain information. book2trip uses cookies to understand and save user’s preferences for future visits based on previous or current site activities, help remember your data to log in easily, and help provide you with improved services, as well as compiling data about the site traffic and site interaction in order to offer better site experience and tools in the future.
              </p>
              <p class="inner-content">
                For instance, when you log in to your account as a book2trip member, your member ID and the name on your member account are recorded in the cookie file on your computer. Your password may be recorded as well if you ask to remember the password for this website.
              </p>
              <p class="inner-content">
                You can choose to have your computer warn you each time a cookie is being sent or you can choose to turn off all cookies. You do this through your browser settings. Since browsers are a little different, look at your browser’s Help Menu to learn the correct way to modify your cookies.
              </p>
              <p class="inner-content">
                If you turn cookies off, some features will be disabled. However, you will still be able to rent a property or list your property.
              </p>
              <h4><strong>Does book2trip use third-party links?</strong></h4>
              <p class="inner-content">
                book2trip does not sell, trade, or otherwise transfer to outside parties your personal information. It does not include or offer third-party products or services.
              </p>
              <h4><strong>How does book2trip notify you of the changes in this Privacy Policy?</strong></h4>
              <p class="inner-content">
                book2trip may update this Privacy Policy in the future; accordingly book2trip is committed to notifying the users of any changes that take place through adding a clear notification to the website which appears with the first login and through the Privacy Policy page.
              </p>
              <h4><strong>How can you contact book2trip team?</strong></h4>
              <p class="inner-content">
                If you have any questions regarding this Privacy Policy, you can contact book2trip team via the following email: <a href="mailto:info@book2trip.com"> info@book2trip.com</a>. You could also contact book2trip through the social media pages.
              </p>
              <p>
                <strong>This Privacy Policy is effective as of 20/11/2016.</strong>
              </p>
              ',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    );