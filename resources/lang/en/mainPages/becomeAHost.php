<?php

return array(

'header' =>  '<header class="main-header">
        <h1 style="color:#fff">List your space in the easiest way.</h1>
        <h1 style="color:#fff">Only three steps to renting business and profit.</h1>
        <p>
          <span type="button" class="btn-scroll" id="toSearch">1. Sign Up</span> <span id="toBook" type="button" class="btn-scroll">2. List Your Space</span> <span id="toTravel" type="button" class="btn-scroll">3. Invite Your Friends</span>
        </p>
    </header>',


    'SignUp' =>  '<div class="search" id="search">
            <div class="column copy">
                <h2 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">1. Sign Up</h2>

                <h2 class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">Create an account to list your space. It is totally for free. Reap the profit with a click on <a href="'.url('login/signup').'" class="signUpNow" target="_blank">Sign Up</a></h2>

            </div>
            <div class="column brigi"><img class="anim animated fadeInRight" data-anim="fadeInRight" data-anim-delay="0s" src="../img/htt.png" alt="Search" style="animation-delay: 0s;"></div>
      </div>',

    'ListYourSpace' =>  ' <div class="column copy">
                <h2 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">2.	List Your Space</h2>
                <ol class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">

                    <li>Choose the type of unit you wish to add.
                        <ul class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">
                            <li>Single Unit: this type best fits listing a single property that has certain features and unique description like listing a single apartment, a single room, and so on.</li>
                            <li>Multi-Unit: this type best fits listing more than one property sharing the same address and shared amenities. For example, choose this type if you own more than one apartment in the same building.</li>
                        </ul>
                    </li>
                    <li>
                      Add eye-catching photos and video that best display your space.
                    </li>
                    <li>
                       Add the rates per night and customize your calendar. Do not forget that there are no listing fees, so be reasonable to attract more travelers.
                    </li>
                    <li>
                       Follow the requests you receive from guests to book your place through your Inbox and enjoy the feature of Send Offer to attract guests.
                    </li>
                </ol>
            </div>',
    'InviteYourFriends' =>  '<div class="column copy equal" style="height: 704px;">
            <h1 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">
                3.	Invite Your Friends
            </h1>
            <h2 class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">Invite your friends to list their spaces and book for traveling. You can always share your space on your social media accounts to attract more excited travelers. </h2>
        </div>',
    'Start_Now' =>  'Start Now',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    );