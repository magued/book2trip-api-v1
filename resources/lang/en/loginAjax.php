<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'continueWithFaceBook' => 'Continue with facebook',
    'continueWithGoogle' => 'Continue with google+',
    'continueWithTwitter' => 'Continue with twitter',
    'signUpWithEmail' => 'Sign up with Email',
    'alreadyHaveAnAcount' => 'already have an account',
    'login' => 'Login',
    'or' => 'or',
    'enterUsername' => 'Enter username',
    'enterPassword' => 'Enter Password',
    'showPassword' => 'Show Password',
    'hidePassword' => 'Hide Password',
    'DontHaveAnAccount' => "Don't have an account?",
    'signup' => 'Sign up',
    'signupWith' => 'Sign up With',
    'facebook' => 'Facebook',
    'google' => 'Google',
    'book2tripTerms' => "I agree to book2trip's Terms of Use and Privacy Policy.",
    'iHaveAnAccount' => 'i have an account?',
    'username' => 'Username',
    'firstName' => 'First Name',
    'lastName' => 'Last Name',
    'email' => 'Email',
    'password' => 'Password',
    'ban_msg' => 'Sorry this user got banned',
    'invalidUsername' => 'This username exist',
    'invalidEmail' => 'This email exist',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

];