<?php

return [
    'status'=>'الحالة',
    'Waiting_for_approval'=>'في انتظار الموافقة عليها',
    'finshed'=>'كاملة',
    'un_list'=>'إيقاف عرض الوحدة',
    'uncomplete'=>'غير كاملة',
    'unlist'=>'إيقاف عرض الوحدة',
    'waitingApprove'=>'في انتظار الموافقة عليها',
    'Single_Unit'=>'وحدة مفردة',
    'You_are'=>'انتهيت من ',
    'done_with_your_listing'=>'% من خطوات إضافة وحدتك.',
    'list'=>'عرض الوحدة',
    'updatedOn'=>'تحديث في',
    'preview'=>'استعراض',
    'update'=>'تحديث',
    'finishYourList'=>'إنهاء القائمة الخاصة بك',
    'youDontHaveListing'=>'ليس لديك قائمة',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ];