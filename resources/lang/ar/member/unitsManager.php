<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "myTrips"=>"رحلاتى",
    "updatedOn"=>"تحديث فى",
    "preview"=>"عرض",
    "viewConversation"=>"عرض المحادثة",
    "noTripsFound"=>"لم يتم العثور على رحلات",
    "singleUnits"=>"وحدات منفردة",
    "myWishListing"=>"قائمتى المفضلة",
    "approvedBooking"=>"حجز مؤكد",
    "multiUnits"=>"وحدات مزدوجة",
    "noApprovedBookingFounds"=>"لم يتم العثور على حجز مؤكد",
    "noMultiListingsFound"=>"لم يتم العثور على وحدات مزدوجة",
    "totalLists"=>"مجموع القوائم:",
    "update"=>"تحديث",
    "finishYourList"=>"انهى قائمتك",
    "noListingFound"=>"لم يتم العثور على قوائم",
    "newSubUnit"=>"ادخال وحدات جديده",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
];