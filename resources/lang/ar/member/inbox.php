<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "asHost"=>"كمالك",
    "asGuest"=>"كضيف",
    "connectionsRequest"=>"طلبات الاتصالات",
    "similarListings"=>"قوائم مشابهة",
    "noMessagesFound"=>"لم يتم العثور على أية رسائل",
    "nights"=>"الليالى",
    "connectionsRequests"=>"طلبات الاتصالات",
    "noRequestsFound"=>"لم يتم الغثور على طلبات الاتصالات",
    "email_accept_request"=>"أقبل طلبك",
    "new_user_added_to_connections"=>"تم إضافة مستخدم جديد إلى اتصالاتك",
    "no_connection"=>"لم يتم العثور على اتصالات",
    "something_wrong"=>"شىء خاطىء",
    "removed_success"=>"نم إلغاء هذا الاتصال",
    "decline"=>"رفض",
    "accept"=>"قبول"
];