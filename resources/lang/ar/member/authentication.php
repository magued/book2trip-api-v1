<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'DataNotCompleted' => 'Data Not Completed',
    'firstNameIsWrong' => 'first Name Is Wrong',
    'lastNameIsWrong' => 'last Name Is Wrong',
    'usernameIsWrong' => 'username Is Wrong',
    'EmailIsWrong' => 'خطأ في البريد الإلكتروني.',
    'usernameOrEmailIsNotUnic' => 'user name Or Email Is Not Unic',
    'passwordMustBeGreaterThan8' => 'يجب أن تحتوي كلمة المرور على 8 حروف على الأقل.',

];
