<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Language Lines
    |--------------------------------------------------------------------------
    */

    'manager'                             => 'المالك',
    'owner'                               => 'الوسيط',

    'amenitiesAllCategories_success'      => 'تم تحميل تصنيفات المرافق بنجاح',
    'amenitiesByCategories_success'       => 'تم تحميل المرافق داخل هذا التصنيف بنجاح',
    'amenitiesAll_success'                => 'تم تحميل جميع المرافق بنجاح',

    'citiesAll_success'                   => 'تم تحميل جميع المدن بنجاح',
    'countriesAll_success'                => 'تم تحميل جميع الدول بنجاح',
    'currenciesAll_success'               => 'تم تحميل جميع العملات بنجاح',

    'listingManagerTypes_success'         => 'تم تحميل أنواع مديري الوحده بنجاح',
    'listingTypes_success'                => 'تم تحميل أنواع الوحدات بنجاح',
    'roomTypes_success'                   => 'تم تحميل أنواع الغرف بنجاح',
    'phoneCodes_success'                  => 'تم تحميل جميع أكواد الهاتف بنجاح',

    'token_error'                         => 'رمز الدخول غير موجود',

    'listing_success'                     => 'تم تحميل بيانات الوحده بنجاح',
    'listing_error'                       => 'الوحده غير موجوده',

    'listingSearch_success'               => 'تم تحميل نتائج البحث بنجاح',
    'listingSearch_error'                 => 'لا توجد نتائج بحث',

    'inboxType_error'                     => 'لم يتم التعرف على نوع الرسائل المراد تحميلها',
    'inboxAsHost_success'                 => 'تم تحميل الرسائل الوارده كمضيف',
    'inboxAsHost_error'                   => 'تم تحميل الرسال الوارده كضيف'
];
