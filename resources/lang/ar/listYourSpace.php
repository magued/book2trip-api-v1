<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [
    'ready_to_list' => 'هل تريد إضافة وحدتك؟ عليك أن تختار بين نوعين من الوحدات.',
    'single_listing' => 'الوحدات المفردة',
    'single_listing_description' => 'الوحدة المفردة تحتوي على وحدة واحدة فقط قائمة بذاتها (مثل شقة أو فيلا أو ستوديو) وبذلك تقوم بإدخال وصف تلك الوحدة ويتم حجزها بمفردها.',
    'group_listing' => 'مجموعات الوحدات',
    'group_listing_description' => 'مجموعة الوحدات تحتوي على عدد من الوحدات لهم نفس العنوان والمرافق المشتركة (مثل عدد من الشقق في نفس المبنى أو عدد من الشاليهات في نفس المنتجع) وبذلك تقوم بإدخال الخصائص الوصف التفصيلي لكل وحدة أو وحدات.',
    'select'=>'-اختر-',
    'select_list_type'=>'اختر نوع الوحدة.',
    'singleUnit'=>'الوحدات المفردة',
    'groupListing'=>'مجموعات الوحدات',
    'selectCreateType'=>'اختر نوع الوحدات لإضافتها.',
    'close'=>'إغلاق',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];
