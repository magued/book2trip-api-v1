<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "يجب أن تحتوي كلمة المرور على 8 حروف على الأقل.",
    "user" => "لم نتمكن من العثور على مستخدم بهذا البريد الإلكتروني.",
    "token" => "رمز إعادة تعيين كلمة المرور غير صالح.",
    "sent" => "تم إرسال كلمة مرور جديدة. من فضلك ارجع إلى بريدك الإلكتروني.",
    "reset" => "تم تغيير كلمة المرور بنجاح.",
	"emailIsWrong"=>"هذا البريد الإلكتروني غير صحيح.",
	"ForgetPasswordEmailIsWrong"=>"هذا البريد الإلكتروني غير موجود في قاعدة البيانات.",

];
