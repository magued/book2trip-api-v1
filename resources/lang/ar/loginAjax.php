<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'continueWithFaceBook' => 'تواصل مع الفيسبوك',
    'continueWithGoogle' => 'تواصل مع جوجل ',
    'continueWithTwitter' => 'تواصل مع تويتر',
    'signUpWithEmail' => 'سجل عن طريق البريد الإلكتروني',
    'alreadyHaveAnAcount' => 'بالفعل يوجد  حساب بهذه البيانات',
    'login' => 'تسجيل الدخول',
    'or' => 'او',
    'enterUsername' => 'ادخل اسم المستخدم',
    'enterPassword' => 'أدخل كلمة المرور',
    'showPassword' => 'عرض كلمة المرور',
    'hidePassword' => 'اخفاء كلمة المرور',
    'DontHaveAnAccount' => "ليس لديك حساب؟",
    'signup' => 'سجل',
    'signupWith' => 'سجل مع',
    'facebook' => 'فيس بوك',
    'google' => 'جوجل',
    'book2tripTerms' => "أوافق على شروط الاستخدام و سياسة الخصوصية.",
    'iHaveAnAccount' => 'لدي حساب؟',
    'username' => 'اسم المستخدم',
    'firstName' => 'الاسم الاول',
    'lastName' => 'اسم العائلة',
    'email' => 'البريد الإلكتروني',
    'password' => 'كلمه السر',
    'ban_msg' => 'آسف تم حظر هذا المستخدم',
    'invalidUsername' => 'هذا اسم المستخدم موجود',
    'invalidEmail' => 'هذه الرسالة الإلكترونية موجودة',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

];