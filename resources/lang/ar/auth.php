<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'هذه البيانات لا تطابق البيانات المسجلة لدينا.',
    'throttle' => 'محاولات كثيرة لتسجيل الدخول. من فضلك أعد المحاولة بعد :seconds ثانية.',
    'error5'      => 'خطأ في استلام البريد الإلكتروني.',
    'error6'      => 'عفوا، لا يمكن التحقق من هذا الحساب.',
    'error1'      => 'تسجيل دخول غير صحيح',
    'error7'      => 'عفوا، هذا الحساب قام بالتسجيل من قبل.',
    'ban_msg'      => 'تم إيقاف حسابك بواسطة المشرفين على الموقع.',

];
