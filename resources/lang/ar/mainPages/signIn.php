<?php

return [

    'Login'=>'تسجيل الدخول',
    'Email_Address'=>'البريد الإلكتروني',
    'Password'=>'كلمة المرور',
    'Forgot_Password'=>'هل نسيت كلمة المرور؟',
    'remember_me'=>'تذكرني',
    'Sign_In'=>'تسجيل الدخول',
    'Facebook'=>'Facebook',
    'Google'=>'Google+',
    'Twitter'=>'Twitter',
    'error'=>'خطأ في اسم المستخدم أو كلمة المرور',
    'error2' => 'ليس هناك مزود من هذا القبيل.',
] ;
