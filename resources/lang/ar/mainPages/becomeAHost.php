<?php

return array(

'header' =>  '<header class="main-header">
        <h1 style="color:#fff">الآن تستطيع عرض وحدتك بأسهل طريقة ممكنة.</h1>
        <h1 style="color:#fff">فقط ثلاث خطوات لتربح من تأجير عقارك.</h1>
        <p>
          <span type="button" class="btn-scroll" id="toSearch">1.	إنشاء حساب جديد</span> <span id="toBook" type="button" class="btn-scroll">2. إضافة وحدتك</span> <span id="toTravel" type="button" class="btn-scroll">3. دعوة الأصدقاء</span>
        </p>
    </header>',


    'SignUp' =>  '<div class="search" id="search">
            <div class="column copy">
                <h2 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">1. إنشاء حساب جديد</h2>

                <h2 class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">قم بإنشاء حساب جديد لإضافة وحدتك بدون دفع أي رسوم. اربح الآن من خلال <a href="'.url('login/signup').'" class="signUpNow" target="_blank">إنشاء حساب جديد</a></h2>

            </div>
            <div class="column brigi"><img class="anim animated fadeInRight" data-anim="fadeInRight" data-anim-delay="0s" src="../img/htt.png" alt="Search" style="animation-delay: 0s;"></div>
      </div>',

    'ListYourSpace' =>  ' <div class="column copy">
                <h2 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">2. أضف وحدتك</h2>
                <ol class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">

                    <li>اختر نوع الوحدة
                        <ul class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">
                            <li>الوحدة المفردة: هو الاختيار المناسب عند إضافة عقار قائم بذاته له وصف ومرافق خاصة به مثل إضافة شقة واحدة أو غرفة واحدة.</li>
                            <li>مجموعة الوحدات: هو الاختيار المناسب عند إضافة أكثر من عقار لهم نفس العنوان والمرافق المشتركة مثل إضافة أكثر من شقة في نفس العمارة.</li>
                        </ul>
                    </li>
                    <li>
                      أضف صور شيقة وفيديو لعرض وحدتك بطريقة رائعة.
                    </li>
                    <li>
                       أضف أسعار مناسبة لتجذب الزائرين وتذكر دائماً أنه لا يوجد أي رسوم لإضافة وحداتك.
                    </li>
                    <li>
                       تابع طلبات تأجير وحدتك عن طريق صندوق الوارد الخاص بك وتمتع بخاصية إرسال عرض لجذب الزائرين.
                    </li>
                </ol>
            </div>',
    'InviteYourFriends' =>  '<div class="column copy equal" style="height: 704px;">
            <h1 class="section-title anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">
                3. دعوة الأصدقاء
            </h1>
            <h2 class="anim animated fadeIn" data-anim="fadeIn" data-anim-delay="0s" style="animation-delay: 0s;">قم بدعوة أصدقائك لإضافة وحداتهم وحجز الوحدات للسفر وتذكر دائماً أنه يمكنك مشاركة وحدتك عبر مواقع التواصل الاجتماعي لعرضها على عدد أكبر من المسافرين.</h2>
        </div>',
    'Start_Now' =>  'إبدأ الآن',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    '' =>  '',
    );