<?php

return array(
'PrivacyPolicy_title'=>'سياسة الخصوصية',
    'privacy_policy_body' =>  '
              <p class="inner-content">
                لقد قامت book2trip ببناء سياسة الخصوصية من أجل تقديم خدمة أفضل لمن لديهم مخاوف بشأن كيفية استخدام بياناتهم الشخصية على شبكة الإنترنت. البيانات الشخصية هي أي معلومة يمكن أن يتم استخدامها بمفردها أو بالإضافة إلى معلومات أخرى للتعرف على أو للتواصل مع أو لتحديد موقع أي فرد. فإن book2trip تحرص على الحفاظ على خصوصية بياناتك لذا يُرجى الاطّلاع على هذه السياسة بعناية من أجل الوصول لفهم واضح لطريقة وأسباب book2trip في تجميع واستخدام وحماية والتعامل مع بياناتك الشخصية. كذلك يُرجى أيضاً الأخذ في الاعتبار أن هذه السياسة هي التي تحكم استخدامك لموقعنا book2trip. فحينما تقوم بزيارة موقعنا، فأنت توافق على الممارسات المنصوص عليها فيما يلي.
              </p>
              <p class="inner-content">
                الاستئمان: تؤمن book2trip بأن الإعداد للسفر أو التأجير أو الإعلان عن وحدتك عبر شبكة الإنترنت يتضمن قدر كبير من الثقة من جانبك ولذلك تأخذ book2trip هذه الثقة على نحو كامل من الجدية معطية خصوصية وسرية بياناتك الشخصية الأولوية القصوى.
              </p>
              <h4><strong>ما هي البيانات الشخصية التي تقوم book2trip بتجميعها من المستخدمين اللذين يقومون بزيارة الموقع؟</strong></h4>
              <h5><strong>بيانات عامة</strong></h5>
              <p class="inner-content">
                تقوم book2trip بتجميع وحفظ أي بيانات تقوم بإدخالها عندما تتصفح الموقع أو تقوم بإنشاء حساب أو حجز أي وحدة أو إضافة وحدة أو ملئ استمارة أو طلب استلام النشرة الإخبارية أو أي نشاط تقوم به على الموقع. هذه البيانات تتضمن الاسم الأول والأخير والبريد الإلكتروني والعنوان ورقم الهاتف المحمول ورقم البطاقة الشخصية والصور والحسابات على مواقع التواصل الاجتماعي وأي تفاصيل أخرى منك. كذلك تجمع book2trip معلومات عن وجهات السفر المفضلة لك.
              </p>
              <p class="inner-content">
                بيانات من مواقع أخرى
              </p>
              <p class="inner-content">
                لا تقوم book2trip حالياً بجمع أي معلومات من مصادر طرف ثالث لتقوم بزيادتها لحسابك كمستخدم.
              </p>
              <h5><strong>معلومات آلية</strong></h5>
              <p class="inner-content">
                تقوم book2trip بجمع معلومات خاصة بالحاسوب تلقائياً عندما تقوم بزيارة الموقع مثل تسجيل عنوان بروتوكول شبكة الإنترنت والنشاط الذي تقوم به مثل مشاهدة الوحدات والحجوزات المرسلة والحجوزات التي تم الموافقة عليها.
              </p>
              <h4><strong>كيف تقوم book2trip باستخدام بياناتك الشخصية؟</strong></h4>
              <p class="inner-content">
                تستخدم book2trip البيانات التي تجمعها لتجعل خبرتك كمستخدم شخصية، لتتلقى المحتوى والوحدات والعروض التي تناسبك. كذلك تستخدم book2trip هذه البيانات لتطوير الموقع من أجل خدمة أفضل في تلبية احتياجاتك وإرسال تأكيدات الحجز والتحديثات. تستخدم book2trip هذه البيانات لإدارة المنافسة والإعلان والدراسات الاستقصائية وخصائص الموقع الأخرى وكذلك لطلب تقييم شامل للوحدات وملاكها ولمتابعة أي استفسارات من جانب المستخدمين. وكذلك من الأسباب الأكثر أهمية التي تستخدم book2trip لأجلها هذه البيانات هي التحقق من هوية المستخدمين سواء الزائرين أو ملاك العقارات لزيادة مصداقية الموقع وكذلك زيادة مستوى أمن وسلامة المعلومات والمستخدمين وتقليل فرصة التزوير والاحتيال. إن خاصية التحقق من المستخدم تسمح لك بمعرفة المزيد عن الشخص الذي تتعامل معه سواء كزائر أو كمالك. بشكل عام تستخدم book2trip هذه البيانات لفائدتك لتتمكن من الاتصال بك وتطبيق شروط الاستخدام او أي أسباب أخرى يتم توضيحها أثناء إدخال البيانات. إذا قمت بالموافقة على إدخال بياناتك على الموقع، فأنت توافق على حفظ تلك البيانات في قاعدة البيانات الخاصة بالموقع.
              </p>
              <h4><strong>كيف تحمي book2trip بياناتك الشخصية؟</strong></h4>
              <p class="inner-content">
                تهتم book2trip جداً بثقتك في استخدام الموقع من أجل تأجير عقار لقضاء عطلتك أو من أجل عرض عقارك للإيجار، فإن book2trip تلتزم البيانات التي تجمعها. لا تقوم book2trip ببيع أي بيانات خاصة بالمستخدم بما في ذلك الأسماء وبيانات الاتصال وأرقام الهاتف وعناوين البريد الإلكتروني أو أي بيانات أخرى لطرف ثالث. على الرغم من أنه لا يوجد موقع يضمن أمن المعلومات بشكل كامل، فقد قامت book2trip بتطبيق إجراءات أمن مناسبة لحماية البيانات الشخصية التي تقوم بإدخالها. فإن بياناتك الشخصية يتم حفظها على شبكات آمنة ولا يجوز إلا لعدد قليل جداً الدخول على تلك الشبكات اللذين لديهم حق الدخول ومطالبون بإبقاء البيانات سرية. تقوم book2trip بتطبيق مجموعة من الإجراءات عندما يقوم المستخدم بأي نشاط على الموقع أو يحاول الوصول إلى أي بيانات وذلك للحفاظ على أمن البيانات الشخصية.
              </p>
              <h5><strong>روابط خارجية</strong></h5>
              <p class="inner-content">
                إذا قام موقع book2trip أي جزء منه بتحويلك إلى روابط خارجية أو مواقع أخرى، فإن تلك المواقع لا تقع تحت أحكام هذه السياسة فإنه من المفضل مراجعة سياسة الخصوصية الخاصة بهذه المواقع لمعرفة إجراءاتهم في جمع واستخدام والكشف عن البيانات الشخصية.
              </p>
              <h4><strong>كيف يمكنك الوصول إلى بياناتك الشخصية؟</strong></h4>
              <p class="inner-content">
                يمكنك اختيار عدم إدخال أي بيانات شخصية ولكن سيؤثر ذلك على إمكانية الاستفادة الكاملة من كل خصائص book2trip من تأجير العقارات لقضاء عطلتك أو عرض عقاراتك للإيجار. يمكنك الوصول إلى بياناتك الشخصية عند تسجيل الدخول على حسابك عبر صفحات مختلفة منها: صفحة "حسابي" حيث يمكنك رؤية جميع البيانات الشخصية التي قمت بإدخالها، وكذلك صفحة "تعديل حسابي" حيث يمكنك تعديل أو تحديث البيانات التي قمت بإدخالها وتغيير كلمة المرور وأيضاً صفحة "حساباتي" حيث يمكنك مراجعة حساباتك على مواقع التواصل الاجتماعي التي قمت بربطها لحسابك على book2trip.
              </p>
              <h4><strong>ما هي اختياراتك نحو جمع واستخدام بياناتك؟</strong></h4>
              <p class="inner-content">
                كما تم التوضيح من قبل، يمكنك اختيار عدم إدخال أي بيانات شخصية ولكن سيؤثر ذلك على إمكانية الاستفادة الكاملة من كل خصائص book2trip من تأجير العقارات لقضاء عطلتك أو الاتصال بأصحاب العقارات أو عرض عقاراتك للإيجار أو الخصائص الأخرى التي تمنحها book2trip. يمكنك أيضاً إضافة أو تعديل البيانات التي قمت بإدخالها من قبل.
              </p>
              <h4><strong>متى يمكن أن تقوم book2trip بالإفصاح عن بياناتك الشخصية لجهات أخرى؟</strong></h4>
              <p class="inner-content">
                يمكن أن تقوم book2trip بالإفصاح عن بياناتك للرد على استدعاءات وأوامر المحكمة أو أي إجراءات قانونية ولتفعيل الحقوق القانونية وللبحث عن ومنع واتخاذ إجراءات تجاه النشاطات غير القانونية أو المشتبه أن تكون غير قانونية ولحماية والدفاع عن الحقوق والملكية وأمن book2trip وعملائه ولمتابعة تطبيق شروط الاستخدام والاتفاقيات الأخرى.
              </p>
              <h4><strong>هل تستخدم book2trip ملفات تعريف الارتباط؟</strong></h4>
              <p class="inner-content">
                تستخدم book2trip ملفات تعريف الارتباط. ملفات تعريف الارتباط هي ملفات صغيرة يرسلها الموقع أو مزود الخدمة للكمبيوتر من خلال متصفح الويب الذي تستخدمه (إذا سمحت بذلك) والتي تساعد الموقع أو مزود الخدمة على معرفة نوع المتصفح الذي تستخدمه وحفظ بعض المعلومات. تستخدم book2trip ملفات تعريف الارتباط لمعرفة وحفظ تفضيلات المستخدم للزيارات المستقبلية للموقع بناء على النشاط السابق أو الحالي على الموقع وكذلك تستخدمها في حفظ بياناتك لتسهيل تسجيل الدخول وتحسين الخدمة وكذلك تجميع بيانات عن حركة المرور على الموقع والتفاعل مع الموقع من أجل تقديم تجربة أفضل مع الموقع ومميزات في المستقبل.
              </p>
              <p class="inner-content">
                فعلى سبيل المثال، عندما تقوم بتسجيل الدخول لحسابك كمستخدم book2trip سيتم تسجيل رقمك كمستخدم واسم المستخدم لحسابك في ملف تعريف الارتباط على جهازك. يمكن أيضا حفظ كلمة المرور إذا قمت بطلب حفظ كلمة المرور لهذا الموقع.
              </p>
              <p class="inner-content">
                يمكنك اختيار أن ينبهك الكمبيوتر كل مرة يتم إرسال ملف تعريف الارتباط وكذلك يمكنك اختيار غلق إرسال ملفات تعريف الارتباط من خلال إعدادات متصفحك. وبما أن هناك اختلافات ما بين المتصفح والآخر فيمكنك الاطلاع على قائمة المساعدة الخاصة بمتصفحك لمعرفة الطريقة الصحيحة لتعديل إعدادات ملفات تعريف الارتباط.
              </p>
              <p class="inner-content">
                إذا قمت بإغلاق إرسال ملفات تعريف الارتباط، سيتم إغلاق بعض الخصائص. ولكن ما زال سيمكنك عرض وحدتك للإيجار أو تأخير الوحدات.
              </p>
              <h4><strong>هل تستخدم book2trip روابط لأطراف ثالثة؟</strong></h4>
              <p class="inner-content">
                لا تقوم book2trip ببيع أو تحويل بياناتك الشخصية لأي أطراف خارجية ولا تحتوي على أو تمنح منتجات أو خدمات أطراف ثالثة.
              </p>
              <h4><strong>كيف تقوم book2trip بإعلامك عن أي تغييرات في سياسة الخصوصية الخاصة بها؟</strong></h4>
              <p class="inner-content">
                يمكن أن تقوم book2trip بتحديث هذه السياسة في المستقبل وعليه تلتزم book2trip بإعلام المستخدمين بهذه التغييرات عن طريق إضافة إشعار واضح على الموقع يظهر مع أول تسجيل دخول تقوم به ومن خلال الصفحة الخاصة بسياسة الخصوصية.
              </p>
              <h4><strong>كيف يمكنك التواصل مع فريق book2trip؟</strong></h4>
              <p class="inner-content">
                إذا أردت الاستفسار عن هذه السياسة، يمكنك التواصل مع فريق book2trip عبر البريد الإلكتروني:  <a href="mailto:info@book2trip.com"> info@book2trip.com</a>. يمكنك أيضاً التواصل مع فريق book2trip عبر الحسابات على مواقع التواصل الاجتماعي.
              </p>
              <p class="inner-content">
                <strong>يتم العمل بهذه السياسة اعتباراً من 20\11\2016.</strong>
              </p>
              ',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    );