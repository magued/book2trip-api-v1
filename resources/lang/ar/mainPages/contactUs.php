<?php

return [

    'contactUs'=>'تواصل معنا',
    'contactuscomment'=>'لمزيد من الاستفسارات، تواصل مع فريق عمل book2trip.',
    'optional'=>'اختياري',
    'subject'=>'موضوع الرسالة',
    'guest'=>'زائر',
    'email'=>'البريد الإلكتروني',
    'phone'=>'رقم الهاتف المحمول',
    'list_id'=>'رقم تعريف الوحدة',
    'msg_text'=>'الرسالة',
    'send'=>'إرسال',
    'facebook'=>'Facebook',
    'msg_success'=>'تم إرسال رسالتك بنجاح.',
    'SomeThing_else'=>'أخرى',
    'FeedBack'=>'تعليق',
    'I_want_to_list_my_place'=>'إضافة وحدتي',
    'My_holiday'=>'عطلتي',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',

];
