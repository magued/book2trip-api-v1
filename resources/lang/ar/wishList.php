<?php


return [
    'delWishList'=>'هل تريد حذف هذه الوحدة من قائمتك المفضلة؟',
    'yes'=>'نعم',
    'no'=>'لا',
    'doYouWantToAddThisListingToYourWishlisting'=>'هل تريد أن تضف هذه القائمة إلى قائمتك المفضلة؟ ',
    'close'=>'إغلاق',
    'addToMyWishlisting'=>'أضف إالى قائمتى امفضلة',
    'addWishListing'=>'أضف قائمتى المفضلة',
    'removeWishlisting'=>'حذف قائمتى المفضلة',
    'removeFromMyWishListing'=>'حذف من قائمتى المفضلة',
    'wishlist'=>'القائمة المفضلة',
    'updatedOn'=>'تحديث فى',
    'preview'=>'استعراض',
    'delete'=>'إزالة من قائمتي المفضلة',
    'noWishlistingFound'=>'لم يتم العثور على القائمة المفضلة',
    'wish_del_success'=>' تم حذف القائمة المفضلة ',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];

