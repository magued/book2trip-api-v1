<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted"             => "يجب أن يتم قبول هذه البيانات.",
    "active_url"           => "يجب أن يكون الرابط صحيح.",
    "after"                => "يجب أن يكون التاريخ بعد :date.",
    "alpha_spaces"                => "يمكن إدخال حروف فقط.",
    "alpha"                => "يمكن إدخال حروف فقط.",
    "alpha_dash"           => "يمكن إدخال حروف وأرقام وشُرَط فقط.",
    "alpha_num"            => "يمكن إدخال حروف وأرقام فقط.",
    "array"                => "يجب أن تكون هذه الخانة مصفوفة فقط.",
    "before"               => "يجب أن يكون التاريخ قبل :date.",
    "between"              => [
        "numeric" => "يجب أن تكون البيانات بين :min و :max.",
        "file"    => "يجب أن تكون البيانات بين :min و :max كيلوبايت.",
        "string"  => "يجب أن تكون البيانات بين :min و :max حروف.",
        "array"   => "يجب أن تكون البيانات بين :min و :max عنصر.",
    ],
    "boolean"              => "يجب أن تكون البيانات صح أو خطأ.",
    "confirmed"            => "يجب أن تكون بيانات التأكيد متطابقة.",
    "date"                 => "يجب أن يكون التاريخ صحيح.",
    "date_format"          => "يجب أن تكون بالبيانات بالشكل :format.",
    "different"            => "يجب أن تكون هذه البيانات و :other مختلفة.",
    "digits"               => "يجب أن تكون البيانات :digits رقم.",
    "digits_between"       => "يجب أن تكون البيانات بين :min و :max رقم.",
    "email"                => "يجب أن يكون البريد الإلكتروني صحيح.",
    "filled"               => "هذه البيانات مطلوبة.",
    "exists"               => "هذه البيانات غير صحيحة.",
    "image"                => "يجب إدخال صورة.",
    "in"                   => "هذه القيمة غير صحيحة.",
    "integer"              => "يجب أن تكون البيانات عدد صحيح.",
    "ip"                   => "يجب أن يكون عنوان بروتوكول الإنترنت صحيح.",
    "max"                  => [
        "numeric" => "يجب ألا تزيد البيانات عن :max.",
        "file"    => "يجب ألا تزيد البيانات عن :max كيلوبايت.",
        "string"  => "يجب ألا تزيد البيانات عن :max حروف.",
        "array"   => "يجب ألا تزيد البيانات عن :max عنصر.",
    ],
    "mimes"                => "يجب أن يكون نوع الملف :values.",
    "min"                  => [
        "numeric" => "يجب أن تكون البيانات على الأقل :min.",
        "file"    => "يجب أن تكون البيانات على الأقل :min كيلوبايب.",
        "string"  => "يجب إدخال :min حروف على الأقل.",
        "array"   => "يجب أن تحتوى البيانات على الأقل على :min عنصر.",
    ],
    "not_in"               => "هذه البيانات غير صحيحة.",
    "numeric"              => "يجب إدخال رقم.",
    "regex"                => "هذا التنسيق غير صحيح.",
    "required"             => "هذه البيانات مطلوبة.",
    "required_if"          => "هذه البيانات مطلوبة عندما يكون :other :value.",
    "required_with"        => "هذه البيانات مطلوبة في حالة وجود :values.",
    "required_with_all"    => "هذه البيانات مطلوبة في حالة وجود :values.",
    "required_without"     => "هذه البيانات مطلوبة في حالة عدم وجود :values.",
    "required_without_all" => "هذه البيانات مطلوبة في حالة عدم وجود أي من :values.",
    "same"                 => "يجب أن تتطابق هذه البيانات و :other.",
    "size"                 => [
        "numeric" => "يجب أن تكون هذه البيانات :size.",
        "file"    => "هذه البيانات يجب أن تكون :size كيلوبايت.",
        "string"  => "هذه البيانات يجب أن تكون :size حرف.",
        "array"   => "هذه البيانات يجب أن تكون :size عنصر.",
    ],
    "unique"               => 'تم استخدام :attribute من قبل.',
    "url"                  => "تنسيق البيانات غير صحيح.",
    "timezone"             => "يجب أن يكون التوقيت صحيح.",
    "nowhitespace"             => "يجب ألا يكون البيانات بها مسافة",
    "lettersonly"             => "يجب أن تكون البيانات حروف فقط",
    "emailexist"             => "مسجل بحساب لتسجيل الدخول",
    "login"             => "تسجيل الدخول",
    "uservalid"             => "يجب أن تكون البيانات حروف صغيرة بدون _",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
